'use strict';

(function() {

  class SuccessController {

    constructor($state, transferService) {
      this.$state = $state;
      this.transferService = transferService;
    }

    goToUrl(url) {

    }
  }

  angular.module('schwimmfestivalApp')
    .controller('SuccessController', SuccessController);

})();
