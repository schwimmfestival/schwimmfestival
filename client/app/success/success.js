'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('success', {
        url: '/cashpoint/success',
        templateUrl: 'app/success/success.html',
        controller: 'SuccessController'
      });
  });
