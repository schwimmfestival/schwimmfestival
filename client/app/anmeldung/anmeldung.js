'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('anmeldung', {
        url: '/anmeldung',
        templateUrl: 'app/anmeldung/anmeldung.html',
        controller: 'AnmeldeStandCtrl',
        controllerAs: 'ctrl',
        authenticate: true
      });
  });
