'use strict';

(function() {
  class AnmeldeStandCtrl {

    constructor($state, $uibModal, buchungsService, utilService, groupService, participantService, challengeService) {
      this.$state = $state;
      this.group = "";
      this.family = "";
      this.teilnehmer = [];
      this.buchungsService = buchungsService;
      this.utilService = utilService;
      this.$uibModal = $uibModal;
      this.groupService = groupService;
      this.participantService = participantService;
      this.challengeService = challengeService;
      this.updatedTn = [];
    }


    searchBuchung(id) {
      this.clearModel();
      this.teilnehmer = [];
      this.family = {};
      this.group = {};
      this.workTn = {};
      this.updatedTn = [];

      this.buchungsService.loadById(id).then(response => {
        var buchungObj = new Buchung();
        buchungObj.deserialize(response);
        this.buchung = buchungObj;

        for (var i = 0; i < this.buchung.teilnehmer.length; i++) {
          if (this.buchung.teilnehmer[i].gebDate !== '1900-01-01') {
            this.updatedTn.push(this.buchung.teilnehmer[i]);
          }
          else {
            this.teilnehmer.push(this.buchung.teilnehmer[i]);
          }

        }


        this.initOpenState();

        if (this.teilnehmer.length >= 1) {
          this.loadTeilnehmer(0);
        }

        this.groupService.getGroupsWithoutFamily().then(response => {
          this.groupList = Object.keys(response).map(function(key) {
            return response[key];
          });
        });

        this.groupService.getFamilies().then(response => {
          this.families = Object.keys(response).map(function(key) {
            return response[key];
          });
        });
      });
    }

    initOpenState() {
      for (var i = 0; i < this.teilnehmer.length; i++) {
        this.teilnehmer[i].open = false;
      }
    }

    addNewGroup(searchText) {
      var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: 'app/registration/modal/groupPopup.html',
        controller: 'GroupPopupCtrl',
        controllerAs: 'modalCtrl',
        size: 'sm',
        resolve: {
          options: function(groupService) {
            return groupService.getGroupCategories();
          },
          text: function() {
            return searchText;
          }
        }
      });

      modalInstance.result.then(group => {
        // 2 -> id for grouptype family
        if (group.idKategorie === 2) {
          this.family = group;
          this.families.push(group);
        }
        else {
          this.group = group;
          this.groupList.push(group);
        }
      });
    }

    removeFamily() {
      this.family = null;
    }

    removeGroup() {
      this.group = null;
    }

    clearModel() {
      this.goeFourTime = "";
      this.goeEightTime = "";
      this.goe4 = false;
      this.goe8 = false;
      this.goe4Challenge = {};
      this.goe8Challenge = {};
    }

    loadTeilnehmer(index) {
      this.open(index);
      this.clearModel();
      this.workTn = this.teilnehmer[index];

      this.loadChallenges();
      this.loadGroups(this.teilnehmer[index]);

      var date = new Date(this.workTn.gebDate);
      var options = {year: 'numeric', month: 'numeric', day: 'numeric'};
      this.gebDate = date.toLocaleDateString('de-De', options);

    }

    loadChallenges() {
      if (this.workTn.challenges) {
        //load Challenges
        for (var i = 0; i < this.workTn.challenges.length; i++) {
          var challenge = this.workTn.challenges[i];
          if (challenge.strecke == 400) {
            this.goe4 = true;
            this.goe4Challenge = challenge;
            this.goeFourTime = this.utilService.millisToMinutesAndSeconds(challenge.zeitErwartet);
          } else if (challenge.strecke == 800) {
            this.goe8 = true;
            this.goe8Challenge = challenge;
            this.goeEightTime = this.utilService.millisToMinutesAndSeconds(challenge.zeitErwartet);
          }
        }
      }
    }

    loadGroups(teilnehmer) {
      var groups = teilnehmer.groups;
      this.workTn.existingGroups = [];
      if (groups && groups.length > 0) {
        for (var j = 0; j < groups.length; j++) {
          if (groups[j].idKategorie == 2) {
            this.family = groups[j];
            this.workTn.existingGroups.push(groups[j].id);
          }
          else {
            this.group = groups[j];
            this.workTn.existingGroups.push(groups[j].id);
          }
        }
      }
    }

    saveTeilnehmer(index, next) {
      this.workTn.vorname = this.utilService.encode(this.workTn.vorname);
      this.workTn.nachname = this.utilService.encode(this.workTn.nachname);
      this.workTn.gebDate = this.utilService.convertToDate(this.gebDate);
      this.saveGoeChallenge();
      this.saveGroups();
      this.teilnehmer[index] = this.workTn;

      this.saveToDatabase(this.teilnehmer[index]);
      this.teilnehmer[index].actions = true;
      if (next) {
        this.loadTeilnehmer(index + 1);
      }
    }

    saveGroups() {
      if (this.family && this.family != null) {
        this.workTn.groups.push(this.family.id);
      }
      if (this.group && this.group != null) {
        this.workTn.groups.push(this.group.id);
      }
    }

    saveGoeChallenge() {
      if (this.goe4) {
        var time4 = this.utilService.convertToTime(this.goeFourTime);
        this.workTn.challenges.push(GoeChallenge.four(time4));
      }
      if (this.goe8) {
        var time8 = this.utilService.convertToTime(this.goeEightTime);
        this.workTn.challenges.push(GoeChallenge.eight(time8));
      }

    }

    saveToDatabase(teilnehmer) {

      //Update Challenges
      for (var i = 0; i < teilnehmer.challenges.length; i++) {
        this.challengeService.addChallenge(teilnehmer.id, teilnehmer.challenges[i]).then(response => {
          teilnehmer.challenges.splice(i, 1, response);
        });
      }

      //Add new Groups
      for (var k = 0; k < teilnehmer.groups.length; k++) {
        this.participantService.addGroup(teilnehmer.id, teilnehmer.groups[k]).then(response => {
          teilnehmer.groups.splice(k, 1, response);
        });
      }

      this.participantService.updateParticipant(teilnehmer).then(response => {
        var tnIdx = this.teilnehmer.indexOf(teilnehmer);
        this.teilnehmer.splice(tnIdx, 1);
        this.updatedTn.push(response);
      });

      console.log(this.updatedTn);

    }

    open(index) {
      for (var i = 0; i < this.teilnehmer.length; i++) {
        this.teilnehmer[i].open = false;
        if (i == index) {
          this.teilnehmer[i].open = true;
        }

      }
    }


    edit(teilnehmer) {
      this.$state.go('dashboard.teilnehmer.edit', {id: teilnehmer.id});
    }

    printChallenge(teilnehmer, distance) {
      for (var i = 0; i < teilnehmer.challenges.length; i++) {
        if (teilnehmer.challenges[i].strecke == distance) {
          return teilnehmer.challenges[i].id;
        }
      }
    }

    millisToMinutesAndSeconds(millis) {
      var minutes = Math.floor(millis / 60000);
      var seconds = ((millis % 60000) / 1000).toFixed(0);
      return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
  }
  angular.module('schwimmfestivalApp').controller('AnmeldeStandCtrl', AnmeldeStandCtrl);
})();


