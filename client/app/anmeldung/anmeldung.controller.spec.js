'use strict';

describe('Controller: AnmeldeStandCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var AnmeldungCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    AnmeldungCtrl = $controller('AnmeldeStandCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
