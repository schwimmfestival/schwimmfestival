'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('tageskasse', {
        url: '/tageskasse',
        templateUrl: 'app/tageskasse/tageskasse.html',
        controller: 'TageskassenCtrl',
        controllerAs: 'ctrl',
        authenticate: true
      });
  });
