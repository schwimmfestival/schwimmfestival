'use strict';

(function() {

  class TageskassenCtrl {

    constructor($http, $window, $uibModal, $state, articleService, Auth, buchungsService, checkinService) {
      this.$http = $http;
      this.$uibModal = $uibModal;
      this.$state = $state;
      this.articleSerive = articleService;
      this.buchungsService = buchungsService;
      this.history = [];
      this.$window = $window;
      var that = this;
      this.menuOptions = [
        ['Löschen', function($itemScope) {
          var id = $itemScope.booking.id;
          console.log("Buchung " + id + " löschen");

          that.buchungsService.storno($itemScope.booking).then(
            response => {
              $.each(that.history, function(index, element) {
                if (element.id == id) {
                  that.history.splice(index, 1);
                  return false;
                }
              });
            }
          );
        }]
      ];
      this.reset();

      buchungsService.getLastBuchungenByUser().then(response=> {
        for (var i = 0; i < response.buchungen.length; i++) {
          var bu = new Buchung();
          bu.deserialize(response.buchungen[i]);

          this.history.push(bu);
        }
        }
      );

      this.ID_ADULT = "10";
      this.ID_CHILD = "12";
      this.ID_DLRG = "13";
      this.ID_VIP = "14";

      this.vip = Auth.hasRole(UserRights.rechnungen());
      this.checkinService = checkinService;
      this.code = "";
    }

    reset() {
      this.countAdult = 0;
      this.countChild = 0;
      this.countDlrg = 0;
      this.countVip = 0;
      this.sum = 0;
      this.step = 1;
      this.paidMoney = 0;

      this.billing = {
        printed: false,
        gender: 'm',
        firstname: '',
        lastname: '',
        company: '',
        street: '',
        zip: '',
        city: ''
      };

      this.updateSum();
    }

    incrementCount(type) {
      switch (type) {
        case "adult":
          this.countAdult++;
          break;
        case "child":
          this.countChild++;
          break;
        case "dlrg":
          this.countDlrg++;
          break;
        case "vip":
          this.countVip++;
          break;
      }
      this.updateSum();
    }

    decrementCount(type) {
      if (type == "adult" && this.countAdult > 0)
        this.countAdult--;
      if (type == "child" && this.countChild > 0)
        this.countChild--;
      if (type == "dlrg" && this.countDlrg > 0)
        this.countDlrg--;
      if (type == "vip" && this.countVip > 0)
        this.countVip--;
      this.updateSum();
    }

    updateSum() {
      this.articleSerive.getDaySell().then(response=> {
          this.sum = response[this.ID_ADULT].preis * this.countAdult + response[this.ID_CHILD].preis * this.countChild +
            response[this.ID_DLRG].preis * this.countDlrg + response[this.ID_VIP].preis * this.countVip;
        }
      );
    }

    nextStep() {
      if (this.step == 2) {
        var buchung = {
          id: -1,
          date: "",
          status: "Barkasse",
          bestCode: "",
          versandart: "",
          userId: "",
          einnahmen: []
        };
        if (this.countAdult > 0) {
          buchung.einnahmen.push({
            "id": buchung.einnahmen.length + 1,
            "artikel": this.ID_ADULT,
            "teilnehmer": this.countAdult
          });
        }
        if (this.countChild > 0) {
          buchung.einnahmen.push({
            "id": buchung.einnahmen.length + 1,
            "artikel": this.ID_CHILD,
            "teilnehmer": this.countChild
          });
        }
        if (this.countDlrg > 0) {
          buchung.einnahmen.push({
            "id": buchung.einnahmen.length + 1,
            "artikel": this.ID_DLRG,
            "teilnehmer": this.countDlrg
          });
        }
        if (this.countVip > 0) {
          buchung.einnahmen.push({
            "id": buchung.einnahmen.length + 1,
            "artikel": this.ID_VIP,
            "teilnehmer": this.countVip
          });

        }
        this.payAndSave(buchung);
      }
      else if (this.step < 3) {
        this.step++;
      }
      else {
        this.step = 1;
        this.reset();
      }
    }

    payAndSave(buchung) {
      this.buchungsService.insertDaySale(buchung).then(response=> {
          console.log(response);
        var bu = new Buchung();
        bu.deserialize(response);
          this.$window.open('../../../server/Rechnungen/24h-' + response.rechnung.id + 'Rechnung.pdf', '24h-Rechnung');
        this.history.push(bu);
          if (this.history.length >= 5) {
            this.history.shift();
          }
          this.step++;
        }
      );
    }

    checkTN() {
      this.checkinService.checkTN(this.code).then(response=> {
        if (response.id == -5) {
          this.error = "Der TN konnte nicht gefunden werden";
        }
        else {
          this.name = response.vorname + " " + response.nachname;
          this.age = response.gebDate;
          this.gender = response.gender;
        }
        if (response.id == -6) {
          this.time = response.isDa;
          this.error = "Der TN ist bereits angemeldet";
        }
        if (response.id == -7) {
          this.error = "Der TN hat noch nicht bezahlt";
        }
      });
    }

    resetTN() {
      this.code = "";
      this.name = "";
      this.age = "";
      this.gender = "";
      this.time = "";
      this.error = "";
    }
  }

  angular.module('schwimmfestivalApp').controller('TageskassenCtrl', TageskassenCtrl);
})
();
