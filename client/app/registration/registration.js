'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('registration', {
        url: '/registration',
        service: 'validations',
        templateUrl: 'app/registration/registration.html',
        controller: 'RegistrationCtrl',
        controllerAs: 'ctrl',
      });
  });
