(function() {

  class RegistrationCtrl {

    constructor($http, $uibModal, $state, utilService, transferService, articleService, groupService) {
      this.$http = $http;
      this.$uibModal = $uibModal;
      this.add = true;
      this.erm = false;
      this.participant = new Participant();
      this.utilService = utilService;
      this.transferService = transferService;
      this.articleServie = articleService;
      this.participantList = [];
      this.veggie = false;
      this.removedFood = {};
      this.selectedFood = {};


      if (transferService.getFirstName()) {
        this.vorname = transferService.getFirstName();
      }

      if (transferService.getLastName()) {
        this.nachname = transferService.getLastName();
      }

      if (transferService.getSubscribers().length > 0) {
        this.participantList = transferService.getSubscribers();
      }

      articleService.getFood().then(response => {
        this.foodList = response;
        Object.assign(this.selectedFood, this.foodList);
        this.foodListCount = Object.keys(response).length;
      });
      articleService.getPreSell().then(response => {
        this.preSell = response;
      });

      groupService.getGroupsWithoutFamily().then(response => {
        this.groupList = Object.keys(response).map(function(key) {
          return response[key];
        });
      });

      groupService.getFamilies().then(response => {
        this.families = Object.keys(response).map(function(key) {
          return response[key];
        });
      });


    }

    addParticipant(form) {
      if (form.$valid) {
        this.participant.gebDate = this.utilService.convertToDate(this.date);
        this.encode();
        this.saveEinnahmen();
        this.saveGoeChallenge();
        this.saveGroups();
        this.participantList.push(this.participant);

        this.reset();
        form.$setUntouched();
        console.log(this.utilService.toJSON(this.participantList));
      }
    }

    removeParticipant(participant) {
      var idx = this.participantList.indexOf(participant);
      this.participantList.splice(idx, 1);
    }

    encode() {
      this.participant.vorname = this.utilService.encode(this.vorname);
      this.participant.nachname = this.utilService.encode(this.nachname);
    }

    saveEinnahmen() {
      var list = $.map(this.selectedFood, function(value, index) {
        return value;
      });

      list.push(this.preSell[this.type]);
      this.participant.einnahmen = list;
    }

    switchVeggi() {
      this.removedFood = {};
      this.selectedFood = {};
      if (this.veggie) {
        this.articleServie.getVegiFood().then(response => {
          this.foodList = response;
          Object.assign(this.selectedFood, this.foodList);
        });
      }
      else {
        this.articleServie.getFood().then(
          response => {
            this.foodList = response;
            Object.assign(this.selectedFood, this.foodList);
          }
        )
      }
    }

    switchFood(id, add) {
      if (add) {
        this.selectedFood[id] = this.removedFood[id];
        delete this.removedFood[id];
      }
      else {
        this.removedFood[id] = this.selectedFood[id];
        delete this.selectedFood[id];
      }
    }

    saveGroups() {
      if (this.family && this.family != null) {
        this.participant.groups.push(this.family.id);
      }
      if (this.group && this.group != null) {
        this.participant.groups.push(this.group.id);
      }
    }

    saveGoeChallenge() {
      if (this.goe4) {
        var time4 = this.utilService.convertToTime(this.goeFourTime);
        this.participant.challenges.push(GoeChallenge.four(time4));
      }
      if (this.goe8) {
        var time8 = this.utilService.convertToTime(this.goeEightTime);
        this.participant.challenges.push(GoeChallenge.eight(time8));
      }

      console.log(this.participant);
    }

    reset() {
      this.date = '';
      this.vorname = '';
      this.nachname = '';
      this.goe4 = false;
      this.goeFourTime = '';
      this.goe8 = false;
      this.goeEightTime = '';
      this.participant = new Participant();
      this.removedFood = {};
    }

    goToCashpoint() {
      this.transferService.setSubscribers(this.participantList);
    }

    addNewGroup(searchText) {
      var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: 'app/registration/modal/groupPopup.html',
        controller: 'GroupPopupCtrl',
        controllerAs: 'modalCtrl',
        size: 'sm',
        resolve: {
          options: function(groupService) {
            return groupService.getGroupCategories();
          },
          text: function() {
            return searchText;
          }
        }
      });

      modalInstance.result.then(group => {
        // 2 -> id for grouptype family
        if (group.idKategorie === 2) {
          this.family = group;
          this.families.push(group);
        }
        else {
          this.group = group;
          this.groupList.push(group);
        }
      });
    }

    removeFamily() {
      this.family = null;
    }

    removeGroup() {
      this.group = null;
    }
  }

  angular.module('schwimmfestivalApp').controller('RegistrationCtrl', RegistrationCtrl);
})();

'use strict';
