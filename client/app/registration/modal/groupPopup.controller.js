'use strict';

(function() {

    class GroupPopupCtrl {
        constructor($scope, $uibModalInstance, options, text, groupService, utilService) {
            this.groupService = groupService;
            this.utilService = utilService;
            this.options = options;
            this.$uibModalInstance = $uibModalInstance;
            this.name = text;
        }

        click() {
            //TODo Save here
            var group = new Group(this.utilService.encode(this.name), this.selected);
            this.groupService.createGroup(group).then(response => {
                this.$uibModalInstance.close(response);
            });
        }

        cancel() {
            this.$uibModalInstance.dismiss('cancel');
        }

    }


    angular.module('schwimmfestivalApp')
        .controller('GroupPopupCtrl', GroupPopupCtrl);

})();
