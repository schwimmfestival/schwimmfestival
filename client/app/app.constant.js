(function(angular, undefined) {
'use strict';

angular.module('schwimmfestivalApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);