'use strict';

(function() {
  class TeilnehmerEditCtrl {

    errors = {};
    editingData = {};
    dataList = [];
    isEditing = false;
    selectedFilter = "offen";
    dt = new Date();
    validationSend = false;

    constructor($http, $state, Auth, $location, utilService, participantService, groupService, challengeService) {
      this.$http = $http;
      this.$state = $state;
      this.participantService = participantService;
      this.challengeService = challengeService;
      this.groupService = groupService;
      this.utilService = utilService;
      this.$location = $location;
      this.Auth = Auth;


      this.tnId = $state.params.id;
      this.loadTeilnehmer(this.tnId);
    }

    initParams() {
      this.familyBak = {};
      this.groupBak = {};
      this.goe4ChallengeBak = {};
      this.goe8ChallengeBak = {};
      this.isInit = false;
    }

    cancelEditing(id) {
      this.isEditing = false;
      this.loadTeilnehmer(id);
    }

    loadTeilnehmer(id) {
      this.initParams();
      this.participantService.loadByID(id).then(response => {
        this.tn = Participant.deserialize(response);

        this.groupService.getGroupsWithoutFamily().then(response => {
          this.groupList = Object.keys(response).map(function(key) {
            return response[key];
          });
        });

        this.groupService.getFamilies().then(response => {
          this.families = Object.keys(response).map(function(key) {
            return response[key];
          });
        });
        this.loadTnValues();
      });

    }

    loadTnValues() {
      var date = new Date(this.tn.gebDate);
      var options = {year: 'numeric', month: 'numeric', day: 'numeric'};
      this.gebDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
      this.tn.vorname = this.utilService.decode(this.tn.vorname);
      this.tn.nachname = this.utilService.decode(this.tn.nachname);
      this.loadChallenges();
      this.loadGroups();
      this.isInit = true;
    }

    loadChallenges() {
      if (this.tn.challenges) {
        //load Challenges
        for (var i = 0; i < this.tn.challenges.length; i++) {
          var challenge = this.tn.challenges[i];
          if (challenge.strecke == 400) {
            this.goe4 = true;
            this.goe4Challenge = challenge;
            this.goeFourTime = this.utilService.millisToMinutesAndSeconds(challenge.zeitErwartet);
            Object.assign(this.goe4ChallengeBak, challenge);
          } else if (challenge.strecke == 800) {
            this.goe8 = true;
            this.goe8Challenge = challenge;
            this.goeEightTime = this.utilService.millisToMinutesAndSeconds(challenge.zeitErwartet);
            Object.assign(this.goe8ChallengeBak, challenge);
          }
        }
      }
    }

    loadGroups() {
      this.family = this.tn.getGroup(true);
      Object.assign(this.familyBak, this.family);
      this.group = this.tn.getGroup(false);
      Object.assign(this.groupBak, this.group);
    }

    saveTeilnehmer() {

      this.tn.vorname = this.utilService.encode(this.tn.vorname);
      this.tn.nachname = this.utilService.encode(this.tn.nachname);
      this.tn.gebDate = this.utilService.convertToDate(this.gebDate);

      this.saveGroup();
      this.saveFamily();
      this.saveGoeChalleng400();
      this.saveGoeChalleng800();
      this.participantService.updateParticipant(this.tn).then(response => {
        this.loadTeilnehmer(response.id);
        this.isEditing = false;
      });


    }

    saveGroup() {
      if (this.group && this.group != null) {

        if (this.group.id > 0 && Object.keys(this.groupBak).length === 0 && this.groupBak.constructor === Object) {
          //Add
          this.participantService.addGroup(this.tn.id, this.group.id).then(response => {
            console.log("Add Group ");
            var dbTn = Participant.deserialize(response);
            this.group = dbTn.getGroup(false);
            this.groupBak = {};
            Object.assign(this.groupBak, this.group);
          });
        }
        else if (this.group.id !== this.groupBak.id)
        //Update
          this.participantService.removeGroup(this.tn.id, this.groupBak.id).then(response => {
            this.participantService.addGroup(this.tn.id, this.group.id).then(response => {
              console.log("Update Group " + this.groupBak.name + '=>' + response.name);
              var dbTn = Participant.deserialize(response);
              this.group = dbTn.getGroup(false);
              this.groupBak = {};
              Object.assign(this.groupBak, this.group);
            });
          });
      } else if ((!this.group || this.group == null) && Object.keys(this.groupBak).length !== 0) {
        //Delete
        console.log("Delete Group " + this.groupBak.name);
        this.participantService.removeGroup(this.tn.id, this.groupBak.id).then(response => {
            this.groupBak = {};
            this.group = null;
          }
        );
      }
    }

    saveFamily() {
      if (this.family && this.family != null) {

        if (this.family.id > 0 && Object.keys(this.familyBak).length === 0 && this.familyBak.constructor === Object) {
          //Add
          this.participantService.addGroup(this.tn.id, this.family.id).then(response => {
            var dbTn = Participant.deserialize(response);
            this.family = dbTn.getGroup(true);
            this.familyBak = {};
            Object.assign(this.familyBak, this.family);
          });
        }
        else if (this.family.id !== this.familyBak.id)
        //Update
          this.participantService.removeGroup(this.tn.id, this.familyBak.id).then(response => {
            this.participantService.addGroup(this.tn.id, this.family.id).then(response => {
              var dbTn = Participant.deserialize(response);
              this.family = dbTn.getGroup(true);
              this.familyBak = {};
              Object.assign(this.familyBak, this.family);
            });
          });
      } else if (!this.family && Object.keys(this.familyBak).length !== 0 && this.familyBak.constructor !== Object) {
        //Delete
        this.participantService.removeGroup(this.tn.id, this.familyBak.id).then(response => {
            this.familyBak = {};
            this.family = null;
          }
        );
      }
    }

    saveGoeChalleng800() {
      if (this.goeEightTime) {
        var time8 = this.utilService.convertToTime(this.goeEightTime);
      }

      if (this.goe8 && Object.keys(this.goe8ChallengeBak).length === 0 && this.goe8ChallengeBak.constructor === Object) {
        //addNewChallenge
        this.goe8Challenge = GoeChallenge.eight(time8);
        this.challengeService.addChallenge(this.tn.id, this.goe8Challenge).then(response => {
          this.goe8Challenge = GoeChallenge.deserialize(response);
          console.log('Add Gö 800');
        });
      }
      else if (this.goe8 && time8 != this.goe8ChallengeBak.zeitErwartet && this.goe8Challenge.id > 0) {
        //Update
        this.goe8Challenge.zeitErwartet = time8;
        this.challengeService.updateChallenge(this.goe8Challenge).then(response => {
          console.log('Update Gö 800');
          this.goe8Challenge = GoeChallenge.deserialize(response);
          this.removeAndAddChallenge(this.goe8Challenge, true);
        });
      } else if (Object.keys(this.goe8ChallengeBak).length !== 0 && !this.goe8) {
        //delete
        this.challengeService.deleteChallenge(this.goe8Challenge).then(response => {
          console.log('Delete Gö 800');
          this.removeAndAddChallenge(this.goe8Challenge, false);
          this.goe8Challenge = null;
          this.goe8 = false;
          this.goe8ChallengeBak = {};
        });
      }
    }

    saveGoeChalleng400() {
      if (this.goeFourTime) {
        var time4 = this.utilService.convertToTime(this.goeFourTime);
      }
      if (this.goe4 && Object.keys(this.goe4ChallengeBak).length === 0 && this.goe4ChallengeBak.constructor === Object) {
        //addNewChallenge
        this.goe4Challenge = GoeChallenge.four(time4);
        this.challengeService.addChallenge(this.tn.id, this.goe4Challenge).then(response => {
          console.log('Add Gö 400');
          this.goe4Challenge = GoeChallenge.deserialize(response);
        });
      }
      else if (this.goe4 && time4 != this.goe4ChallengeBak.zeitErwartet && this.goe4Challenge.id > 0) {
        //Update
        this.goe4Challenge.zeitErwartet = time4;
        this.challengeService.updateChallenge(this.goe4Challenge).then(response => {
          console.log('Update Gö 400');
          this.goe4Challenge = GoeChallenge.deserialize(response);
          this.removeAndAddChallenge(this.goe4Challenge, true);
        });
      } else if (Object.keys(this.goe4ChallengeBak).length !== 0 && !this.goe4) {
        //delete
        this.challengeService.deleteChallenge(this.goe4Challenge).then(response => {
          console.log('Delete Gö 400');
          this.removeAndAddChallenge(this.goe4Challenge, false);
          this.goe4Challenge = null;
          this.goe4 = false;
          this.goe4ChallengeBak = {};
        });
      }
    }

    removeAndAddChallenge(challenge, add) {
      var idx = this.tn.challenges.indexOf(challenge);
      this.tn.challenges.splice(idx, 1);
      if (add)
        this.tn.challenges.push(challenge);
    }

    addNewGroup(searchText) {
      var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: 'app/registration/modal/groupPopup.html',
        controller: 'GroupPopupCtrl',
        controllerAs: 'modalCtrl',
        size: 'sm',
        resolve: {
          options: function(groupService) {
            return groupService.getGroupCategories();
          },
          text: function() {
            return searchText;
          }
        }
      });

      modalInstance.result.then(group => {
        // 2 -> id for grouptype family
        if (group.idKategorie === 2) {
          this.family = group;
          this.families.push(group);
        }
        else {
          this.group = group;
          this.groupList.push(group);
        }
      });
    }

    printChallenge(teilnehmer, distance) {
      for (var i = 0; i < teilnehmer.challenges.length; i++) {
        if (teilnehmer.challenges[i].strecke == distance) {
          return teilnehmer.challenges[i].id;
        }
      }
    }
  }

  angular.module('schwimmfestivalApp')
    .controller('TeilnehmerEditCtrl', TeilnehmerEditCtrl);
})();

