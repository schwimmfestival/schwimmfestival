'use strict';

(function() {
  class TeilnehmerCtrl {

    errors = {};
    editingData = {};
    dataList = [];
    isEditing = false;
    selectedFilter = "offen";
    dt = new Date();
    validationSend = false;

    constructor($scope, $http, $state, Auth, $location, $window, participantService) {
      this.$http = $http;
      this.$window = $window;
      this.participantService = participantService;
      this.$location = $location;
      this.Auth = Auth;
      this.$state = $state;
      this.searchString = "";
    }


    updateList() {
      this.dataList = [];
      if (isNaN(this.searchString)) {
        this.participantService.search(this.searchString).then(response => {
          for (var i = 0; i < response.teilnehmer.length; i++) {
            this.dataList.push(Participant.deserialize(response.teilnehmer[i]));
          }
        });
      } else {
        this.participantService.loadByID(this.searchString).then(response => {
          var tn = response;
          if (tn.id > 0) {
            this.edit(tn);
          }
        });
      }
    }

    printChallenge(teilnehmer, distance) {
      for (var i = 0; i < teilnehmer.challenges.length; i++) {
        if (teilnehmer.challenges[i].strecke == distance) {
          return teilnehmer.challenges[i].id;
        }
      }
    }

    edit(teilnehmer) {
      this.$state.go('dashboard.teilnehmer.edit', {id: teilnehmer.id});
    }



  }
  angular.module('schwimmfestivalApp')
    .controller('TeilnehmerCtrl', TeilnehmerCtrl);
})
();

