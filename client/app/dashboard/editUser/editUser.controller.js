'use strict';

(function () {
  class EditUserCtrl {

    constructor(Auth, BenutzerService) {
      this.auth = Auth;
      this.isAdmin = this.auth.hasRole(UserRights.admin())
      this.userService = BenutzerService;
      this.message = "";
      this.userList = [];
      this.permission = [
        {
          id: 1,
          name: 'Admin'
        },
        {
          id: 2,
          name: 'Tageskasse'
        },
        {
          id: 3,
          name: 'Essenskasse'
        },
        {
          id: 4,
          name: 'Rechnungen'
        },
        {
          id: 5,
          name: 'Anmeldung'
        }
      ];

      this.closeChanges();
    }

    expandNewUser() {
      this.showAdd = true;
      this.closeChanges();
    }

    collapseNewUser() {
      this.showAdd = false;
      this.user = {};
      this.user.id = -1;
      this.user.name = "";
      this.user.mail = "";
      this.user.password = "";
      this.user.confirmPassword = "";
      this.closeMessage();
    }

    addUser() {
      if (this.user.password != this.user.confirmPassword) {
        this.message  = "Passwörter sind nicht gleich";
        return;
      }

      var user = {
        id: this.user.id,
        name: this.user.name,
        mail: this.user.mail,
        passwort: this.user.password
      };

      var that = this;
      this.userService.addUser(user).then(response => {
        that.collapseNewUser();
      });
    }

    searchUser() {
      console.log("Searching for " + this.search);
      this.collapseNewUser();
      if (this.search == undefined || this.search == "")
        return;


      var that = this;
      this.userService.searchUser(this.search).then(response=> {
        console.log(response.liste);
        if (response.liste.length == 0) {
          that.message = "Keine Benutzer gefunden";
        }
        if (response.liste.length > 1) {
          that.userList = [];
          $.each(response.liste, function() {
            var user = new User();
            user.deserialize(this);
            that.userList.push(user);
          });
        }
        if (response.liste.length == 1) {
          var user = new User();
          user.deserialize(response.liste[0]);
          that.selectUser(user);
        }
      });
    }

    selectUser(user) {
      this.userList = [];
      this.prev = {};
      this.prev.id = user.id;
      this.prev.name = user.name;
      this.prev.mail = user.mail;
      this.prev.password = user.passwort;
      this.prev.permissions = [];
      var that = this;
      $.each(this.permission, function () {
        that.prev.permissions[this.id] = user.hasPermission(this.id);
      });
      this.change = $.extend(true, {}, that.prev);

      this.editUser = true;
      this.collapseNewUser();
    }

    saveChanges() {
      if (this.change.password != this.change.confirmPassword) {
        this.message = "Passwörter sind nicht gleich";
        return;
      }

      var changedUser = {
        id: this.change.id,
        name: this.change.name,
        mail: this.change.mail,
        passwort: this.change.password
      };

      if (this.change.name != this.prev.name)
        this.userService.updateUserName(changedUser);
      if (this.change.mail != this.prev.mail)
        this.userService.updateUserMail(changedUser);
      if (this.change.password != this.prev.password)
          this.userService.updateUserPw(changedUser);

      var that = this;
      $.each(this.permission, function () {
        if (!that.prev.permissions[this.id] && that.change.permissions[this.id])
          that.userService.addRole(changedUser, this.id);
        if (that.prev.permissions[this.id] && !that.change.permissions[this.id])
          that.userService.removeRole(changedUser, this.id);
      });

      this.closeChanges();
    }

    closeChanges() {
      this.editUser = false;
      this.change = {};
      this.change.id = -1;
      this.change.name = "";
      this.change.mail = "";
      this.change.password = "";
      this.change.confirmPassword = "";
      this.change.permissions = {};
      this.closeMessage();
    }

    closeMessage() {
      this.message = "";
    }
  }
  angular.module('schwimmfestivalApp')
    .controller('EditUserCtrl', EditUserCtrl);
})();

