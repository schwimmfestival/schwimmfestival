'use strict';

(function() {
  class BuchungenEditCtrl {


    errors = {};
    isEditing = false;


    constructor($state, $http, Auth, $location, $window, buchungsService, billService, utilService) {
      this.$http = $http;
      this.$window = $window;
      this.buchungsService = buchungsService;
      this.$location = $location;
      this.Auth = Auth;
      this.isInit = false;
      this.billService = billService;
      this.utilService = utilService;

      this.id = $state.params.id;
      this.loadBuchung(this.id);
    }

    loadBuchung(id) {
      this.isInit = false;
      this.isEditing = false;
      this.isAddressEditing = false;
      this.buchungsService.loadById(id).then(response => {
        var bu = new Buchung();
        bu.deserialize(response);
        this.buchung = bu;
        this.rechnung = this.buchung.rechnung;
        this.adresse = this.buchung.rechnung.adresse;
        this.isInit = true;
        this.decodeValues();
      });
    }

    decodeValues() {
      this.adresse.vorname = this.utilService.decode(this.adresse.vorname);
      this.adresse.nachname = this.utilService.decode(this.adresse.nachname);
      this.adresse.firma = this.utilService.decode(this.adresse.firma);
      this.adresse.strasse = this.utilService.decode(this.adresse.strasse);
      this.adresse.ort = this.utilService.decode(this.adresse.ort);
    }

    encodeValues() {
      this.adresse.vorname = this.utilService.encode(this.adresse.vorname);
      this.adresse.nachname = this.utilService.encode(this.adresse.nachname);
      this.adresse.firma = this.utilService.encode(this.adresse.firma);
      this.adresse.strasse = this.utilService.encode(this.adresse.strasse);
      this.adresse.ort = this.utilService.encode(this.adresse.ort);
    }

    saveAddress() {
      if (this.Auth.hasRole(UserRights.admin())) {
        this.encodeValues();
        this.buchungsService.updateAdresse(this.buchung).then(response => {
          this.loadBuchung(response.id);
        });
      }
    }

    saveStatus() {
      if (this.buchung.status === "storniert") {
        this.buchungsService.storno(this.buchung).then(response => {
          this.loadBuchung(this.id);
        });
      }
      else {
        this.buchungsService.updateStatus(this.buchung.id, this.buchung.status).then(response => {
          this.loadBuchung(this.id);
        });
      }

    }

    cancelEditing(id) {
      this.loadBuchung(id);
    }

  }
  angular.module('schwimmfestivalApp')
    .controller('BuchungenEditCtrl', BuchungenEditCtrl);
})
();

