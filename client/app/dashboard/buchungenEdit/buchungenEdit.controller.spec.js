'use strict';

describe('Controller: BuchungenEditCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var BuchungenCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    BuchungenCtrl = $controller('BuchungenEditCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
