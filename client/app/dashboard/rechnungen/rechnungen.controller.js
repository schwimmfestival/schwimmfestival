'use strict';

(function() {
  class BillCtrl {

    user = {};
    errors = {};
    picture = {};
    editingData = {};
    images = {};
    add = false;
    savedMail = true;
    billList = [];
    isEditing = false;
    selectedFilter = "alle";
    dt = new Date();

    constructor($scope, $http, Auth, $location, billService, $window) {
      this.$http = $http;
      this.$window = $window;
      this.billService = billService;
      this.$location = $location;
      this.Auth = Auth;
      this.idSelectedRow = null;
      this.sendMail = this.savedMail;
      this.updateList();
    }

    setSelected(id) {
      this.idSelectedRow = id;
    }

    saveMail() {
      this.sendMail = this.savedMail;
    }

    updateList() {
      this.billService.getRechnungsliste(this.selectedFilter).then(response=> {
          this.billList = [];
          for (var i = 0; i < response.rechnungen.length; i++) {
            var rechnung = new Rechnung();
            rechnung.deserialize(response.rechnungen[i]);
            this.billList[i] = rechnung;
            this.editingData[this.billList[i].id] = false;
          }
        }
      );
    }

    modify(rechnung) {
      if (!this.isEditing) {
        this.editingData[rechnung.id] = true;
        this.isEditing = true;
      }
    }

    cancel(rechnung) {
      this.closeEdit(rechnung);
    }

    closeEdit(rechnung) {
      this.dt = new Date();
      this.isEditing = false;
      this.editingData[rechnung.id] = false;
      this.sendMail = this.savedMail;
    }

    update(rechnung) {
      rechnung.setDate(this.dt);
      console.log("Rechnung bezahlt am:" + rechnung.geldeingang);
      this.billService.setRechnungBezahlt(rechnung.id, rechnung.geldeingang, this.sendMail).then(response => {
          this.updateBillList(response);
        this.closeEdit(rechnung);
        }
      );
    }

    storno(rechnung) {
      this.billService.setRechnungStorniert(rechnung.id, this.sendMail).then(response => {
        this.updateBillList(response);
        this.closeEdit(rechnung);
      });

    }

    sendWarning(rechnung, typ) {
      this.billService.sendMahnung(rechnung.id, typ).then(response => {
        this.updateBillList(response);
      });
    }

    updateBillList(rechnung) {
      var newRechnung = new Rechnung();
      newRechnung.deserialize(rechnung);
      for (var i = 0; i < this.billList.length; i++) {
        if (this.billList[i].id == rechnung.id) {
          this.billList[i] = rechnung;
        }
      }
    }

    getLastDate(rechnung) {

      if (rechnung.mahnungen.length == 0) {
        return "";
      }
      var dates = [];
      for (var i = 0; i < rechnung.mahnungen.length; i++) {
        dates[i] = rechnung.mahnungen[i].datum;
      }
      var maxDate = Math.max.apply(Math, dates);
      return new Date(maxDate);
    }

    getName(rechnung) {
      if (rechnung.adresse.id == -1) {
        return "";
      }
      var name = rechnung.adresse.vorname + ' ' + rechnung.adresse.nachname;
      if (rechnung.adresse.firma != "") {
        name = name + ' (' + rechnung.adresse.firma + ')';
      }
      return name;
    }

  }
  angular.module('schwimmfestivalApp')
    .controller('BillCtrl', BillCtrl);
})
();

