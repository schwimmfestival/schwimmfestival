'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'app/dashboard/dashboard.html',
        controller: 'DashboardCtrl',
        authenticate: true
      }).state('dashboard.users', {
      url: '/users',
      templateUrl: 'app/dashboard/editUser/editUsers.html',
      controller: 'EditUserCtrl',
      controllerAs: 'ctrl',
      authenticate: 'admin'
    }).state(
      'dashboard.strecken', {
        url: '/strecken',
        params: {code: ""},
        templateUrl: 'app/dashboard/strecken/strecken.html',
        controller: 'StreckenCtrl',
        controllerAs: 'ctrl',
        authenticate: 'anmeldung'
      }).state(
      'dashboard.buchung', {
        url: '/buchung',
        templateUrl: 'app/dashboard/buchungen/buchungen.html',
        controller: 'BuchungenCtrl',
        controllerAs: 'ctrl',
        authenticate: 'rechnungen'
      }).state(
      'dashboard.buchung.edit', {
        url: '/:id',
        templateUrl: 'app/dashboard/buchungenEdit/buchungenEdit.html',
        controller: 'BuchungenEditCtrl',
        controllerAs: 'ctrl',
        authenticate: 'admin'
      }).state(
      'dashboard.teilnehmer', {
        url: '/teilnehmer',
        templateUrl: 'app/dashboard/teilnehmer/teilnehmer.html',
        controller: 'TeilnehmerCtrl',
        controllerAs: 'ctrl',
        authenticate: true
      }).state(
      'dashboard.teilnehmer.edit', {
        url: '/:id',
        templateUrl: 'app/dashboard/teilnehmerEdit/teilnehmerEdit.html',
        controller: 'TeilnehmerEditCtrl',
        controllerAs: 'ctrl',
        authenticate: true
      }).state(
      'dashboard.bill', {
        url: '/rechnungen',
        templateUrl: 'app/dashboard/rechnungen/rechnungen.html',
        controller: 'BillCtrl',
        controllerAs: 'ctrl',
        authenticate: 'rechnungen'
      }
    );
  });
