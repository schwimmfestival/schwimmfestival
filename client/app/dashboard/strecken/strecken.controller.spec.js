'use strict';

describe('Controller: StreckenCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var AnmeldungCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    AnmeldungCtrl = $controller('StreckenCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
