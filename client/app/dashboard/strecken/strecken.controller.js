'use strict';

(function() {
  class StreckenCtrl {

    constructor($state, $timeout, $uibModal, utilService, challengeService) {
      this.$state = $state;
      this.$timeout = $timeout;
      this.utilService = utilService;
      this.$uibModal = $uibModal;
      this.challengeService = challengeService;
      this.barcode = "";
      this.isChallenge = false;
      this.tnId = -1;
      this.routeId = -1;
      this.bahn = 50;
      this.validErr = false;
      this.success = false;
      this.isFirstInsert = true;
    }

    searchBarcode(form) {
      if (form.$valid) {
        this.barcode.toUpperCase();
        var ids = [];
        if (this.barcode.indexOf('X') > 0) {
          ids = this.barcode.split("X");
          this.getIds(ids);

          this.loadRoute();
        }
        else if (this.barcode.indexOf('Y')) {
          ids = this.barcode.split("Y");
          this.getIds(ids);

          this.loadChallenge();
        }
        else {
          this.clear();
          this.err = true;
          this.fadeOut();
        }
      } else {
        this.clear();
        this.err = true;
        this.fadeOut();
      }
    }

    loadChallenge() {
      this.challengeService.loadChallengeById(this.routeId).then(response => {
        this.challenge = GoeChallenge.deserialize(response);
        this.distance = this.challenge.strecke;
        this.isChallenge = true;
        this.isRoute = false;
        if (this.challenge.istZeit > 20) {
          this.isFirstInsert = false;
        }
      });
    }

    loadRoute() {
      this.challengeService.loadStreckeById(this.routeId).then(response => {
        var r = new Route();
        r.deserialize(response);
        this.route = r;
        this.isRoute = true;
        this.isChallenge = false;
        if (this.route.distance >= 100) {
          this.isFirstInsert = false;
        }
      });
    }

    getIds(ids) {
      this.tnId = parseInt(ids[1]);
      this.tnId = this.tnId - 1000;
      this.routeId = parseInt(ids[0]);
      this.routeId = this.routeId - 1000;
    }

    clear() {
      this.time = "";
      this.barcode = "";
      this.isRoute = false;
      this.isChallenge = false;
      this.tnId = -1;
      this.routeId = -1;
      this.challenge = {};
      this.route = {};
      this.distance = 0;
      this.bahnen = 0;
      this.validErr = false;
      this.timeErr = false;
      this.isFirstInsert = true;
    }

    calc() {
      this.distance = this.bahnen * this.bahn;
    }

    getBahnenValid() {
      return this.bahnen % 2 == 0;
    }

    fadeOut() {
      // Loadind done here - Show message for 3 more seconds.
      this.$timeout(function() {
        return false;
      }, 2000).then(response => {
        this.success = response;
        this.err = response;
      });
    }

    saveRoute(form) {
      if (form.$valid && this.isFirsInsert) {
        if (this.isChallenge) {
          var time = this.utilService.convertToTime(this.time);
          this.challenge.strecke = this.distance;
          this.challenge.istZeit = time;
          if (time > 240000) {
            this.challengeService.setTime(this.challenge).then(response => {
              if (this.challenge.id == response.id) {
                this.clear();
                this.success = true;
                this.fadeOut();
              }
            });
          } else {
            this.timeErr = true;
          }

        } else if (this.isRoute) {
          this.distance = this.bahnen * this.bahn;
          this.route.distance = this.distance;
          if (this.bahnen % 2 == 0 && this.bahnen < 360 && this.bahnen >= 2) {
            this.challengeService.setStrecke(this.route.serialize()).then(response => {
              if (this.route.id == response.id) {
                this.clear();
                this.success = true;
                this.fadeOut();
              }
            });
          }
          else {
            this.validErr = true;
          }
        }
      }
    }
  }
  angular.module('schwimmfestivalApp').controller('StreckenCtrl', StreckenCtrl);
})();


