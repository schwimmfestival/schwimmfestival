'use strict';

describe('Controller: BuchungenCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var BuchungenCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    BuchungenCtrl = $controller('BuchungenCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
