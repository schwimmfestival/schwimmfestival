'use strict';

(function() {
  class BuchungenCtrl {

    user = {};
    errors = {};
    editingData = {};
    dataList = [];
    isEditing = false;
    selectedFilter = "offen";
    dt = new Date();
    validationSend = false;

    constructor($state, $http, Auth, $location, $window, buchungsService) {
      this.$http = $http;
      this.$window = $window;
      this.buchungsService = buchungsService;
      this.$location = $location;
      this.Auth = Auth;
      this.$state = $state;
      this.updateList();
    }

    saveMail() {
      this.sendMail = this.savedMail;
    }

    updateList() {
      this.buchungsService.getLastBuchungenByState(this.selectedFilter).then(response=> {
          this.dataList = [];
          for (var i = 0; i < response.buchungen.length; i++) {
            var buchung = new Buchung();
            buchung.deserialize(response.buchungen[i]);
            this.dataList[i] = buchung;
            this.editingData[this.dataList[i].id] = false;
          }
        }
      );
    }

    modify(buchung) {
      if (!this.isEditing) {
        this.editingData[buchung.id] = true;
        this.isEditing = true;
      }
    }

    delete(buchung) {
      var id = buchung.id;
      console.log("Buchung " + id + " löschen");

      this.buchungsService.storno(buchung);
      var that = this;
      $.each(this.dataList, function(index, element) {
        if (element.id == id) {
          that.dataList.splice(index, 1);
          return false;
        }
      });

      this.closeEdit(buchung);
    }

    closeEdit(buchung) {
      this.dt = new Date();
      this.isEditing = false;
      this.editingData[buchung.id] = false;
      this.sendMail = this.savedMail;
    }

    edit(id) {
      this.$state.go('dashboard.buchung.edit', {id: id });
    }


    sendWarning(buchung) {
      this.buchungsService.validateBesteller(buchung).then(response => {
        this.validationSend = true;
      });
    }

    updateDataList(rechnung) {
      var newRechnung = new Rechnung();
      newRechnung.deserialize(rechnung);
      for (var i = 0; i < this.dataList.length; i++) {
        if (this.dataList[i].id == rechnung.id) {
          this.dataList[i] = rechnung;
        }
      }
    }


  }
  angular.module('schwimmfestivalApp')
    .controller('BuchungenCtrl', BuchungenCtrl);
})
();

