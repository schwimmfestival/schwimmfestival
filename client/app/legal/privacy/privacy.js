'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('privacy', {
        url: '/legal/privacy',
        templateUrl: 'app/legal/privacy/privacy.html',
        controller: 'PrivacyCtrl'
      });
  });
