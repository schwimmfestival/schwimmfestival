'use strict';

describe('Controller: ParticipationCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var ParticipationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    ParticipationCtrl = $controller('ParticipationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
