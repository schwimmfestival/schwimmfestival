'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('participation', {
        url: '/legal/participation',
        templateUrl: 'app/legal/participation/participation.html',
        controller: 'ParticipationCtrl'
      });
  });
