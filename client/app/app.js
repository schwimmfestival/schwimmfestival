'use strict';

angular.module('schwimmfestivalApp', [
  'schwimmfestivalApp.constants',
  'schwimmfestivalApp.auth',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'ui.select',
  'toggle-switch',
  'ui.bootstrap.contextMenu'
])
angular.module('schwimmfestivalApp').config(function($urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(false);
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
  });


