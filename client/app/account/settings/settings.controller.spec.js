'use strict';

describe('Controller: SettingsCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var PicturesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    PicturesCtrl = $controller('SettingsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
