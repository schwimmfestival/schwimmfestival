'use strict';

class SettingsController {
  constructor(Auth, BenutzerService) {
    this.Auth = Auth;
    this.userService = BenutzerService;

    var user = Auth.getCurrentUser();
    this.id = user.id;
    this.name = user.name;
    this.mail = user.mail;
    this.password = user.passwort;
    this.confirmPassword = user.passwort;
    this.message = "";
  }

  changeName() {
    this.userService.updateUserName(this.createUser());
    this.Auth.reloadCurrentUser();
  }

  changeMail() {
    this.userService.updateUserMail(this.createUser());
    this.Auth.reloadCurrentUser();
  }

  changePw() {
    if (this.password != this.confirmPassword) {
      this.message = "Passwörter sind nicht gleich";
      return;
    }
    this.userService.updateUserPw(this.createUser());
  }

  createUser() {
    return {
      id: this.id,
      name: this.name,
      mail: this.mail,
      passwort: this.password
    };
  }

  closeMessage() {
    this.message = "";
  }

}

angular.module('schwimmfestivalApp')
  .controller('SettingsController', SettingsController);
