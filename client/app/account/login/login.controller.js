'use strict';


(function() {

  class LoginController {

    constructor(Auth, $state) {
      this.user = {};
      this.errors = {};
      this.submitted = false;

      this.Auth = Auth;
      this.$state = $state;
    }

    login(form) {
      this.submitted = true;
      console.log("Login Started");
      if (form.$valid) {
        this.Auth.login({
          email: this.user.email,
          password: this.user.password
        })
          .then(() => {
            // Logged in, redirect to home
            this.$state.go('dashboard');
          })
          .catch(err => {
            this.errors.other = err.message;
          });
      }
    }
  }
  angular.module('schwimmfestivalApp')
    .controller('LoginController', LoginController);

})();
