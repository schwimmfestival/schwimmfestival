/**
 * Created by domi on 04.08.16.
 */
'use strict';

class Kategorie {
  constructor(id, name, kuerzel) {
    this.id = id;
    this.name = name;
    this.kuerzel = kuerzel;
  }
}
