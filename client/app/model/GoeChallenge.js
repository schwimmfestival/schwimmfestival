/**
 * Created by domi on 13.03.16.
 */
'use strict';

class GoeChallenge {
  constructor(strecke, sollZeit) {
    this.id = -1;
    this.strecke = strecke;
    this.zeitErwartet = sollZeit;
    this.istZeit = -1;
    this.code = "";
  }

  static deserialize(object) {
    var item = new GoeChallenge(object.strecke, object.zeitErwartet);
    item.id = object.id;
    item.istZeit = object.istZeit;
    item.code = object.code;
    return item;
  }

  getIstZeitReadable() {
    var millis = this.istZeit;
    var minutes = Math.floor(millis / 60000);

    var seconds = ((millis % 60000) / 1000).toFixed(0);
    var tenth = Math.floor(((millis % 60000) % 1000) / 100);
    var hundreth = Math.floor((((millis % 60000) % 1000) % 100) / 10);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds + '.' + tenth + hundreth;
  }

  /**
   *
   * @param time Date
   * @returns {GoeChallenge}
   */
  static four(time) {
    return new GoeChallenge(400, time);
  }

  /**
   *
   * @param time Date
   * @returns {GoeChallenge}
   */
  static eight(time) {
    return new GoeChallenge(800, time)
  }

}
