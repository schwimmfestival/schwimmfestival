/**
 * Created by domi on 09.03.16.
 */
'use strict';
class Participant {

  constructor() {
    this.id = -1;
    this.vorname = '';
    this.nachname = '';
    this.gender = '';
    this.gebDate = '';
    this.fullName = this.getFullName();
    this.einnahmen = [];
    this.challenges = [];
    this.groups = [];
    this.checkIn = "";
    this.routes = [];
    this.invoiceCounter = 0;
  }

  getGroup(family) {
    if (this.groups && this.groups.length > 0) {
      for (var i = 0; i < this.groups.length; i++) {
        if (family && this.groups[i].idKategorie == 2) {
          return this.groups[i];
        } else if (!family && this.groups[i].idKategorie != 2) {
          return this.groups[i]
        }
      }
    }
  }

  getInvoiceSum() {
    var sum = 0;
    for (var key in this.einnahmen)
      sum += Number(Math.round(parseFloat(this.einnahmen[key].preis)+'e2')+'e-2');
    return sum.toFixed(2);
  }

  getInvoiceItem(id) {
    for (var i = 0; i < this.einnahmen.length; i++) {
      if (this.einnahmen[i].id == id)
        return this.einnahmen[i];
    }
    return null;
  }

  hasInvoiceItem(id) {
    return null != this.getInvoiceItem(id);
  }

  isReducedPreSell() {
    return this.hasInvoiceItem(23);
  }

  isNormalPreSell() {
    return this.hasInvoiceItem(21);
  }

  hasFood(id, idVeg) {
    if (this.hasInvoiceItem(id) || this.hasInvoiceItem(idVeg)) {
      return true;
    }
    return false;

  }

  isReduced() {
    if (this.hasInvoiceItem(25) || this.hasInvoiceItem(24) || this.hasInvoiceItem(23)) {
      return true;
    }
    return false;
  }

  hasBreakfast() {
    return this.hasFood(15, 16);
  }

  hasLunch() {
    return this.hasFood(19, 18);
  }

  hasDinner() {
    return this.hasFood(17, 20);
  }

  getStreckeSum() {
    var sum = 0;
    for (var i = 0; i < this.routes.length; i++) {
      if (this.routes[i].distance >= 100) {
        sum = sum + parseInt(this.routes[i].distance);
      }
    }
    return sum;
  }

  getFullName() {
    return this.vorname + ' ' + this.nachname;
  }

  static deserialize(object) {
    var item = new Participant();
    item.id = object.id;
    item.vorname = object.vorname;
    item.nachname = object.nachname;
    item.fullName = item.getFullName();
    item.gender = object.gender;
    item.gebDate = object.gebDate;
    item.checkIn = new Date(object.isDa);
    //Challenges laden
    item.challenges = [];
    for (var i = 0; i < object.challenges.length; i++) {
      item.challenges[i] = GoeChallenge.deserialize(object.challenges[i]);
    }
    // Gruppen laden
    item.groups = [];
    for (var j = 0; j < object.gruppen.length; j++) {
      item.groups[j] = Group.deserialize(object.gruppen[j]);
    }
    // Einnahmen laden
    for (var k = 0; k < object.einnahmen.length; k++) {
      var einnahme = new Artikel();
      einnahme.deserialize(object.einnahmen[k]);
      item.einnahmen[k] = einnahme;
    }
    //Stecken laden
    item.routes = [];
    for (var l = 0; l < object.strecken.length; l++) {
      var route = new Route();
      route.deserialize(object.strecken[l]);
      item.routes[l] = route;
    }

    return item;
  }

}

