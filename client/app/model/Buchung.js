/**
 * Created by domi on 04.08.16.
 */
'use strict';

class Buchung {

  constructor() {
    this.id = -1;
    this.date = "";
    this.status = "offen";
    this.bestCode = "";
    this.versandart = "";
    this.userId = -1;
    this.rechnung = {};
    this.einnahmen = [];
    this.teilnehmer = [];
  }

  deserialize(object) {
    this.id = object.id;
    this.date = new Date(object.date);
    this.date.setHours(this.date.getHours() - 2);
    this.status = object.status;
    this.bestCode = object.bestCode;
    this.versandart = object.versandart;
    this.userId = object.userId;
    var rechnung = new Rechnung();
    rechnung.deserialize(object.rechnung);
    this.rechnung = rechnung;

    for (var i = 0; i < object.einnahmen.length; i++) {
      var einnahme = new EinnahmeBuchung();
      einnahme.deserialize(object.einnahmen[i]);
      this.einnahmen[i] = einnahme;
    }

    for (var j = 0; j < object.teilnehmer.length; j++) {
      var teilnehmer = Participant.deserialize(object.teilnehmer[j]);
      this.teilnehmer[j] = teilnehmer;

    }

  }

  getEmpfaenger() {
    var emp = this.rechnung.adresse.vorname + ' ' + this.rechnung.adresse.nachname;
    if (this.rechnung.adresse.firma != "")
      emp = emp + ' (' + this.rechnung.adresse.firma + ')';

    return emp;
  }
}

class EinnahmeBuchung {
  constructor() {
    this.id = -1;
    this.artikel = -1;
    this.teilnehmer = -1;
  }

  deserialize(object) {
    this.id = object.id;
    this.artikel = object.artikel;
    this.teilnehmer = object.teilnehmer;
  }
}
