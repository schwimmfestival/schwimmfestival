/**
 * Created by domi on 09.03.16.
 */
'use strict';
class Addresse {

  constructor() {
    this.id = -1;
    this.firma = '';
    this.vorname = ""
    this.nachname = '';
    this.gender = '';
    this.ort = '';
    this.strasse = "";
    this.plz = "";
    this.typ = "";
  }


  deserialize(object) {

    this.id = object.id;
    this.firma = object.firma;
    this.vorname = object.vorname;
    this.nachname = object.nachname;
    this.gender = object.gender;
    this.ort = object.ort;
    this.strasse = object.strasse;
    this.plz = object.plz;
    this.typ = object.typ;
  }

}

