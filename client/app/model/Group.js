/**
 * Created by domi on 27.03.16.
 */
/**
 * Created by domi on 13.03.16.
 */
'use strict';

class Group {
  constructor(name, idKategorie) {
    this.id = -1;
    this.name = name;
    this.idKategorie = parseInt(idKategorie);
    this.kategorieName = "";
  }

  static deserialize(object) {
    var item = new Group(object.name, object.idKategorie);
    item.id = object.id;
    item.kategorieName = object.kategorieName;
    return item;
  }
}
