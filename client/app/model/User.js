/**
 * Created by domi on 13.03.16.
 */
'use strict';

class User {
  constructor(name, mail) {
    this.id = -1;
    this.mail = mail;
    this.name = name;
    this.password = "";
    this.rechte = [];
  }

  deserialize(object) {
    this.id = object.id;
    this.name = object.name;
    this.mail = object.mail;
    for (var j = 0; j < object.rechte.rechte.length; j++) {
      var recht = new Recht();
      recht.deserialize(object.rechte.rechte[j]);
      this.rechte[j] = recht;
    }
  }

  hasPermission(id) {
    for (var i = 0; i < this.rechte.length; i++) {
      if (this.rechte[i].id == id)
        return true;
    }
    return false;
  }
}

class Recht {
  constructor() {
    this.id = -1;
    this.name = "";
  }

  deserialize(object) {
    this.id = object.id;
    this.name = object.name;
  }
}
