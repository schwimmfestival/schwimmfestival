/**
 * Created by domi on 09.03.16.
 */
'use strict';
class Rechnung {

  constructor() {
    this.id = -1;
    this.datum = '';
    this.geldeingang = '';
    this.status = '';
    this.idBuchung = '';
    this.mahnungen = [];
    this.versand = "";
    this.adresse = {};
    this.rechnungsbetrag = 0;
  }

  deserialize(object) {
    this.id = object.id;
    this.datum = new Date(object.datum);
    if (object.geldeingang != null) {
      this.geldeingang = new Date(object.geldeingang);
    }
    else {
      this.geldeingang = "";
    }
    this.status = object.status;
    this.idBuchung = object.idBuchung;
    this.mahnungen = [];
    for (var i = 0; i < object.mahnungen.length; i++) {
      var mahnung = new Mahnung();
      mahnung.deserialize(object.mahnungen[i]);
      this.mahnungen[i] = mahnung;
    }
    this.rechnungsbetrag = object.rechnungsbetrag;
    this.versand = object.versand;
    this.adresse = new Addresse();
    this.adresse.deserialize(object.adresse)
  }

  getStatus() {
    if (this.status == 1) {
      return 'offen';
    } else if (this.status == 2) {
      return 'bezahlt';
    } else if (this.status == 3) {
      return 'storniert';
    }
  }

  setStatus(name) {
    if (name == 'offen') {
      this.status = 1;
    } else if (name == 'bezahlt') {
      this.status = 2;
    } else if (name == 'storniert') {
      this.status = 3;
    }
  }

  setDate(date) {
    this.geldeingang = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
  }
}

class Mahnung {
  constructor() {
    this.id = -1;
    this.typ = "";
    this.idRechnung = "";
    this.datum = "";
  }

  deserialize(object) {
    this.id = object.id;
    this.datum = new Date(object.datum);
    this.typ = object.typ;
    this.idRechnung = object.idRechnung;
  }
}

