/**
 * Created by domi on 04.08.16.
 */
'use strict';

class Artikel {

  constructor() {
    this.id = -1;
    this.name = "";
    this.preis = -1;
    this.kategorie = {};
    this.rechnungshinweis = {};
    this.steuer = 0;
  }

  deserialize(object) {
    this.id = object.id;
    this.name = object.name;
    this.preis = object.preis;
    this.kategorie = new Kategorie(object.kategorie.id, object.kategorie.name, object.kategorie.kuerzel);
    this.rechnungshinweis = new Rechnungshinweis(object.rechnungshinweis.id, object.rechnungshinweis.hinweis);
    this.steuer = object.steuer;
  }

}


class Rechnungshinweis {
  constructor(id, hinweis) {
    this.id = id;
    this.hinweis = hinweis;
  }
}
