/**
 * Created by domi on 04.08.16.
 */

'use strict';

class Route {
  constructor() {
    this.id = -1;
    this.distance = -1;
    this.time = "";
    this.code = "";
  }

  deserialize(object) {
    this.id = object.id;
    this.distance = object.strecke;
    this.time = new Date(object.zeitEintrag);
    this.code = object.code;
  }

  serialize() {
    var obj = {};
    obj.id = this.id;
    obj.strecke = this.distance;
    obj.time = "";
    obj.code = "";
    return obj;
  }
}
