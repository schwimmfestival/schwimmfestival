'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('selfservice', {
        url: '/selfservice',
        templateUrl: 'app/selfservice/selfservice.html',
        controller: 'SelfServiceController',
        controllerAs: 'ssCtrl'
      })
        .state('selfservice.listParticipants', {
            url: '/participantList',
            templateUrl: 'app/selfservice/participantList/participantList.html',
            controller: 'ParticipantListController',
            controllerAs: 'listCtrl'
        })
        .state('selfservice.editUser', {
            url: '/editUser',
            templateUrl: 'app/selfservice/editUser/editUser.html',
            controller: 'EditUserController',
            controllerAs: 'usrCtrl'
        });
  });
