'use strict';

(function () {

    class EditUserController {

        constructor($state, transferService, participantService) {
            this.$state = $state;
            this.participantService = participantService;
            this.transferService = transferService;
            this.clicked = false;
        }

        showParticipant() {
            this.clicked = true;
            this.participantService.getParticipants(this.billingNr, this.email).then(response => {
                //response = JSON.parse('{"participants":[{"id":"5","vorname":"Enrike","nachname":"Silling","gebDate":"2006-09-06","gender":"w","einnahmen":[{"id":"3","name":"Fr&uuml;hst&uuml;ck","preis":"2.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"5","name":"Mittagessen","preis":"3","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"7","name":"Abendessen","preis":"4.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"11","name":"Startgeb&uuml;hr erm&auml;&szlig;igt","preis":"4","kategorie":"TN Vorkasse","rechnungshinweis":"2","steuer":"0"}],"challenges":[{"id":"4","strecke":"400","zeitErwartet":"1802100"},{"id":"6","strecke":"800","zeitErwartet":"1802100"}],"groups":[{"id":"5","name":"Schmidt","idKategorie":"2","kategorieName":"Familie"},{"id":"4","name":"IGS","idKategorie":"4","kategorieName":"Schule"}]},{"id":"3","vorname":"Gerda","nachname":"Schildkroete","gebDate":"2006-08-22","gender":"w","einnahmen":[{"id":"3","name":"Fr&uuml;hst&uuml;ck","preis":"2.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"5","name":"Mittagessen","preis":"3","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"7","name":"Abendessen","preis":"4.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"11","name":"Startgeb&uuml;hr erm&auml;&szlig;igt","preis":"4","kategorie":"TN Vorkasse","rechnungshinweis":"2","steuer":"0"}],"challenges":[],"groups":[]},{"id":"4","vorname":"Katarina","nachname":"Fest","gebDate":"2006-08-23","gender":"w","einnahmen":[{"id":"3","name":"Fr&uuml;hst&uuml;ck","preis":"2.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"5","name":"Mittagessen","preis":"3","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"7","name":"Abendessen","preis":"4.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"11","name":"Startgeb&uuml;hr erm&auml;&szlig;igt","preis":"4","kategorie":"TN Vorkasse","rechnungshinweis":"2","steuer":"0"}],"challenges":[],"groups":[]},{"id":"2","vorname":"Miranda","nachname":"Gut","gebDate":"2006-08-21","gender":"w","einnahmen":[{"id":"3","name":"Fr&uuml;hst&uuml;ck","preis":"2.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"5","name":"Mittagessen","preis":"3","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"7","name":"Abendessen","preis":"4.5","kategorie":"TN Essen","rechnungshinweis":"1","steuer":"0"},{"id":"11","name":"Startgeb&uuml;hr erm&auml;&szlig;igt","preis":"4","kategorie":"TN Vorkasse","rechnungshinweis":"2","steuer":"0"}],"challenges":[],"groups":[]}]}');
                this.participantList = [];
                for (var i = 0; i < response.participants.length; i++) {
                    var participant = Participant.deserialize(response.participants[i]);
                    this.participantList.push(participant)
                }
                console.log(this.participantList);
            });
        }
    }
    angular.module('schwimmfestivalApp')
        .controller('EditUserController', EditUserController);

})();
