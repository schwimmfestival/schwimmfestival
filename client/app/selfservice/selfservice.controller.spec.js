'use strict';

describe('Controller: SelfServiceController', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var SelfserviceCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    SelfserviceCtrl = $controller('SelfServiceController', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
