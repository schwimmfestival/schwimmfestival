'use strict';

(function() {

  class SelfServiceController {


    constructor($state, transferService) {
      this.$state = $state;
      this.transferService = transferService;
      this.buchung = [
        {
          'title': 'Teilnehmer einer Buchung',
          'state': 'selfservice.listParticipants'
        }
        /*{
          'title': 'Benutzer bearbeiten',
          'state': 'selfservice.editUser'
         }*/
      ]
    }

    register() {
      this.transferService.setFirstName(this.firstname);
      this.transferService.setLastName(this.lastname);
      this.$state.go('registration');
    }
  }

  angular.module('schwimmfestivalApp')
    .controller('SelfServiceController', SelfServiceController);

})();
