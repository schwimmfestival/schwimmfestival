'use strict';

(function() {

  class MainController {

    constructor($state, $window, transferService) {
      this.$state = $state;
      this.$window = window;
      this.transferService = transferService;
    }

    register() {
      this.transferService.setFirstName(this.firstname);
      this.transferService.setLastName(this.lastname);
      this.$state.go('registration');
    }

    moreInfo() {
      this.$window.open("https://schwimmfestival.de");
    }
  }

  angular.module('schwimmfestivalApp')
    .controller('MainController', MainController);

})();
