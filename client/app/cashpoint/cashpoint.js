'use strict';

angular.module('schwimmfestivalApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('cashpoint', {
        url: '/cashpoint',
        templateUrl: 'app/cashpoint/cashpoint.html',
        controller: 'CashpointCtrl',
        controllerAs: 'cashCtrl'
      })
    ;
  })
  .directive('calcSum', function() {
    return {
      link: function(scope) {
        if (scope.$last) {
          window.setTimeout(function() {
            var sum = 0;
            $(".sum").each(function() {
              sum += parseFloat($(this).text());
            });
            $(".total_sum span").text(Number(Math.round(parseFloat(sum)+'e2')+'e-2').toFixed(2));
            ;
            $("#age").text(sum <= 20 ? "12 Jahre oder älter" : "mind. 18 Jahre alt");
          }, 0);
        }
      }
    };
  });
