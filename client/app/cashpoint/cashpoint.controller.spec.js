'use strict';

describe('Controller: CashpointCtrl', function() {

  // load the controller's module
  beforeEach(module('schwimmfestivalApp'));

  var CashpointCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    CashpointCtrl = $controller('CashpointCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
