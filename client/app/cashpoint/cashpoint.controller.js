'use strict';


(function() {

  class CashpointCtrl {

    static COOKIE = 'participant';

    constructor($http, $window, $uibModal, $state, utilService, transferService, articleService, $cookieStore, participantService) {
      this.$http = $http;
      this.$uibModal = $uibModal;
      this.$state = $state;
      this.utilService = utilService;
      this.transferService = transferService;
      this.participantService = participantService;
      this.showAddressForm = false;
      this.deliveryType = false;
      this.billing = {};
      // Load participants from store to enable going backwards without loosing data;
      if (transferService.getSubscribers().length != 0) {
        this.participantList = transferService.getSubscribers();
        $cookieStore.put(CashpointCtrl.COOKIE, this.participantList);
      }
      else {
        this.participantList = [];
        var tmp = $cookieStore.get(CashpointCtrl.COOKIE);
        if (tmp == undefined) {
          $window.location.href = '#/registration';
        }
        for (var i = 0; i < tmp.length; i++)
          this.participantList[i] = Participant.deserialize(tmp[i]);
      }
      this.setFreeParticipants();

      articleService.getFood().then(response => {
        this.foodList = response;
        this.foodListCount = Object.keys(response).length;
      });
      articleService.getVegiFood().then(response => {
        this.vegieFoodList = response;
      });
      articleService.getPreSell().then(response => {
        this.preSell = response;
      });
      articleService.getBillHint().then(response => {
        this.billHints = response;
      });

      if (this.participantList.length == 1) {
        var participant = this.participantList[0];
        this.firstname = utilService.decode(participant.vorname);
        this.lastname = utilService.decode(participant.nachname);
        this.billing.gender = participant.gender;
        console.log('Gender: ' + participant.gender);
      }
    }

    setFreeParticipants() {
      for (var i = 10; i < this.participantList.length; i += 11) {
        var item = this.participantList[i];
        if (item.isNormalPreSell()) {
          item.getInvoiceItem(9).preis = 0;
        }
        if (item.isReducedPreSell()) {
          item.getInvoiceItem(23).preis = 0;
        }
      }
    }


    goBack() {
      this.transferService.setSubscribers(this.participantList);
    }

    submit(form) {
      this.delivery = $.extend({}, this.billing, this.delivery);

      this.encode();
      this.encodeDeliverytype();
      console.log("Participants: " + JSON.stringify(this.participantList));
      console.log("Billing addr: " + JSON.stringify(this.billing));

      this.participantService.sendOrder(this.participantList, this.billing).then(response => {
        if (response.erfolg == true) {
          this.$state.go('success');
        }
      });
    }

    clearForm(form) {
      this.firstname = "";
      this.lastname = "";
      this.street = "";
      this.city = "";
      this.mail = "";
      this.company = "";
      this.billing.gender = "";
      this.deliveryType = false;
      form.$rollbackViewValue();
      form.$setUntouched();
      form.$setPristine();
    }

    encode() {
      this.billing.firstname = this.utilService.encode(this.firstname);
      this.billing.lastname = this.utilService.encode(this.lastname);
      this.billing.street = this.utilService.encode(this.street);
      this.billing.city = this.utilService.encode(this.city);
      this.billing.mail = this.utilService.encode(this.mail);
      if (this.company && this.company.length > 0) {
        this.billing.company = this.utilService.encode(this.company);
      }
      else {
        this.billing.company = "";
      }
    }

    encodeDeliverytype() {
      if (this.deliveryType) {
        this.billing.deliveryType = "kasse";
      }
      else {
        this.billing.deliveryType = "mail";
      }
    }
  }

  angular.module('schwimmfestivalApp').controller('CashpointCtrl', CashpointCtrl);
})();
