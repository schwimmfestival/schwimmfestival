'use strict';

angular.module('schwimmfestivalApp.auth', [
  'schwimmfestivalApp.constants',
  'schwimmfestivalApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
