/**
 * Created by domi on 04.08.16.
 */
'use strict';

class UserRights {

  static admin() {
    return 'admin';
  }

  static tageskasse() {
    return 'tageskasse';
  }

  static essenskasse() {
    return 'essenskasse';
  }

  static anmeldung() {
    return 'anmeldung';
  }

  static rechnungen() {
    return 'rechnungen';
  }

}

