'use strict';

(function() {

  function UserResource($resource, $rootScope) {
    var service = {

      model: {},

      SaveState: function() {
        localStorage.userService = angular.toJson(service.model);
      },

      RestoreState: function() {
        service.model = angular.fromJson(localStorage.userService);
      },

      DeleteState: function() {
        localStorage.removeItem(localStorage.userService);
      }
    };

    $rootScope.$on("savestate", service.SaveState);
    $rootScope.$on("restorestate", service.RestoreState);

    return service;
  }

  angular.module('schwimmfestivalApp.auth')
    .factory('UserService', UserResource);

})();
