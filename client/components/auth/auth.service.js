'use strict';

(function() {

  function AuthService($rootScope, $location, $http, $cookies, $q, appConfig, Util, UserService) {
    var safeCb = Util.safeCb;
    var currentUser = {};

    var loginUrl = '../../../server/clientFunction/anmeldungUser.php';
    var logutUrl = '../../../server/clientFunction/abmeldungUser.php';
    var createUrl = '../../../server/clientFunction/addUser.php';

    if ($cookies.get('token') && $location.path() !== '/logout') {
      UserService.RestoreState();
      if (!angular.isUndefined(UserService.model)) {
        currentUser = UserService.model;
      }
    }

    var Auth = {

      /**
       * Sets the session cookie. Also validates if session cookie still exists.
       * @param token
       * @returns {boolean}
       */
      cookieValid(token) {
        var expiryDate = new Date();
        expiryDate.setMinutes(expiryDate.getMinutes() + 1);
        if (token === '')
          token = $cookies.get('token');
        if (token === undefined)
          return false;

        $cookies.put('token', token, {
          expires: expiryDate
        });
        return true;
      },

      /**
       * Delete access token and user info
       */
      logout() {
        $cookies.remove('token');
        currentUser = {};
        UserService.DeleteState();
        $http.get(logutUrl);
      },

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional, function(error, user)
       * @return {Promise}
       */
      login({email, password}, callback) {
        return $http.post(loginUrl, {
          mail: email,
          passwort: password
        })
          .then(res => {
            console.log("Login of" + res.data.name + "success!");
            Auth.cookieValid(res.data.session);
            var loggedInUser = new User(res.data.name, res.data.mail);
            loggedInUser.deserialize(res.data);
            currentUser = loggedInUser;
            UserService.model = loggedInUser;
            return currentUser.$promise;
          })
          .then(user => {
            safeCb(callback)(null, user);
            return user;
          })
          .catch(err => {
            Auth.logout();
            safeCb(callback)(err.data);
            return $q.reject(err.data);
          });
      },

      /**
       * Gets all available info on a user
       *   (synchronous|asynchronous)
       *
       * @param  {Function|*} callback - optional, funciton(user)
       * @return {Object|Promise}
       */
      getCurrentUser(callback) {
        if (arguments.length === 0) {
          return currentUser;
        }

        var value = (currentUser.hasOwnProperty('$promise')) ?
          currentUser.$promise : currentUser;
        return $q.when(value)
          .then(user => {
            safeCb(callback)(user);
            return user;
          }, () => {
            safeCb(callback)({});
            return {};
          });
      },

      /**
       * Reloads the data of the current user, as the user has changed his data.
       */
      reloadCurrentUser() {
        //TODO implement. See #131
      },

      /**
       * Check if a user is logged in
       *   (synchronous|asynchronous)
       *
       * @param  {Function|*} callback - optional, function(is)
       * @return {Bool|Promise}
       */
      isLoggedIn(callback) {
        if (!Auth.cookieValid('')) {
          //TODO hier müsste der Benutzer abgemeldet werden...
          return false;
        }

        if (arguments.length === 0) {
          return currentUser.hasOwnProperty('rechte');
        }

        return Auth.getCurrentUser(null)
          .then(user => {
            var is = user.hasOwnProperty('rechte');
            safeCb(callback)(is);
            return is;
          });
      },

      /**
       * Check if a user has a specified role or higher
       *   (synchronous|asynchronous)
       *
       * @param  {String}     role     - the role to check against
       * @param  {Function|*} callback - optional, function(has)
       * @return {Bool|Promise}
       */
      hasRole(role, callback) {
        if (!Auth.cookieValid('')) {
          document.location.href="/";
          return false;
        }

        if (arguments.length < 2) {
          var has = false;
          for (var i = 0; i < currentUser.rechte.length; i++) {
            if (currentUser.rechte[i].name === role) {
              has = true;
              break;
            }
          }
          return has;
        }

        return Auth.getCurrentUser(null)
          .then(user => {
            var has = false;
            if (user && user.rechte) {
              for (var i = 0; i < user.rechte.length; i++) {
                if (user.rechte[i].name === role) {
                  has = true;
                  break;
                }
              }
            }
            safeCb(callback)(has);
            return has;
          });
      },

      /**
       * Check if a user is an admin
       *   (synchronous|asynchronous)
       *
       * @param  {Function|*} callback - optional, function(is)
       * @return {Bool|Promise}
       */
      isAdmin() {
        return Auth.hasRole
          .apply(Auth, [].concat.apply(['admin'], arguments));
      }
    };

    return Auth;
  }

  angular.module('schwimmfestivalApp.auth')
    .factory('Auth', AuthService);

})();
