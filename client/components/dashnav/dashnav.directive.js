'use strict';

angular.module('schwimmfestivalApp')
  .directive('dashnav', function() {
    return {
      templateUrl: 'components/dashnav/dashnav.html',
      restrict: 'EA',
      controller: 'DashnavController',
      controllerAs: 'dashNav',
      link: function(scope, element, attrs) {
      }
    };
  });
