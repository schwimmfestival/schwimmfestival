/**
 * Created by domi on 14.02.16.
 */

'use strict';

class DashnavController {
  menu = [{
    'title': 'Anmeldung',
    'state': 'anmeldung',
    'role': 'anmeldung'
  }, {
    'title': 'Teilnehmer suchen',
    'state': 'dashboard.teilnehmer',
    'role': 'anmeldung'

  }, {
    'title': 'Strecken eintragen',
    'state': 'dashboard.strecken',
    'role': 'anmeldung'
  }, {
    'title': 'Buchungen bearbeiten',
    'state': 'dashboard.buchung',
    'role': 'rechnungen'

  }, {
    'title': 'Rechnungen bearbeiten',
    'state': 'dashboard.bill',
    'role': 'rechnungen'
  }, {
    'title': 'Benutzer bearbeiten',
    'state': 'dashboard.users',
    'role': 'admin'
  }
  ];

  constructor(Auth) {
    this.Auth = Auth;
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }

  hasRole(name) {

    var has = this.Auth.hasRole(name);
    return has;
  }
}

angular.module('schwimmfestivalApp')
  .controller('DashnavController', DashnavController);
