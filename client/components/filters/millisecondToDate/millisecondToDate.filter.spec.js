'use strict';

describe('Filter: millisecondToDate', function() {

  // load the filter's module
  beforeEach(module('schwimmfestivalApp'));

  // initialize a new instance of the filter before each test
  var millisecondToDate;
  beforeEach(inject(function($filter) {
    millisecondToDate = $filter('millisecondToDate');
  }));

  it('should return the input prefixed with "millisecondToDate filter:"', function() {
    var text = 'angularjs';
    expect(millisecondToDate(text)).toBe('millisecondToDate filter: ' + text);
  });

});
