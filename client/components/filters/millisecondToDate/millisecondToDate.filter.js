'use strict';

angular.module('schwimmfestivalApp')
  .filter('millisecondToDate', function() {
    return function(milliseconds) {
      return new Date(1970, 0, 1).setMilliseconds(milliseconds);
    };
  });
