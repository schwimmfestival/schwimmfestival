'use strict';

angular.module('schwimmfestivalApp')
  .service('billService', function($http, utilService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var url = '../../../server/clientFunction/rechnung.php';

    /**
     *
     * @param listType
     * Ausgewetet werden alle, offen, storniert, bezahlt
     * @returns {*}
     */
    this.getRechnungsliste = function(listType) {
      return $http.get(url, {params: {"rechnungsliste": listType}}).then(response => {
        return response.data;
      });
    };

    this.getBuchung = function(buchungsnummer) {
      return $http.get(url, {params: {"buchung": buchungsnummer}}).then(response => {
        return response.data;
      });
    };

    this.getRechnung = function(rechnungsnummer) {
      return $http.get(url, {params: {"rechnung": rechnungsnummer}}).then(response => {
        return response.data;
      });
    };

    this.setRechnungBezahlt = function(rechnungsnummer, date, sendmail) {
      return $http.get(url, {
        params: {
          "bezahlt": rechnungsnummer,
          "geldeingang": date,
          "isMail": sendmail
        }
      }).then(response => {
        return response.data;
      });
    };

    this.setRechnungStorniert = function(rechnungsnummer, sendmail) {
      return $http.get(url, {params: {"storniert": rechnungsnummer, "isMail": sendmail}}).then(response => {
        return response.data;
      });
    };

    this.sendMahnung = function(rechnungsnummer, typ) {
      return $http.get(url, {params: {"mahnung": rechnungsnummer, "typ": typ}}).then(response => {
        return response.data;
      });
    };


    this.updateRechnung = function(rechnungsnummer) {
      return $http.get(url, {params: {"rechnung": rechnungsnummer, "geldeingang": date}}).then(response => {
        return response.data;
      });
    };

  });
