'use strict';

angular.module('schwimmfestivalApp')
  .service('articleService', function($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var url = '../../../server/clientFunction/getArtikel.php';
    var hintUrl = '../../../server/clientFunction/getRechnungshinweis.php';
    var categoryUrl = url + '?kategorie=';

    var Categpry = {
      TN_ESSEN: 1,
      TN_VEG_ESSEN: 2,
      TN_VORKASSE: 3,
      TN_TAGESKASSE: 4
    };


    this.getArticles = function() {
      return $http.get(url, {cache: true}).then(response => {

        return response.data;
      });
    };


    this.getArticleByCategory = function(category) {
      return $http.get(categoryUrl + category, {cache: true}).then(response => {

        return response.data;
      });
    };

    this.getFood = function() {
      return $http.get(categoryUrl + Categpry.TN_ESSEN, {cache: true}).then(response => {

        return response.data;
      });
    };

    this.getVegiFood = function() {
      return $http.get(categoryUrl + Categpry.TN_VEG_ESSEN, {cache: true}).then(response => {

        return response.data;
      });
    };

    this.getPreSell = function() {
      return $http.get(categoryUrl + Categpry.TN_VORKASSE, {cache: true}).then(response => {

        return response.data;
      });
    };

    this.getDaySell = function() {
      return $http.get(categoryUrl + Categpry.TN_TAGESKASSE, {cache: true}).then(response => {

        return response.data;
      });
    };

    this.getBillHint = function() {
      return $http.get(hintUrl, {cache: true}).then(response => {

        return response.data;
      });
    };


  });
