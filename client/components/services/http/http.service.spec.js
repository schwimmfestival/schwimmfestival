'use strict';

describe('Service: httpService', function() {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var http;
  beforeEach(inject(function(_http_) {
    http = _http_;
  }));

  it('should do something', function() {
    expect(!!http).toBe(true);
  });

});
