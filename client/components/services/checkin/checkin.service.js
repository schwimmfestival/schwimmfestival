'use strict';

angular.module('schwimmfestivalApp')
  .service('checkinService', function ($http, Auth) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var checkinUrl = '../../../server/clientFunction/teilnehmer.php';
    var error = {state: 'error', msg: 'Sie besitzen nicht die nötigen Rechte!'}

    /**
     * Checkt den Code ein und liefert einen Status für den Code.
     *
     * @param code
     */
    this.checkTN = function (code) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.get(checkinUrl, {params: {"checkIn": code}}).then(response=> {
          console.log(response.data);
          return response.data;
        });
      }
    };
  });
