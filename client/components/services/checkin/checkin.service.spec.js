'use strict';

describe('Service: checkinService', function() {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var checkin;
  beforeEach(inject(function(_checkin_) {
    checkin = _checkin_;
  }));

  it('should do something', function() {
    expect(!!checkin).toBe(true);
  });

});
