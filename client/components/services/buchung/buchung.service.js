'use strict';

angular.module('schwimmfestivalApp')
  .service('buchungsService', function($http, Auth) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var buchungUrl = '../../../server/clientFunction/buchung.php';
    var buchungenUrl = '../../../server/clientFunction/buchungen.php';
    var error = {state: 'error', msg: 'Sie besitzen nicht die nötigen Rechte!'}

    /**
     * Gibt die angegebene Anzahl an Buchungen für den eingeloggten User zurück
     *
     * @param anzahl
     */
    this.getLastBuchungenByUser = function(anzahl) {
      if (isNaN(anzahl)) {
        anzahl = 5;
      }
      if (Auth.hasRole(UserRights.tageskasse()) || Auth.hasRole(UserRights.rechnungen)) {
        return $http.get(buchungenUrl, {params: {"task": "listOfUser", "anzahl": anzahl}}).then(response=> {
          return response.data;
        });
      }
    };

    /**
     * Gibt alle Buchungen nach dem Status zurück
     * @param status <offen|rechnung|storniert|barkasse>
     */
    this.getLastBuchungenByState = function(status) {
      if (Auth.hasRole(UserRights.rechnungen())) {
        return $http.get(buchungenUrl, {params: {"task": "listByStatus", "status": status}}).then(response=> {
          return response.data;
        });
      }
    };


    /**
     * Storniert eine Buchung
     * @param buchung erwartet ein Buchungsobjekt
     */
    this.storno = function(buchung) {
      if (Auth.hasRole(UserRights.rechnungen()) || Auth.hasRole(UserRights.tageskasse())) {
        return $http.post(buchungUrl + '?task=stornieren', buchung).then(response=> {
          return response.data;
        });
      }
    };

    /**
     * Wird genutzt um einnahmen aus der Tageskasse zu speichern
     * @param buchung
     */
    this.insertDaySale = function(buchung) {
      if (Auth.hasRole(UserRights.tageskasse())) {
        return $http.post(buchungUrl + '?task=tageskasse', buchung).then(response => {
          return response.data
        });
      }
    };

    /**
     * Wird aufgerufen um einen Besteller zu validieren
     * @param buchung
     */
    this.validateBesteller = function(buchung) {
      if (Auth.hasRole(UserRights.rechnungen())) {
        return $http.post(buchungUrl + '?task=validateBesteller', buchung).then(response => {
          return response.data;
        });
      }
    };

    /**
     * Lädt eine Buchung durch eine id
     * @param id
     */
    this.loadById = function(id) {
      if (Auth.hasRole(UserRights.tageskasse()) || Auth.hasRole(UserRights.rechnungen()) || Auth.hasRole(UserRights.anmeldung())) {
        return $http.get(buchungUrl, {params: {"task": "loadById", "id": id}}).then(response => {
          return response.data;
        });
      }
    };

    /**
     * Wird genutzt um den Status einer Buchung zu ändern.
     * @param id
     * @param status
     * @returns {*}
     */
    this.updateStatus = function(id, status) {
      if (Auth.hasRole(UserRights.rechnungen())) {
        return $http.get(buchungUrl, {params: {"buchung": id, "status": status}}).then(response => {
          return response.data;
        })
      }
    };

    /**
     * Kann genutzt werden um eine neue Rechnungsadresse einzugeben
     * @param buchung
     * @returns {*}
     */
    this.updateAdresse = function(buchung) {
      {
        if (Auth.hasRole(UserRights.admin())) {
          return $http.post(buchungUrl + '?task=updateRechnungsadresse', buchung).then(response => {
            return response.data;
          })
        }
      }

    }
  });
