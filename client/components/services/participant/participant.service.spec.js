'use strict';

describe('Service: participantService', function () {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var participent;
  beforeEach(inject(function (_participent_) {
    participent = _participent_;
  }));

  it('should do something', function () {
    expect(!!participent).toBe(true);
  });

});
