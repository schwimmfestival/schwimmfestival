'use strict';

angular.module('schwimmfestivalApp')
  .service('participantService', function($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var createUrl = '../../../server/clientFunction/setBestellungVk.php';
    var getParticipantsUrl = '../../../server/clientFunction/getTnListe.php';
    var getParticipantUrl = '../../../server/clientFunction/getTeilnehmer.php';
    var updateParticipantUrl = '../../../server/clientFunction/updateTeinehmer.php';

    var teilnehmerRESTUrl = '../../../server/clientFunction/teilnehmer.php';
    var teilnehmerSucheRESTUrl = '../../../server/clientFunction/teilnehmerSuche.php';

    this.sendOrder = function(participant, billing) {
      var order = {participant, billing};

      return $http.post(createUrl, order).then(response => {
        return response.data;
      });
    };

    this.getParticipants = function(billingNr, email) {
      var data = {
        rechnung: billingNr,
        mail: email
      };
      return $http.post(getParticipantsUrl, data).then(response => {
        return response.data;
      });
    };

    this.getParticipant = function(participantNr, date) {
      var data = {
        id: participantNr,
        gebDate: date
      };
      return $http.post(getParticipantUrl, data).then(response => {
        return response.data;
      });
    };

    this.updateParticipant = function(participant) {
      return $http.post(teilnehmerRESTUrl + '?update=true', participant).then(response => {
        return response.data;
      });
    };

    this.updateParticipantGroups = function(id, gebDate, groups) {
      var data = {
        id: id,
        gebDate: gebDate,
        groups: groups
      };
      return $http.post(updateParticipantUrl, data).then(response => {
        return response.data;
      });
    };

    this.updateParticipantChallenges = function(id, gebDate, challenges) {
      var data = {
        id: id,
        gebDate: gebDate,
        challenges: challenges
      };
      return $http.post(updateParticipantUrl, data).then(response => {
        return response.data;
      });
    };

    this.addGroup = function(tnId, groupId) {
      return $http.get(teilnehmerRESTUrl, {params: {'teilnehmer': tnId, 'gruppeadd': groupId}}).then(response => {
        return response.data;
      });

    };

    this.removeGroup = function(tnId, groupId) {
      return $http.get(teilnehmerRESTUrl, {params: {'teilnehmer': tnId, 'gruppeloeschen': groupId}}).then(response => {
        return response.data;
      });
    };

    this.search = function(searchString) {
      return $http.get(teilnehmerSucheRESTUrl, {params: {'task': 'suche', 'string': searchString}}).then(response => {
        return response.data;
      });
    };

    this.loadByID = function(id) {
      return $http.get(teilnehmerRESTUrl, {params: {'teilnehmer': id}}).then(response => {
        return response.data;
      })
    };


  });
