'use strict';

describe('Service: transferService', function() {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var transfer;
  beforeEach(inject(function(_transfer_) {
    transfer = _transfer_;
  }));

  it('should do something', function() {
    expect(!!transfer).toBe(true);
  });

});
