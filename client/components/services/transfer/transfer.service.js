'use strict';

angular.module('schwimmfestivalApp')
  .service('transferService', function() {
    var subscribers = [];

    this.setSubscribers = function(subscriberList) {
      subscribers = subscriberList;
    };

    this.addSubscriber = function(subscriber) {
      subscribers.push(subscriber);
    };

    this.getSubscribers = function() {
      return subscribers;
    };

    this.getSubscriber = function(idx) {
      return subscribers[idx];
    };

    var firstname;
    var lastname;

    this.getFirstName = function() {
      return this.firstname;
    }
    this.setFirstName = function(firstname) {
      this.firstname = firstname;
    }

    this.getLastName = function() {
      return this.lastname;
    }

    this.setLastName = function(lastname) {
      this.lastname = lastname;
    }
  });
