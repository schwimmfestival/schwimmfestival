'use strict';

angular.module('schwimmfestivalApp')
  .service('utilService', function($window) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    /**
     * This method accept strings in the given formats below and return a JavaScript Date.
     * - dd.MM.yyyy
     * - dd-MM-yyyy
     * - dd/MM/yyyy
     *
     * @param dateString
     * @returns {Date} / undefined if error happens
     */
    this.convertToDate = function(dateString) {
      var date;
      if (dateString.includes('.')) {
        date = createDate(dateString, '.');
      } else if (dateString.includes('-')) {
        date = createDate(dateString, '-');
      }
      else if (dateString.includes('/')) {
        date = createDate(dateString, '/');
      }

      return date;
    };

    /**
     * This method accept strings in the given formats below and return a JavaScript Date.
     * Only use it to crate a time with minutes and seconds.
     * - mm:ss
     *
     * @param timeString
     * @returns number representing milliseconds / undefined if error happens
     */
    this.convertToTime = function(timeString) {
      if (timeString.includes(':')) {
        var nirvana = 0;
        var hundreth = 0;
        var tenth = 0;
        var seconds = 0;
        var timeArray = timeString.split(':');
        if (timeString.includes('.')) {
          var secondArray = timeArray[1].split('.');
          seconds = parseInt(secondArray[0]);
          nirvana = secondArray[1];

          tenth = parseInt(nirvana.charAt(0));
          tenth = tenth * 100;

          hundreth = parseInt(nirvana.charAt(1));
          hundreth = hundreth * 10;

        }
        else {
          seconds = parseInt(timeArray[1]);
        }

        var minutes = parseInt(timeArray[0]);
        seconds = seconds * 1000;
        minutes = minutes * 60000;

        return minutes + seconds + tenth + hundreth;
      }
    };

    this.millisToMinutesAndSeconds = function(millis) {
      var millis = this.istZeit;
      var minutes = Math.floor(millis / 60000);

      var seconds = ((millis % 60000) / 1000).toFixed(0);
      var tenth = Math.floor(((millis % 60000) % 1000) / 100);
      var hundreth = Math.floor((((millis % 60000) % 1000) % 100) / 10);
      return minutes + ":" + (seconds < 10 ? '0' : '') + seconds + '.' + tenth + hundreth;
    };

    function createDate(dateString, delimiter) {
      var dateArray = dateString.split(delimiter);
      var date = new Date(Date.UTC(dateArray[2], dateArray[1] - 1, dateArray[0]));
      return date;
    };

    this.toJSON = function(jsObject) {
      return JSON.stringify(jsObject);
    };

    this.encode = function(string) {
      return $window.he.encode(string, {
        'useNamedReferences': true
      });
    };

    this.decode = function(string) {
      return $window.he.decode(string);
    };

  });
