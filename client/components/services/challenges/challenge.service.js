'use strict';

angular.module('schwimmfestivalApp')
  .service('challengeService', function($http, Auth) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var challengeRESTUrl = '../../../server/clientFunction/challenge.php';
    var streckenRESTUrl = '../../../server/clientFunction/strecke.php';
    var error = {state: 'error', msg: 'Sie besitzen nicht die nötigen Rechte!'};

    this.addChallenge = function(teilnehmerId, challenge) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.post(challengeRESTUrl + '?task=add&teilnehmer=' + teilnehmerId, challenge).then(response => {
          return response.data;
        });
      }
      return error;
    };

    this.updateChallenge = function(challenge) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.post(challengeRESTUrl + '?task=update', challenge).then(response => {
          return response.data;
        });
      }
      return error;
    };

    this.deleteChallenge = function(challenge) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.post(challengeRESTUrl + '?task=delete', challenge).then(response => {
          return response.data;
        });
      }
      return error;
    };

    this.loadChallengeById = function(id) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.get(challengeRESTUrl, {params: {task: 'loadById', id: id}}).then(response => {
          return response.data;
        });
      }
      return error;
    };


    this.setTime = function(challenge) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.post(challengeRESTUrl + '?task=setIstZeit', challenge).then(response => {
          return response.data;
        });
      }
      return error;
    };

    this.setStrecke = function(strecke) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.post(streckenRESTUrl + '?task=setStrecke', strecke).then(response => {
          return response.data;
        });
      }
      return error;
    };

    this.loadStreckeById = function(id) {
      if (Auth.hasRole(UserRights.anmeldung())) {
        return $http.get(streckenRESTUrl, {params: {task: 'loadById', id: id}}).then(response => {
          return response.data;
        });
      }
    };

  });
