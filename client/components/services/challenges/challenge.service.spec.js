'use strict';

describe('Service: challengeService', function() {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var buchung;
  beforeEach(inject(function(_buchung_) {
    buchung = _buchung_;
  }));

  it('should do something', function() {
    expect(!!buchung).toBe(true);
  });

});
