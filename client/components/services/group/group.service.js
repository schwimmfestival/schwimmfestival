'use strict';

angular.module('schwimmfestivalApp')
    .service('groupService', function($http, $httpParamSerializer) {
        // AngularJS will instantiate a singleton by calling "new" on this function
      var url = '../../../server/clientFunction/getGroups.php';
      var groupCategoryUrl = '../../../server/clientFunction/getGroupCategory.php';
      var createUrl = '../../../server/clientFunction/setGroup.php';
      var groupCategoryIdFamily = 2;
        this.getGroups = function() {
            return $http.get(url, {cache: false}).then(response => {
                console.log('Groups: ' + JSON.stringify(response.data));
                return response.data;
            });
        };

        this.getGroupCategories = function() {
            return $http.get(groupCategoryUrl, {cache: true}).then(response => {
                console.log('Group Categories: ' + JSON.stringify(response.data));
                return response.data;
            });
        };

        this.getGroupsCategorized = function() {
            return $http.get(url + '?kategorie', {cache: true}).then(response => {
                console.log('Group Categories: ' + JSON.stringify(response.data));
                return response.data;
            });
        };

        this.createGroup = function(groupIn) {
            return $http.post(createUrl, groupIn).then(response => {
                var newGroup = Group.deserialize(response.data);
                console.log(newGroup);
                return newGroup;
            });
        };

        this.getFamilies = function() {
          return $http.get(url, {cache: true, params: {'kategorie': groupCategoryIdFamily}}).then(response => {
                return response.data;
            });
        };

      this.getGroupsWithoutFamily = function() {

        return $http.get(url, {cache: true, params: {'nichtKategorie': groupCategoryIdFamily}}).then(response => {
          return response.data;
        });

      }

    });
