'use strict';

describe('Service: benutzer', function() {

  // load the service's module
  beforeEach(module('schwimmfestivalApp'));

  // instantiate service
  var benutzer;
  beforeEach(inject(function(_benutzer_) {
    benutzer = _benutzer_;
  }));

  it('should do something', function() {
    expect(!!benutzer).toBe(true);
  });

});
