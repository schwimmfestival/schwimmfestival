'use strict';
(function() {
  class BenutzerService {
    url = '../../../server/clientFunction/benutzer.php';
    url2 = '../../../server/clientFunction/benutzerListe.php';

    constructor($http, Auth) {
      this.$http = $http;
      this.auth = Auth;
    }

    /**
     * Sucht einen Benutzer aus der Datenbank.
     * Benoetigt adminreachte
     * @param search
     */
    searchUser(search) {
      if (this.auth.hasRole(UserRights.admin())) {
        return this.$http.get(this.url2 + '?search=' + search).then(response => {
            return response.data;
          }
        );
      }
    }

    /**
     * Einen neuen Benutzer anlegen
     * Benoetigt adminrechte
     * @param user
     * @returns {Benutzer}
     */
    addUser(user) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=add', user).then(response => {
            return response.data;
          }
        );
    }

    /**
     * Einem Benutzer eine neue Rolle geben
     * @param user
     * @param roleId
     * @returns {*}
     */
    addRole(user, roleId) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=addRecht&recht=' + roleId, user).then(response => {
            return response.data;
          }
        );
    }

    /**
     * Einem Benutzer ein recht enziehen
     * @param user
     * @param roleId
     */
    removeRole(user, roleId) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=removeRecht&recht=' + roleId, user).then(response => {
            return response.data;
          }
        );
    }

    /**
     * Zum setzten des Passworts eines anderen Benutzers
     * Benötigt Admin rechte.
     * @param user
     * @returns {*}
     */
    updateUserPw(user) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=updateUserPw', user).then(response => {
            return response.data;
          }
        );
    }

    /**
     * Zum setzten des Names eines anderen Benutzers
     * Benötigt Admin rechte.
     * @param user
     * @returns {*}
     */
    updateUserName(user) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=updateUserName', user).then(response => {
            return response.data;
          }
        );
    }

    /**
     * Zum setzten der Email  eines anderen Benutzers
     * Benötigt Admin rechte.
     * @param user
     * @returns {*}
     */
    updateUserMail(user) {
      if (this.auth.hasRole(UserRights.admin()))
        return this.$http.post(this.url + '?task=updateUserMail', user).then(response => {
            return response.data;
          }
        );
    }

    updatePw(user) {
      if (this.auth.isLoggedIn())
        return this.$http.post(this.url + '?task=updatePw', user).then(response => {
            return response.data;
          }
        );
    }

    updateName(user) {
      if (this.auth.isLoggedIn())
        return this.$http.post(this.url + '?task=updateName', user).then(response => {
            return response.data;
          }
        );
    }

    updateMail(user) {
      if (this.auth.isLoggedIn())
        return this.$http.post(this.url + '?task=updateMail', user).then(response => {
            return response.data;
          }
        );
    }

  }
  angular.module('schwimmfestivalApp')
    .service('BenutzerService', BenutzerService);
})();
