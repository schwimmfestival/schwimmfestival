'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'title': 'Home',
    'state': 'main'
  },
    {
      'title': 'Anmeldung',
      'state': 'registration'
    }, {
      'title': 'SB Funktionen',
      'state': 'selfservice'
    }];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

angular.module('schwimmfestivalApp')
  .controller('NavbarController', NavbarController);
