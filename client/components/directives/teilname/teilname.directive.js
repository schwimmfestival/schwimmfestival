'use strict';

angular.module('schwimmfestivalApp')
  .directive('teilname', function() {
    return {
      templateUrl: 'components/directives/teilname/teilname.html',
      restrict: 'E',
      link: function(scope, element, attrs) {
      }
    };
  });
