'use strict';

describe('Directive: teilname', function() {

  // load the directive's module and view
  beforeEach(module('schwimmfestivalApp'));
  beforeEach(module('components/directives/teilname/teilname.html'));

  var element, scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function($compile) {
    element = angular.element('<teilname></teilname>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the teilname directive');
  }));
});
