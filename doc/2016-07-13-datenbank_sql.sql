-- MySQL Script generated by MySQL Workbench
-- 07/13/16 15:29:05
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dlrg_goettingen_24h
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dlrg_goettingen_24h
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dlrg_goettingen_24h` DEFAULT CHARACTER SET utf8 ;
USE `dlrg_goettingen_24h` ;

-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`besteller`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`besteller` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`besteller` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pw` VARCHAR(255) NOT NULL,
  `isActive` TINYINT(4) NULL DEFAULT NULL COMMENT '1: Email unbestätigt\n2: Email bestätigt',
  `email` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 113;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`adresse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`adresse` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`adresse` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `strasse` VARCHAR(200) NOT NULL,
  `plz` VARCHAR(10) NOT NULL,
  `ort` VARCHAR(100) NOT NULL,
  `typ` VARCHAR(20) NOT NULL DEFAULT 'Rechnungsanschrift',
  `besteller_id` INT(11) NOT NULL,
  `vorname` VARCHAR(200) NOT NULL,
  `nachname` VARCHAR(200) NOT NULL,
  `geschlecht` VARCHAR(2) NOT NULL,
  `firma` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_adresse_besteller1_idx` (`besteller_id` ASC),
  CONSTRAINT `fk_adresse_besteller1`
    FOREIGN KEY (`besteller_id`)
    REFERENCES `dlrg_goettingen_24h`.`besteller` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 112;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`user` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `mail` VARCHAR(150) NOT NULL,
  `pw` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`buchung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`buchung` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`buchung` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` VARCHAR(30) NULL DEFAULT NULL,
  `versandart` VARCHAR(5) NOT NULL,
  `bestcode` VARCHAR(10) NULL DEFAULT NULL,
  `besteller_id` INT(11) NULL COMMENT 'Bei Buchungen an der Tageskasse wird erst bei einem Rechnungsbetrag > 150 Euro ein Besteller benötigt',
  `user_id` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_buchung_besteller1_idx` (`besteller_id` ASC),
  INDEX `fk_buchung_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_buchung_besteller1`
    FOREIGN KEY (`besteller_id`)
    REFERENCES `dlrg_goettingen_24h`.`besteller` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_buchung_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `dlrg_goettingen_24h`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`gruppe_typ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`gruppe_typ` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`gruppe_typ` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`gruppe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`gruppe` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`gruppe` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `gruppe_typ_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_gruppe_gruppe_typ1_idx` (`gruppe_typ_id` ASC),
  CONSTRAINT `fk_gruppe_gruppe_typ1`
    FOREIGN KEY (`gruppe_typ_id`)
    REFERENCES `dlrg_goettingen_24h`.`gruppe_typ` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`gutschein`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`gutschein` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`gutschein` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(15) NOT NULL,
  `isactive` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`helfer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`helfer` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`helfer` (
  `id` INT(11) NOT NULL,
  `teilnehmernr` INT(11) NOT NULL,
  `vorname` VARCHAR(250) NOT NULL,
  `nachname` VARCHAR(250) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `telefon` VARCHAR(250) NOT NULL,
  `alter` INT(11) NOT NULL,
  `stunden` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`helferAufgaben`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`helferAufgaben` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`helferAufgaben` (
  `id` INT(11) NOT NULL,
  `helfer` INT(11) NOT NULL,
  `Aufgabe` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`helferZeit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`helferZeit` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`helferZeit` (
  `id` INT(11) NOT NULL,
  `helfer` INT(11) NOT NULL,
  `von` TIME NOT NULL,
  `bis` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`rechnung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`rechnung` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`rechnung` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `datum` DATE NOT NULL,
  `geldeingang` DATE NULL DEFAULT NULL,
  `kartenstatus` INT(11) NOT NULL DEFAULT '1',
  `buchung_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rechnung_buchung1_idx` (`buchung_id` ASC),
  UNIQUE INDEX `buchung_id_UNIQUE` (`buchung_id` ASC),
  CONSTRAINT `fk_rechnung_buchung1`
    FOREIGN KEY (`buchung_id`)
    REFERENCES `dlrg_goettingen_24h`.`buchung` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`teilnehmer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`teilnehmer` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`teilnehmer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `vorname` VARCHAR(200) NOT NULL,
  `nachname` VARCHAR(250) NOT NULL,
  `gebdat` DATE NOT NULL,
  `gender` VARCHAR(10) NOT NULL,
  `kartentyp` VARCHAR(20) NULL,
  `buchung_id` INT(11) NOT NULL,
  `istDa` DATETIME NULL,
  `gutschein_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_teilnehmer_buchung1_idx` (`buchung_id` ASC),
  INDEX `fk_teilnehmer_gutschein1_idx` (`gutschein_id` ASC),
  CONSTRAINT `fk_teilnehmer_buchung1`
    FOREIGN KEY (`buchung_id`)
    REFERENCES `dlrg_goettingen_24h`.`buchung` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_teilnehmer_gutschein1`
    FOREIGN KEY (`gutschein_id`)
    REFERENCES `dlrg_goettingen_24h`.`gutschein` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`tnStrecke`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`tnStrecke` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`tnStrecke` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `strecke` BIGINT(20) NOT NULL DEFAULT 1,
  `zeitEintrag` DATETIME NULL COMMENT 'ON Update set time',
  `teilnehmer_id` INT(11) NOT NULL,
  `code` VARCHAR(45) NULL COMMENT 'zur identifizierung der Streckenkarte',
  PRIMARY KEY (`id`),
  INDEX `fk_tnStrecke_teilnehmer1_idx` (`teilnehmer_id` ASC),
  CONSTRAINT `fk_tnStrecke_teilnehmer1`
    FOREIGN KEY (`teilnehmer_id`)
    REFERENCES `dlrg_goettingen_24h`.`teilnehmer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`tnZeit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`tnZeit` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`tnZeit` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `strecke` INT(11) UNSIGNED NOT NULL,
  `istZeit` BIGINT(20) UNSIGNED NULL DEFAULT 1,
  `sollZeit` BIGINT(20) UNSIGNED NOT NULL,
  `teilnehmer_id` INT(11) NOT NULL,
  `code` VARCHAR(45) NULL COMMENT 'Zuordnung der Zeitkarten',
  PRIMARY KEY (`id`),
  INDEX `fk_tnZeit_teilnehmer1_idx` (`teilnehmer_id` ASC),
  CONSTRAINT `fk_tnZeit_teilnehmer1`
    FOREIGN KEY (`teilnehmer_id`)
    REFERENCES `dlrg_goettingen_24h`.`teilnehmer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`bu_kategorien`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`bu_kategorien` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`bu_kategorien` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `kategorie` VARCHAR(45) NOT NULL,
  `katKurz` VARCHAR(10) NULL COMMENT 'Abkürzung für die Kategorie',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`bu_rechnung_hinweis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`bu_rechnung_hinweis` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`bu_rechnung_hinweis` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hinweis` MEDIUMTEXT NULL COMMENT 'Beispiel:\nNach §19 USTG umsatzsteuerfrei.\n\nHinweise werden dann mit auf den Rechnungen / Quittungen aufgenommen',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`bu_artikel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`bu_artikel` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`bu_artikel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `preis` FLOAT UNSIGNED NULL,
  `bu_kategorien_id` INT NOT NULL,
  `bu_rechnung_hinweis_id` INT NULL,
  `steuer` INT NULL DEFAULT 0,
  `jahr` YEAR NULL COMMENT 'Jahr in dem die Presie gültig sind',
  PRIMARY KEY (`id`),
  INDEX `fk_bu_artikel_bu_kategorien1_idx` (`bu_kategorien_id` ASC),
  INDEX `fk_bu_artikel_bu_rechnung_hinweis1_idx` (`bu_rechnung_hinweis_id` ASC),
  CONSTRAINT `fk_bu_artikel_bu_kategorien1`
    FOREIGN KEY (`bu_kategorien_id`)
    REFERENCES `dlrg_goettingen_24h`.`bu_kategorien` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bu_artikel_bu_rechnung_hinweis1`
    FOREIGN KEY (`bu_rechnung_hinweis_id`)
    REFERENCES `dlrg_goettingen_24h`.`bu_rechnung_hinweis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`buchung_einnahme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`buchung_einnahme` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`buchung_einnahme` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bu_artikel_id` INT NOT NULL,
  `teilnehmer_id` INT(11) NULL,
  `buchung_id` INT(11) NOT NULL,
  `eingeloest` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_buba_einnahme_bu_artikel1_idx` (`bu_artikel_id` ASC),
  INDEX `fk_buba_einnahme_teilnehmer1_idx` (`teilnehmer_id` ASC),
  INDEX `fk_buba_einnahme_buchung1_idx` (`buchung_id` ASC),
  CONSTRAINT `fk_buba_einnahme_bu_artikel1`
    FOREIGN KEY (`bu_artikel_id`)
    REFERENCES `dlrg_goettingen_24h`.`bu_artikel` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_buba_einnahme_teilnehmer1`
    FOREIGN KEY (`teilnehmer_id`)
    REFERENCES `dlrg_goettingen_24h`.`teilnehmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_buba_einnahme_buchung1`
    FOREIGN KEY (`buchung_id`)
    REFERENCES `dlrg_goettingen_24h`.`buchung` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`gruppeZuTn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`gruppeZuTn` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`gruppeZuTn` (
  `teilnehmer_id` INT(11) NOT NULL,
  `gruppe_id` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  INDEX `fk_gruppeZuTn_teilnehmer1_idx` (`teilnehmer_id` ASC),
  INDEX `fk_gruppeZuTn_gruppe1_idx` (`gruppe_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gruppeZuTn_teilnehmer1`
    FOREIGN KEY (`teilnehmer_id`)
    REFERENCES `dlrg_goettingen_24h`.`teilnehmer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_gruppeZuTn_gruppe1`
    FOREIGN KEY (`gruppe_id`)
    REFERENCES `dlrg_goettingen_24h`.`gruppe` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`user_rechte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`user_rechte` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`user_rechte` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`userHatRechte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`userHatRechte` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`userHatRechte` (
  `user_id` INT NOT NULL,
  `user_rechte_id` INT NOT NULL,
  INDEX `fk_userHatRechte_user1_idx` (`user_id` ASC),
  INDEX `fk_userHatRechte_user_rechte1_idx` (`user_rechte_id` ASC),
  PRIMARY KEY (`user_id`, `user_rechte_id`),
  CONSTRAINT `fk_userHatRechte_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `dlrg_goettingen_24h`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_userHatRechte_user_rechte1`
    FOREIGN KEY (`user_rechte_id`)
    REFERENCES `dlrg_goettingen_24h`.`user_rechte` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`rechnung_adresse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`rechnung_adresse` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`rechnung_adresse` (
  `rechnung_id` INT(11) NOT NULL,
  `adresse_id` INT(11) NOT NULL,
  PRIMARY KEY (`rechnung_id`, `adresse_id`),
  INDEX `fk_rechnung_adresse_adresse1_idx` (`adresse_id` ASC),
  CONSTRAINT `fk_rechnung_adresse_rechnung1`
    FOREIGN KEY (`rechnung_id`)
    REFERENCES `dlrg_goettingen_24h`.`rechnung` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rechnung_adresse_adresse1`
    FOREIGN KEY (`adresse_id`)
    REFERENCES `dlrg_goettingen_24h`.`adresse` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dlrg_goettingen_24h`.`rechnung_mahnwesen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dlrg_goettingen_24h`.`rechnung_mahnwesen` ;

CREATE TABLE IF NOT EXISTS `dlrg_goettingen_24h`.`rechnung_mahnwesen` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `typ` VARCHAR(45) NULL,
  `datum` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `rechnung_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_rechnung_mahnwesen_rechnung1_idx` (`rechnung_id` ASC),
  CONSTRAINT `fk_rechnung_mahnwesen_rechnung1`
    FOREIGN KEY (`rechnung_id`)
    REFERENCES `dlrg_goettingen_24h`.`rechnung` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
