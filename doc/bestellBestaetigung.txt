Hallo Florian Unger
zum Abschluss Ihrer Bestellung müssen sie noch Ihre E-Mail Adresse bestätigen.

Rufen sie dazu Bitte den folgenden Link auf. Sollte sich kein Browserfenster öffnen kopieren Sie den Link bitte in ein Browserfenster und rufen ihn auf.
https://anmeldung.schwimmfestival.de/server/func/validateMail.php?mail=floh.unger@gmail.com&idBesteller=137&idBuchung=26

Nachdem Sie ihre E-Mail bestätigt haben bekommen sie eine weitere E-Mail mit Ihrer Rechnung sowie den Eintrittskarten.
Sofern ihre E-Mail Adresse nicht bestätigt wird, wird die Bestellung in 3 Tagen unsererseits storniert!

Ihre Bestellung ist wie folgt bei uns eingegangen.
ArtikelNr, Artikel, Anzahl, Einzelpreis, Summe
7, Abendessen, 1, 4,50 Euro, 4,50 Euro
3, Frühstück, 1, 2,50 Euro, 2,50 Euro
5, Mittagessen, 1, 3,00 Euro, 3,00 Euro
9, Startgebühr, 1, 6,00 Euro, 6,00 Euro

 Gesamtsumme der Bestellung: 16,00 Euro

 Mit freundlichen Grüßen
 vom Schwimmfestival Team

Deutsche Lebens-Rettungs-Gesellschaft
Landesverband Niedersachen – Bezirk Göttingen
Ortsgruppe Göttingen e.V.

Anschrift:
DLRG Göttingen e.V.
Haus des Sports
Sandweg 5
37083 Göttingen

Telefon: 0551 30 77 922
Telefax: 0551 48 95 596

Webseite: http://www.goettingen.dlrg.de
Webseite Jugend: http://www.goettingen.dlrg-jugend.de
