<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 13.04.2016
 * Time: 19:21
 */
class Buchung
{
  public $id = - 1;
  public $date = '';
  public $status = '';
  public $bestCode = '';
  public $versandart = '';
  public $userId = 1;

  /**
   * @var Rechnung
   */
  public $rechnung;

  public $einnahmen = array();

  public $teilnehmer = array();

  public function __construct ()
  {
    $this->rechnung = new Rechnung();
  }

  public function loadById ( mysqli $db, $idBuchung )
  {
    $sql = "SELECT " . "id, DATE_FORMAT(timestamp, '%Y-%m-%dT%H:%i:%s.000Z') as date, status, versandart, bestcode, besteller_id as besteller, user_id AS user "
           . "FROM buchung WHERE id =" . $idBuchung;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
        $this->date = $row[ 'date' ];
        $this->status = $row[ 'status' ];
        $this->bestCode = $row[ 'bestcode' ];
        $this->versandart = $row[ 'versandart' ];
        $this->userId = $row[ 'user' ];
      }
    }
    $this->loadEinnahmen( $db );
    $this->loadTeilnehmer( $db );
    $this->rechnung->loadRechnungByBuchungId( $db, $idBuchung );
  }

  public function loadByRechnungUndEmail ( mysqli $db, $rechnung, $email )
  {
    $this->getIdByRechnungUndMail( $db, $rechnung, $email );
    $this->loadById( $db, $this->id );
  }

  /**
   * Validiert ob Rechnungsnummer zum angegebenen Besteller (email) vorhanden ist.
   *
   * @param $rechnung
   * @param $db
   *
   * @return int
   *      Buchungsnummer wenn die Validierung positiv ausfällt und -1 wenn die Validierung negativ ausfällt.
   */
  private function getIdByRechnungUndMail ( mysqli $db, $rechnung, $mail )
  {
    $rechnungNummer = str_split( $rechnung, 4 );
    $sql = "SELECT buchung.id "
           . "FROM besteller, buchung, rechnung "
           . "WHERE besteller.id = buchung.besteller_id AND buchung.id = rechnung.buchung_id "
           . "AND besteller.email = '" . $mail . "' "
           . "AND rechnung.id = " . $rechnungNummer[ 1 ];

    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
      }
    }
  }

  public function setStatus ( mysqli $db, $idBuchung, $status )
  {
    $sql = "UPDATE `buchung` SET `status` = '" . $status . "' WHERE `buchung`.`id` =" . $idBuchung;
    $db->query( $sql );
    $this->loadById( $db, $idBuchung );
  }

  public function loadEinnahmen ( mysqli $db )
  {
    $sql = "SELECT id, bu_artikel_id, teilnehmer_id FROM `buchung_einnahme` WHERE buchung_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $einnahme = new BuchungEinnahme();
        $einnahme->id = $row[ 'id' ] * 1;
        $einnahme->artikel = $row[ 'bu_artikel_id' ] * 1;
        if ( $row[ 'teilnehmer_id' ] > 0 )
        {
          $einnahme->teilnehmer = $row[ 'teilnehmer_id' ] * 1;
        }
        array_push( $this->einnahmen, $einnahme );
      }
    }
  }

  public function loadTeilnehmer ( mysqli $db )
  {
    $sql = "SELECT id FROM teilnehmer WHERE buchung_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $teilnehmer = new Teilnehmer();
        $teilnehmer->loadById( $db, $row[ 'id' ] );
        array_push( $this->teilnehmer, $teilnehmer );
      }
    }
  }

  public function neu ( mysqli $db, $userId, $rechnungsadresse = 'NULL', $idBesteller = 'NULL' )
  {
    $sql = "INSERT INTO `buchung`(`status`, `versandart`, `bestcode`, `besteller_id`, `user_id`, `rechnungsadresse`) VALUES "
           . "('" . $this->status . "', '" . $this->versandart . "', '" . $this->bestCode . "', "
           . $idBesteller . ", " . $userId . ", " . $rechnungsadresse . ")";
    $db->query( $sql );
    $this->id = $db->insert_id;
  }

  public function einnahmeTageskasse ( mysqli $db, $json, $userId )
  {
    $this->deserializeJson( $json );

    //Rechnungsbetrag ermitteln
    $rechnungsbetrag = 0;
    $adresse = new Adresse();
    foreach ( $json->{'einnahmen'} as $einnahme )
    {
      $artikel = new Artikel();
      $artikel->loadById( $db, $einnahme->{'artikel'} );
      $rechnungsbetrag = $rechnungsbetrag + $artikel->preis;
    }
    if ( $rechnungsbetrag >= 150 ) //Rechnungsadresse anlegen
    {
      $rechnung = $json->{'rechnung'};
      $adresse->deserializeJson( $rechnung->{'adresse'} );
      $adresse->addToDatenbank( $db );
      $this->neu( $db, $userId, $adresse->id );
    }
    else
    {
      $this->neu( $db, $userId );
    }

    //Teilnehmer und Einnahmen einfügen
    foreach ( $json->{'einnahmen'} as $einnahme )
    {
      $tn = new Teilnehmer();
      $tn->addToDatenbank( $db, $this->id );
      $ein = new BuchungEinnahme();
      $ein->teilnehmer = $tn->id;
      $ein->artikel = $einnahme->{'artikel'};
      $ein->addToDatenbank( $db, $this->id );
      $tn->setIstDa( $db, $tn->id );
    }
    generateRechnung( $this->id, $db, false, true );
    $this->setStatus( $db, $this->id, 'Barkasse' );
    $this->loadById( $db, $this->id );
  }

  public function deserializeJson ( $json )
  {
    if ( $json->{'id'} != - 1 )
    {
      $this->id = $json->{'id'};
      $this->date = prooveDate( $json->{'date'} );
      $this->status = $json->{'status'};
      $this->bestCode = $json->{'bestCode'};
      $this->versandart = $json->{'versandart'};
    }
    // {"id":-1,"date":"","status":"","bestCode":"","versandart":"","rechnung":{"id":0,"datum":"","geldeingang":"","status":1,"idBuchung":0,"versand":"",
    // "adresse":{"id":0,"vorname":"","nachname":"","gender":"","firma":"","strasse":"","plz":"","ort":"","typ":"Rechnungsanschrift"},"mahnungen":[]},
    // "einnahmen":[],"teilnehmer":[]}

  }

  public function setStorniert ( mysqli $db, $id )
  {
    //Prüfe ob es eine Rechnung gibt
    $idRechnung = - 1;
    $sql = "SELECT id FROM `rechnung` WHERE `buchung_id` =" . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $idRechnung = $row[ 'id' ];
      }
    }
    if ( $idRechnung > 0 )
    {
      $rechnung = new Rechnung();
      $rechnung->setStorniert( $db, $idRechnung, false );
    }
    else
    {
      $sql = "UPDATE `buchung` SET `status` = 'storniert' WHERE `buchung`.`id` = " . $id;
      $db->query( $sql );
    }
    $this->loadById( $db, $id );
  }

  public function sendValidationMail ( mysqli $db )
  {
    $adresse = new Adresse();
    $idBesteller = - 1;
    $mail = '';
    $sql = "SELECT buchung.besteller_id AS besteller, besteller.email AS mail, buchung.rechnungsadresse AS adresse "
           . "FROM `buchung`, besteller WHERE buchung.id = " . $this->id . " AND buchung.besteller_id = besteller.id";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $adresse->id = $row[ 'adresse' ];
        $idBesteller = $row[ 'besteller' ];
        $mail = $row[ 'mail' ];
      }
    }
    if ( $adresse->id != - 1 && $idBesteller != - 1 )
    {
      $adresse->loadAdressById( $db, $adresse->id );

      sendMailBestellerValidation( $adresse, $mail, $idBesteller, $this->id, $db );
      $this->loadById( $db, $this->id );
    }
    else
    {
      $this->loadById( $db, $this->id );
      $this->id = - 5;
    }

  }

  public function updateRechnungsadresse (mysqli $db, $json)
  {
    if ( $json->{'id'} != - 1 )
    {
      $this->id = $json->{'id'};
    }
    $sql = "SELECT rechnungsadresse FROM `buchung` WHERE id = " . $this->id;
    $idAdresse = -1;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        if($row['rechnungsadresse'] != null)
        {
          $idAdresse = $row['rechnungsadresse'];
        }
      }
    }
    $adresse = new Adresse();
    $rechnung = $json->{'rechnung'};
    $adresse->deserializeJson( $rechnung->{'adresse'} );
    //Adresse neu anlegen
    if($idAdresse == -1)
    {
      $adresse->addToDatenbank( $db );
      $sql = "UPDATE `buchung` SET `rechnungsadresse` = ".$adresse->id." WHERE `buchung`.`id` = " . $this->id;
      $db->query( $sql );
    }
    else
    {
      $adresse->updateAdresseInDatenbank( $db );
    }
    $rechnung = new Rechnung();
    $rechnung->idBuchung = $this->id;
    $rechnung->updateRechnung( $db );
    $this->loadById( $db, $this->id );
  }

}

class Buchungen
{
  public $buchungen = array();

  public function __construct ()
  {

  }

  public function loadBuchungenByBestellerId ( mysqli $db, $idBesteller )
  {
    $sql = "SELECT " . "id, DATE_FORMAT(timestamp, '%Y-%m-%dT%H:%i:%s.000Z') as date, status, versandart, bestcode, besteller_id as besteller "
           . "FROM buchung WHERE besteller_id =" . $idBesteller;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $bu = new Buchung();
        $bu->id = $row[ 'id' ];
        $bu->date = $row[ 'date' ];
        $bu->status = $row[ 'status' ];
        $bu->bestCode = $row[ 'bestcode' ];
        $bu->versandart = $row[ 'versandart' ];
        $bu->rechnung->loadRechnungByBuchungId( $db, $bu->id );
        $bu->loadEinnahmen( $db );
        array_push( $this->buchungen, $bu );
      }
    }
  }

  public function loadLastN ( mysqli $db, $userId, $n = 5 )
  {
    $buchungsIds = array();
    $sql = "SELECT id FROM `buchung` WHERE `user_id` = " . $userId . " ORDER BY id DESC LIMIT " . $n;

    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        array_push( $buchungsIds, $row[ 'id' ] );
      }
    }

    $this->loadByIds( $db, $buchungsIds );
  }

  public function loadByStatus ( mysqli $db, $status )
  {
    $buchungsIds = array();
    $sql = "SELECT id FROM `buchung` WHERE `status` = '" . $status . "' ORDER BY id DESC";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        array_push( $buchungsIds, $row[ 'id' ] );
      }
    }
    $this->loadByIds( $db, $buchungsIds );
  }

  private function loadByIds ( mysqli $db, $buchungsIds )
  {
    foreach ( $buchungsIds as $id )
    {
      $buchung = new Buchung();
      $buchung->loadById( $db, $id );
      array_push( $this->buchungen, $buchung );
    }
  }
}
