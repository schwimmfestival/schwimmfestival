<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 01.04.2016
 * Time: 16:53
 */
class Teilnehmer
{
  public $id = - 1;
  public $vorname = '';
  public $nachname = '';
  public $gebDate = '';
  public $gender = '';
  public $einnahmen = array();
  public $challenges = array();
  public $gruppen = array();
  public $strecken = array();
  public $isDa = '';

  public function __construct ()
  {
  }

  public function loadById ( mysqli $db, $id )
  {
    $sql = "SELECT vorname, nachname, gebdat, gender, istDa FROM teilnehmer WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      $this->id = $id;
      while ( $row = $result->fetch_assoc() )
      {
        $this->vorname = $row[ 'vorname' ];
        $this->nachname = $row[ 'nachname' ];
        $this->gebDate = $row[ 'gebdat' ];
        $this->gender = $row[ 'gender' ];
        if ( $row[ 'istDa' ] != null )
        {
          $this->isDa = $row[ 'istDa' ];
        }
      }
      $this->loadGruppen( $db );
      $this->loadChallenges( $db );
      $this->loadStrecken( $db );
      $this->loadEinnahmen( $db );
    }
  }

  public function loadSelfService ( mysqli $db, $json )
  {
    $this->decodeJson( $json );
    $sql = "SELECT vorname, nachname, gebdat, gender, istDa FROM teilnehmer WHERE id = " . $this->id . " AND gebdat ='" . $this->gebDate . "'";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->vorname = $row[ 'vorname' ];
        $this->nachname = $row[ 'nachname' ];
        $this->gebDate = $row[ 'gebdat' ];
        $this->gender = $row[ 'gender' ];
        if ( $row[ 'istDa' ] != null )
        {
          $this->isDa = $row[ 'istDa' ];
        }
      }
      $this->loadGruppen( $db );
      $this->loadChallenges( $db );
      $this->loadStrecken( $db );
      $this->loadEinnahmen( $db );
    }
  }

  public function loadEinnahmen ( mysqli $db )
  {
    $sql = "SELECT bu_artikel_id FROM buchung_einnahme WHERE teilnehmer_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $artikel = new Artikel();
        $artikel->loadById( $db, $row[ 'bu_artikel_id' ] );
        array_push( $this->einnahmen, $artikel );
      }
    }
  }

  public function loadStrecken ( mysqli $db )
  {
    $sql = "SELECT id, strecke, zeitEintrag, code FROM tnStrecke WHERE teilnehmer_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $strecke = new Strecke();
        $strecke->id = $row[ 'id' ];
        if ( $row[ 'strecke' ] != null )
        {
          $strecke->strecke = $row[ 'strecke' ];
        }
        if ( $row[ 'zeitEintrag' ] != null )
        {
          $strecke->zeitEintrag = $row[ 'zeitEintrag' ];
        }
        if ( $row[ 'code' ] != null )
        {
          $strecke->code = $row[ 'code' ];
        }
        array_push( $this->strecken, $strecke );
      }
    }
  }

  public function loadChallenges ( mysqli $db )
  {
    $sql = "SELECT id, strecke, istZeit, sollZeit, code FROM `tnZeit` WHERE teilnehmer_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $challenge = new Challenge();
        $challenge->id = $row[ 'id' ];
        $challenge->strecke = $row[ 'strecke' ];
        if ( $row[ 'istZeit' ] != null )
        {
          $challenge->istZeit = $row[ 'istZeit' ];
        }
        if ( $row[ 'sollZeit' ] != null )
        {
          $challenge->zeitErwartet = $row[ 'sollZeit' ];
        }
        if ( $row[ 'code' ] != null )
        {
          $challenge->code = $row[ 'code' ];
        }
        array_push( $this->challenges, $challenge );
      }
    }
  }

  public function loadGruppen ( mysqli $db )
  {

    $sql = "SELECT gruppe.id, gruppe.name, gruppe_typ.name as kategorie, gruppe_typ.id as kategorieId "
           . " FROM gruppe, gruppeZuTn, gruppe_typ "
           . " WHERE  gruppeZuTn.gruppe_id = gruppe.id AND gruppe.gruppe_typ_id = gruppe_typ.id  "
           . " AND gruppeZuTn.teilnehmer_id = " . $this->id;

    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $gruppe = new Gruppe();
        $gruppe->id = $row[ 'id' ];
        $gruppe->name = $row[ 'name' ];
        $gruppe->idKategorie = $row[ 'kategorieId' ];
        $gruppe->kategorieName = $row[ 'kategorie' ];
        array_push( $this->gruppen, $gruppe );
      }
    }
  }

  public function update ( mysqli $db, $json )
  {
    $this->decodeJson( $json );
    $sql = "UPDATE `teilnehmer` SET `vorname` = '" . $this->vorname . "', `nachname` = '" . $this->nachname . "', `gebdat` = '" . $this->gebDate .
           "', `gender` = '"
           . $this->gender . "' WHERE `teilnehmer`.`id` =" . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  private function decodeJson ( $json )
  {
    $this->id = $json->{'id'};
    $this->vorname = $json->{'vorname'};
    $this->nachname = $json->{'nachname'};
    $this->gender = $json->{'gender'};
    $gebDatAr = str_split( $json->{'gebDate'}, 10 );
    $this->gebDate = $gebDatAr[ 0 ];
  }

  public function checkIn ( mysqli $db, $barcode )
  {
    //barcode 1002510037 buchung +10000 . tn +10000
    $split = str_split( $barcode, 5 );
    $idBuchung = $split[ 0 ] - 10000;
    $idTn = $split[ 1 ] - 10000;

    //check ob Buchung bezahlt
    $sql = "SELECT status FROM `buchung` WHERE `id` = " . $idBuchung;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        if($row['status'] != 'bezahlt')
        {
          $this->id = -7;
          return;
        }
      }
    }
    $id = - 1;
    $istDa = null;
    $sql = "SELECT id, istDa FROM `teilnehmer` WHERE `id`= " . $idTn . " AND `buchung_id` = " . $idBuchung;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $id = $row[ 'id' ];
        $istDa = $row[ 'istDa' ];
      }
    }
    if ( $id != - 1 )
    {
      if ( $istDa == null )
      {
        $this->setIstDa( $db, $id );
      }
      else
      {
        $this->loadById( $db, $id );
        $this->id = - 6;
      }

    }
    else
    {
      $this->id = - 5;
    }

  }

  public function setIstDa ( mysqli $db, $id )
  {
    $sql = "UPDATE `teilnehmer` SET `istDa` = CURRENT_TIMESTAMP WHERE id = " . $id;
    $db->query( $sql );
    $this->loadById( $db, $id );
  }

  public function addToDatenbank ( mysqli $db, $buchungId )
  {
    $gebDat = $this->gebDate;
    if ( $this->gebDate == '' )
    {
      $gebDat = '1900-01-01';
    }
    $sql = "INSERT INTO `teilnehmer`(`vorname`, `nachname`, `gebdat`, `gender`, `buchung_id`) VALUES "
           . "('" . $this->vorname . "', '" . $this->nachname . "', '" . $gebDat . "', '" . $this->gender . "', " . $buchungId . " )";
    $db->query( $sql );
    $this->id = $db->insert_id;
  }

}
