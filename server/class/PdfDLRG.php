<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 30.03.2016
 * Time: 23:59
 */

require '../lib/code39.php';

class PdfDLRG extends PDF_Code39
{
  public $rechnungNummer = '';
  public $rechnungDatum = '';
  public $iban = '';
  public $versandart = '';
  private $steuernummer = 'Steuernr.: 20/206/10331';
  private $isSteuerNummerZuZeigen = false;

  // Page header
  function Header()
  {
    // Logo
    $this->Image('../pic/logo_dlrg.jpg', 155, 10, 40);
    // correct Position
    $this->SetFont('Arial', '', 10);
    $this->SetXY(155, 45);
    $this->MultiCell(45, 4, utf8_decode("Ortsgruppe Göttingen \nWilli-Eichler-Straße 26\n37079 Göttingen\ninfo@schwimmfestival.de\n\nRechnung: " 
                                        . $this->rechnungNummer . "\nDatum: " . $this->rechnungDatum), 0);

  }

  // Page footer
  function Footer()
  {
    // Position at 3 cm from bottom
    $this->SetY(-25);
    // Arial italic 8
    $this->SetFont('Arial', 'I', 8);
    // Page number
    $this->MultiCell(45, 4, utf8_decode("DLRG Göttingen \nTel. +49 (0) 551 3077922 \nFAX +49 (0) 551 4895596\ninfo@schwimmfestival.de"), 0);
    $this->SetXY(85, -25);
    $this->MultiCell(45, 4, utf8_decode("Amtsgericht Göttingen \nVR:1889  \nFinanzamt Göttingen \n " . $this->getSteuernummer() ), 0);
    $this->SetXY(150, -25);
    $this->MultiCell(55, 4, utf8_decode("DLRG Göttingen \nSparkasse Göttingen  \nBIC: NOLADE21GOE \nIBAN: " . $this->iban), 0);
    $this->SetY(-10);
    $this->Cell(0, 10, utf8_decode("www.schwimmfestival.de www.goettingen.dlrg.de www.goettingen.dlrg-jugend.de"), 0, 0, 'C');
    $this->SetY(-10);
    $this->Cell(0, 10, utf8_decode("Seite " . $this->PageNo() . '/{nb}'), 0, 0, 'R');
  }

  function zeigeSteuernummer()
  {
    $this->isSteuerNummerZuZeigen = true;
  }

  function getSteuernummer()
  {
    if($this->isSteuerNummerZuZeigen)
    {
      return $this->steuernummer;
    }
    return '';
  }

}
