<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 15:23
 */
class Mahnung
{
  public $id = 0;
  /**
   * Zahlunserinnerung, Mahnung ...
   * @var string
   */
  public $typ = '';
  public $idRechnung = 0;
  public $datum = '';

  public function addMahnung(mysqli $db, $idRechnung, $typ)
  {
    $sql = "INSERT INTO `rechnung_mahnwesen` ( `typ`, `rechnung_id`) VALUES ( '".$typ."',  ".$idRechnung.")";
    if ( $db->query( $sql ) )
    {
      $this->loadById( $db, $db->insert_id );
      return true;
    }
    return false;
  }

  public function loadById(mysqli $db, $id)
  {
    $sql = "SELECT id, typ, datum, rechnung_id FROM `rechnung_mahnwesen` WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row['id'];
        $this->typ = $row['typ'];
        $this->datum = $row['datum'];
        $this->idRechnung = $row['rechnung_id'];
      }
    }
  }

  public function deserializeQueryRow($row)
  {
    $this->id = $row['id'];
    $this->typ = $row['typ'];
    $this->idRechnung = $row['rechnung_id'];
    $this->datum = $row['datum'];
  }

}
