<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 07.04.2016
 * Time: 18:16
 */
class Besteller
{
  public $id = 0;
  public $pw = '';
  public $isActive = 1;
  public $mail = '';
  /**
   * @var Adressen
   */
  public $Adressen;
  /**
   * @var Buchung
   */
  public $Buchungen;

  public function __construct ()
  {
    $this->Adressen = new Adressen();
    $this->Buchungen = new Buchungen();
  }

  public function loadBestellerById ( mysqli $db, $idBesteller = 0 )
  {
    $sql = "SELECT " . "pw, isActive, email FROM besteller "
           . "WHERE id = " . $idBesteller;

    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $idBesteller;
        $this->pw = $row[ 'pw' ];
        $this->isActive = $row[ 'isActive' ];
        $this->mail = $row[ 'email' ];

      }
    }
    $adr = new Adressen();
    $adr->loadAdressen( $db, $idBesteller );
    $this->Adressen = $adr;
    $bu = new Buchungen();
    $bu->loadBuchungenByBestellerId( $db, $idBesteller );
    $this->Buchungen = $bu;
  }

  public function sendeValidierungsmail(mysqli $db)
  {
    if($this->id != 0)
    {
     
    }
  }
}
