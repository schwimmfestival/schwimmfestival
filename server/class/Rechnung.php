<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 07.04.2016
 * Time: 18:05
 */
class Rechnung
{
  public $id = -1;
  public $datum = '';
  public $geldeingang = '';
  public $status = 1;
  public $idBuchung = 0;
  public $versand = '';
  public $rechnungsbetrag = 0;

  /**
   * @var Rechnungsadresse
   */
  public $adresse;

  public $mahnungen;

  public function __construct ()
  {
    $this->adresse = new Rechnungsadresse();
    $this->mahnungen = array();
  }

  /**
   * @param mysqli $db
   * @param number $idBuchung Buchungsnummer
   */
  public function loadRechnungByBuchungId ( mysqli $db, $idBuchung )
  {
    $this->idBuchung = $idBuchung;
    $sql = "SELECT rechnung.id, rechnung.datum, geldeingang, kartenstatus, buchung.versandart AS versand "
           ."FROM rechnung, buchung "
           ."WHERE rechnung.buchung_id = ".$idBuchung." AND rechnung.buchung_id = buchung.id ";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
        $this->datum = $row[ 'datum' ];
        $this->geldeingang = $row[ 'geldeingang' ];
        $this->status = $row[ 'kartenstatus' ];
        $this->versand = $row[ 'versand' ];
      }
    }
    $this->loadRechnungsbetrag( $db );
    $this->adresse->loadAdresseByBuchungId( $db, $this->idBuchung );
    $this->getMahnungen( $db );
  }

  /**
   * @param mysqli $db
   * @param number $id Rechnungsnummer
   */
  public function loadRechnungById ( mysqli $db, $id )
  {
    $sql = "SELECT rechnung.id, rechnung.datum, geldeingang, kartenstatus, buchung.versandart AS versand, buchung_id "
           ."FROM rechnung, buchung "
           ."WHERE rechnung.id = ".$id." AND rechnung.buchung_id = buchung.id ";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->idBuchung = $row[ 'buchung_id' ];
        $this->id = $row[ 'id' ];
        $this->datum = $row[ 'datum' ];
        $this->geldeingang = $row[ 'geldeingang' ];
        $this->status = $row[ 'kartenstatus' ];
        $this->versand = $row[ 'versand' ];
      }
    }
    $this->loadRechnungsbetrag( $db );
    $this->adresse->loadAdresseByBuchungId( $db, $this->idBuchung );
    $this->getMahnungen( $db );
  }

  private function loadRechnungsbetrag(mysqli $db)
  {
    $sql = "SELECT SUM(bu_artikel.preis) AS summe FROM bu_artikel, buchung_einnahme WHERE buchung_einnahme.buchung_id = "
           .$this->idBuchung." AND buchung_einnahme.bu_artikel_id = bu_artikel.id";
    $gesamtSummeBestellung = 0;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $gesamtSummeBestellung = $gesamtSummeBestellung + $row[ 'summe' ];
      }
    }
    $this->rechnungsbetrag = $gesamtSummeBestellung;
  }

  /**
   * @param mysqli $db
   * @param        $id Rechnungsnummer
   * @param        $geldeingang yyy-mm-dd
   * @param        $isMail Mail senden?
   *
   * @return bool
   */
  public function setBezahlt ( mysqli $db, $id, $geldeingang, $isMail = true)
  {
    $boolRet = false;
    $sql = "UPDATE" . " rechnung SET geldeingang = '" . $geldeingang
           . "', kartenstatus = 2 "
           . "WHERE id = " . $id;
    if ( $db->query( $sql ) )
    {
      $boolRet = true;
    }
    if ( $boolRet )
    {
      $this->loadRechnungById( $db, $id );
      $sql = "UPDATE `buchung` SET `status` = 'bezahlt' WHERE `buchung`.`id` = " . $this->idBuchung;
      $db->query( $sql );
      if($isMail)
      {
        $this->sendBezahltMail( $db );
      }
      return true;
    }
    return false;
  }

  /**
   * @param mysqli $db
   *
   * @return bool
   */
  private function sendBezahltMail ( mysqli $db )
  {
    $mail = getMail();
    $mail->AddAddress( $this->getMailAdresse( $db ) );
    $mail->Subject = 'Rechnung 24h-' . $this->id . ' bezahlt ' . VERANSTALTUNG . ' vom ' . VERANSTALTUNG_ZEITRAUM;

    $nachricht = $this->getAnrede( $this->adresse->gender ) . html_entity_decode( $this->adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
                 . 'vielen Dank für Ihre Bestellung. Wir haben den Rechnungsbetrag erhalten.' . "\n"
                 . '  Die Eintrittskarten haben Sie bereits mit der Rechnung erhalten. '
                 . "\n \n";
    $nachricht .= MAIL_FOOT;
    $mail->Body = $nachricht;

    if ( !$mail->send() )
    {
      errorHandling( 'senden Fehlgeschlagen' . __FILE__ . ' Line' . __LINE__);
      return false;
    }
    return true;
  }

  /**
   * @param mysqli $db
   * @param        $id Rechnungsnummer
   * @param        $isMail Mail senden?
   * @return bool
   */
  public function setStorniert ( mysqli $db, $id, $isMail = true )
  {
    $boolRet = false;
    $sql = "UPDATE rechnung SET kartenstatus = 3, geldeingang = NULL "
           . "WHERE id = " . $id;
    if ( $db->query( $sql ) )
    {
      $boolRet = true;
    }
    if ( $boolRet )
    {
      $this->loadRechnungById( $db, $id );
      $sql = "UPDATE `buchung` SET `status` = 'storniert' WHERE `buchung`.`id` = " . $this->idBuchung;
      $db->query( $sql );
      if($isMail)
      {
        $this->sendStorniertMail( $db );
      }
      return true;
    }
    return false;

  }

  /**
   * @param mysqli $db
   *
   * @return bool
   */
  private function sendStorniertMail ( mysqli $db )
  {
    $mail = getMail();
    $mail->AddAddress( $this->getMailAdresse( $db ) );
    $mail->Subject = 'Rechnung 24h-' . $this->id . ' storniert ' . VERANSTALTUNG . ' vom ' . VERANSTALTUNG_ZEITRAUM;

    $nachricht = $this->getAnrede( $this->adresse->gender ) . html_entity_decode( $this->adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
                 . 'Ihre Buchung und unsere Rechnung wurden storniert.'
                 . ' Ihnen zugesandte Eintrittskarten werden damit ungültig. '
                 . "\n \n";
    $nachricht .= MAIL_FOOT;
    $mail->Body = $nachricht;

    if ( !$mail->send() )
    {
      errorHandling( 'senden Fehlgeschlagen'. __FILE__ . ' Line' . __LINE__ );
      return false;
    }
    return true;
  }

  /**
   * @param mysqli $db
   * @param        $id Rechnungsnummer
   * @param        $typ 'Zahlungserinnerung'|'Mahnung ...
   */
  public function addMahnung ( mysqli $db, $id, $typ )
  {
    $sql = "UPDATE rechnung SET kartenstatus = 1, geldeingang = NULL "
           . "WHERE id = " . $id;
    $db->query( $sql );
    $mahnung = new Mahnung();
    $mahnung->addMahnung( $db, $id, $typ );
    $this->loadRechnungById( $db, $id );
    $this->sendMahnungMail( $db, $mahnung );
  }

  /**
   * @param mysqli $db
   */
  private function getMahnungen ( mysqli $db )
  {
    $sql = "SELECT id, typ, DATE_FORMAT( datum, '%Y-%m-%d') AS datum, rechnung_id FROM `rechnung_mahnwesen` WHERE rechnung_id = " . $this->id;
    if ( $result = $db->query( $sql ) )
      {
        while ( $row = $result->fetch_assoc() )
        {
        $mahnung = new Mahnung();
        $mahnung->deserializeQueryRow( $row );
        array_push( $this->mahnungen, $mahnung );
      }
    }

  }

  private function sendMahnungMail ( mysqli $db, Mahnung $mahnung )
  {
    $mail = getMail();
    $mail->AddAddress( $this->getMailAdresse( $db ) );
    $mail->Subject = $mahnung->typ . ' ' . 'Rechnung 24h-' . $this->id . ' ' . VERANSTALTUNG . ' vom ' . VERANSTALTUNG_ZEITRAUM;

    $nachricht = $this->getAnrede( $this->adresse->gender ) . html_entity_decode( $this->adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
                 . 'bislang konnten wir keinen Zahlungseingang feststellen und bitten Sie den Rechnungsbetrag umgehend zu überweisen.'
                 . ' Die Rechnung vom (' . mysqlDatetimeConverter( $this->datum, 'datum' ) . ') haben wir Ihnen noch einmal angehänt. '
                 . "\n \n";
    $nachricht .= MAIL_FOOT;
    $mail->Body = $nachricht;
    $mail->addAttachment( '../Rechnungen/24h-' . $this->id . 'Rechnung.pdf' );

    if ( !$mail->send() )
    {
      errorHandling( 'senden Fehlgeschlagen' . __FILE__ . ' Line' . __LINE__ );
      return false;
    }
    return true;

  }

  private function getAnrede ( $gender )
  {
    $anrede = 'Sehr geehrte Frau ';
    if ( $gender == 'm' )
    {
      $anrede = 'Sehr geehrter Herr ';
    }
    return $anrede;
  }

  public function getMailAdresse(mysqli $db)
  {
    $sql = "SELECT besteller.email AS email FROM buchung, besteller WHERE buchung.id = ".$this->idBuchung." AND buchung.besteller_id = besteller.id";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        return $row[ 'email' ];
      }
    }
  }

  public function updateRechnung(mysqli $db)
  {
    generateRechnung ( $this->idBuchung, $db, true );
    $this->loadRechnungByBuchungId( $db, $this->idBuchung );
    $email = $this->getMailAdresse( $db );
    if($email != null)
    {
      $this->sendAktualisierungsMail( $email );
    }
  }

  public function sendAktualisierungsMail($email)
  {
    $mail = getMail();
    $mail->AddAddress( $email );
    $mail->Subject = 'Aktualisiert ' . 'Rechnung 24h-' . $this->id . ' ' . VERANSTALTUNG . ' vom ' . VERANSTALTUNG_ZEITRAUM;

    $nachricht = $this->getAnrede( $this->adresse->gender ) . html_entity_decode( $this->adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
                 . 'Ihre Rechnung wurde aktualisiert. Sofgern noch nicht bezahlt, bitte wir Sie den Rechnungsbetrag umgehend zu überweisen.'
                 . ' Die Rechnung vom (' . mysqlDatetimeConverter( $this->datum, 'datum' ) . ') haben wir Ihnen angehänt. '
                 . "\n \n";
    $nachricht .= MAIL_FOOT;
    $mail->Body = $nachricht;
    $mail->addAttachment( '../Rechnungen/24h-' . $this->id . 'Rechnung.pdf' );

    if ( !$mail->send() )
    {
      errorHandling( 'senden Fehlgeschlagen' . __FILE__ . ' Line' . __LINE__ );
      return false;
    }
    return true;
  }

  public function deserialize($json)
  {

  }
}

class RechnungsListe
{
  public $rechnungen = array();

  public function __construct ()
  {

  }

  public function loadList ( mysqli $db, $status='' )
  {
    $rechnungNummern = array();
    $sql = "SELECT id FROM rechnung ";
    if($status == 'offen')
    {
      $sql .= "WHERE kartenstatus = 1";
    }
    if($status == 'bezahlt')
    {
      $sql .= "WHERE kartenstatus = 2";
    }
    if($status == 'storniert')
    {
      $sql .= "WHERE kartenstatus = 3";
    }
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        array_push( $rechnungNummern, $row[ 'id' ] );
      }
    }
    foreach ( $rechnungNummern as $value )
    {
      $rechnung = new Rechnung();
      $rechnung->loadRechnungById( $db, $value );
      array_push( $this->rechnungen, $rechnung );
    }
  }
}
