<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 05.04.2016
 * Time: 00:23
 */
class Challenge
{
  public $id = - 1;
  public $strecke = - 1;
  public $zeitErwartet = - 1;
  public $istZeit = - 1;
  public $code = '';

  public function __construct ()
  {
  }

  public function loadById ( mysqli $db, $id )
  {
    $sql = "SELECT id, strecke, istZeit, sollZeit, code FROM `tnZeit` WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
        $this->strecke = $row[ 'strecke' ];
        if ( $row[ 'istZeit' ] != null )
        {
          $this->istZeit = $row[ 'istZeit' ];
        }
        $this->zeitErwartet = $row[ 'sollZeit' ];
        if ( $row[ 'code' ] != null )
        {
          $this->code = $row[ 'code' ];
        }
      }
    }
  }

  public function decodeJson ( $json )
  {
    $this->id = $json->{'id'};
    $this->strecke = $json->{'strecke'};
    $this->zeitErwartet = $json->{'zeitErwartet'};
    $this->istZeit = $json->{'istZeit'};
    $this->code = $json->{'code'};
  }

  public function addChallenge ( mysqli $db, $teilnehmerId )
  {
    $sql = "INSERT INTO tnZeit(strecke, sollZeit, teilnehmer_id) VALUES ("
           . $this->strecke . ", " . $this->zeitErwartet . ", " . $teilnehmerId . ")";
    if ( $db->query( $sql ) )
    {
      $this->loadById( $db, $db->insert_id );
    }
  }

  public function updateChallenge ( mysqli $db )
  {
    $sql = "UPDATE tnZeit SET "
           . " tnZeit.strecke = " . $this->strecke
           . " , tnZeit.sollZeit = " . $this->zeitErwartet
           . " WHERE tnZeit.id =" . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function setCode ( mysqli $db, $id, $code )
  {
    $sql = "UPDATE tnZeit SET "
           . " tnZeit.code = '" . $code
           . "' WHERE tnZeit.id =" . $id;
    $db->query( $sql );
    $this->loadById( $db, $id );
  }

  public function setIstZeit ( mysqli $db )
  {
    //Prüfen ob zuvor bereits eine Zeit gesetzt war und wenn nicht automatisch die Strecke den Strecken hinzufügen
    $sql = "SELECT istZeit, teilnehmer_id as tn FROM `tnZeit` WHERE id = " . $this->id;
    $istZeit = -1;
    $tn = -1;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $istZeit = $row['istZeit'];
        $tn = $row['tn'];
      }
    }
    if ($istZeit < 2)
    {
      $sql = "INSERT INTO `tnStrecke` ( `strecke`, `zeitEintrag`, `teilnehmer_id`) VALUES ( ".$this->strecke.", CURRENT_TIMESTAMP, ".$tn.")";
      $db->query( $sql );
    }

    $sql = "UPDATE tnZeit SET "
           . " tnZeit.istZeit = '" . $this->istZeit
           . "', tnZeit.strecke = " . $this->strecke
           . " WHERE tnZeit.id =" . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function deleteById ( mysqli $db )
  {
    $sql = "DELETE FROM `tnZeit` WHERE `tnZeit`.`id` = " . $this->id;
    $db->query( $sql );
  }

  public function deleteByTeilnehmer ( mysqli $db, $teilnehmer )
  {
    $sql = "DELETE FROM tnZeit WHERE tnZeit.teilnehmer_id = " . $teilnehmer;
    $db->query( $sql );
  }

  public function getIdByCode ( $code )
  {
    $split = explode( 'Y', $code );
    $id = $split[ 0 ] - 1000;
    return $id;
  }
}
