<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 25.03.2016
 * Time: 10:01
 */
class Artikel
{
  public $id = - 1;
  public $name = '';
  public $preis = 0;
  /**
   * @var ArtikelKategorie
   */
  public $kategorie;
  /**
   * @var ArtikelRechnungshinweis
   */
  public $rechnungshinweis;
  public $steuer = 0;

  public static $artikel = array();


  public function __construct ()
  {
    $this->kategorie = new ArtikelKategorie();
    $this->rechnungshinweis = new ArtikelRechnungshinweis();
  }

  public function loadById ( mysqli $db, $id )
  {
    if ( count( Artikel::$artikel ) < 1 )
    {
      Artikel::load( $db );
    }
    $this->id = $id;
    $artikel = Artikel::$artikel[$id];
    $this->name = $artikel->name;
    $this->preis = $artikel->preis;
    $this->kategorie = $artikel->kategorie;
    $this->rechnungshinweis = $artikel->rechnungshinweis;
    $this->steuer = $artikel->steuer;

  }

  private static function load ( mysqli $db )
  {
    $sql = "SELECT id, name, preis, bu_kategorien_id, bu_rechnung_hinweis_id, steuer FROM bu_artikel ";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $artikel = new Artikel();
        $artikel->id = $row[ 'id' ] * 1;
        $artikel->name = $row[ 'name' ];
        $artikel->preis = $row[ 'preis' ] * 1;
        $artikel->kategorie->loadById( $db, $row[ 'bu_kategorien_id' ] );
        $artikel->rechnungshinweis->loadById( $db, $row[ 'bu_rechnung_hinweis_id' ] );
        $artikel->steuer = $row[ 'steuer' ] * 1;
        Artikel::$artikel[ $row[ 'id' ] ] = $artikel;
      }
    }
  }
}
