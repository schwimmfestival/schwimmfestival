<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 00:13
 */
class Rechnungsadresse extends Adresse
{
  public function loadAdresseByBuchungId ( mysqli $db, $idBuchung )
  {
    $idAdresse = 0;
    $besteller = 0;
    $sql = "SELECT rechnungsadresse AS adresse, status, `besteller_id` AS besteller FROM buchung WHERE id = " . $idBuchung;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $idAdresse = $row[ 'adresse' ];
        $besteller = $row[ 'besteller' ];
      }
    }
    if ( $idAdresse != null )
    {
      $this->loadAdressById( $db, $idAdresse );
    }
    if ($besteller == null)
    {
      if ( $idAdresse == null )
      {
        $this->id = -5;
      }
      $this->vorname = '(Barkasse)' . $this->vorname;
    }

  }
}
