<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 10.08.2016
 * Time: 16:41
 */
class BenutzerListe
{
  public $liste = array();

  public function searchByName(mysqli $db, $name)
  {
    $idList = array();
    $sql = "SELECT id FROM `user` WHERE LOWER(`name`) LIKE LOWER('%".$name."%') OR LOWER(mail) LIKE LOWER('%".$name."%')" ;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        array_push( $idList, $row['id'] );
      }
    }
    foreach ($idList as $id)
    {
      $benutzer = new Benutzer();
      $benutzer->loadById( $db, $id );
      array_push( $this->liste, $benutzer );
    }
  }
}
