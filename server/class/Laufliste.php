<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 02.08.2016
 * Time: 12:44
 */
class Laufliste
{

  public function generate ( mysqli $db )
  {
    //bezahlte TN holen
    $streckenListen = array();
    $sql = "SELECT tnZeit.id, tnZeit.strecke, tnZeit.sollZeit  FROM `tnZeit`, teilnehmer, buchung "
           . "WHERE buchung.status= 'bezahlt' AND tnZeit.teilnehmer_id = teilnehmer.id AND teilnehmer.buchung_id = buchung.id "
           . "ORDER BY tnZeit.strecke, tnZeit.sollZeit";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $streckenListen[ $row[ 'strecke' ] ];
      }
    }

  }

  public function update ( mysqli $db )
  {

  }

  public function sendLaufInfoAnTeilnehmer ( mysqli $db )
  {
    $sql = "SELECT tnZeit.strecke, tnZeit.teilnehmer_id, teilnehmer.vorname, teilnehmer.nachname, besteller.email FROM tnZeit, `teilnehmer`, buchung, besteller WHERE tnZeit.teilnehmer_id = teilnehmer.id AND teilnehmer.buchung_id = buchung.id AND buchung.besteller_id = besteller.id AND buchung.status != 'storniert'";
    
  }

}

class Lauf
{
  public $id = -1;
  public $start = '';
  public $bahn = -1;
  public $lauf = -1;
  public $tnZeit ;

  public function __construct ()
  {
    $this->tnZeit = new Challenge();
  }
}
