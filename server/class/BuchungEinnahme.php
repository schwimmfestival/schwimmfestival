<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 23.07.2016
 * Time: 23:54
 */

/*
 * Hilfsklasse für Buchungen
 */
class BuchungEinnahme
{
  public $id = -1;
  public $artikel = -1;
  public $teilnehmer = -1;

  public function addToDatenbank(mysqli $db, $buchungId)
  {
    $sql = "INSERT INTO `buchung_einnahme`( `bu_artikel_id`, `teilnehmer_id`, `buchung_id`) VALUES "
           ."(".$this->artikel.", ".$this->teilnehmer.", ".$buchungId.")";
    $db->query( $sql );
    $this->id = $db->insert_id;
  }

}
