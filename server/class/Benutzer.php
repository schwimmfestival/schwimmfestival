<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 15.04.2016
 * Time: 18:38
 */
class Benutzer
{
  /**
   * id des Benutzers um ihn mit Aktionen in Verbindung bringen zu können.
   * @var int
   */
  public $id = - 1;
  /**
   * Vorname und Nachname des Benutzers.
   * @var string
   */
  public $name = '';
  /**
   * E-Mail-Adresse des Benutzers
   * @var string
   */
  public $mail = '';
  /**
   * Object welches die Rechte des Benutzers enthält.
   * @var BenutzerRechte
   */
  public $rechte;

  /**
   * Passwort Hash des Benutzers.
   * @var
   */
  private $pw;

  public $session = '';

  public function __construct ()
  {
    $this->rechte = new BenutzerRechte();
  }

  public function loadById(mysqli $db, $id)
  {
    $sql = "SELECT id, name, mail, pw FROM `user` WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->name = $row[ 'name' ];
        $this->id = $row[ 'id' ];
        $this->pw = $row[ 'pw' ];
        $this->mail = $row[ 'mail' ];
      }
    }
    $this->rechte->loadRechteByBenutzerId( $db, $this->id );
  }

  /**
   * Lädt die Daten eines Benutzers aus der Datenbank sofern Passwort und E-Mail passen.
   *
   * @param mysqli $db
   * @param        $mail
   * @param        $pw
   */
  public function loadBenutzer ( mysqli $db, $mail, $pw )
  {
    $this->mail = $mail;
    $sql = "SELECT user.name, user.pw, user.id "
           . "FROM user "
           . " WHERE user.mail = '" . $mail . "' ";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $_SESSION[ 'username' ] = $row[ 'name' ];
        $this->name = $row[ 'name' ];
        $this->id = $row[ 'id' ];
        $this->pw = $row[ 'pw' ];
        if ( password_verify( $pw, $row[ 'pw' ] ) )
        {
          $_SESSION[ 'login' ] = true;
          $this->session = session_id();
        }
        else
        {
          $this->id = - 1;
          $this->mail = '';
          $this->name = '';
          return;
        }
      }
    }
    $this->rechte->loadRechteByBenutzerId( $db, $this->id );
  }

  /**
   * Prüft ob das eingegebene Passwort zum Passwort des Benutzers mit der angegebenen E-Mail passt.
   *
   * @param $mail
   * @param $pw
   *
   * @return bool
   */
  public function verify ( $mail, $pw )
  {
    if ( password_verify( $pw, $this->pw ) AND $mail == $this->mail )
    {
      return true;
    }
    return false;
  }

  /**
   * Fügt der Datenbank anhand eines Json Objekts einen neuen Benutzer hinzu.
   *
   * @param mysqli $db
   * @param        $jsonUser
   *
   * @return bool
   *          false sofern der Benutzer nicht hinzugefügt wurde.
   */
  public function insertNewUser ( mysqli $db, $jsonUser )
  {
    //{"id":"5","name":"Test AddUser","mail":"test2@test.de", "passwort":"testfrau2@musterfrau.de",
    // "rechte":{"rechte":[{"id":"1","name":"admin"},{"id":"5","name":"anmeldung"},{"id":"3","name":"essenskasse"},{"id":"4","name":"rechnungen"},{"id":"2","name":"tageskasse"}]}}';
    if ( strlen( $jsonUser->{'passwort'} ) > 7 )
    {
      $this->decryptPw( $jsonUser->{'passwort'} );
      $sql = "INSERT" . " INTO `user`( `name`, `mail`, `pw`) VALUES ('" . $jsonUser->{'name'} . "', '" . $jsonUser->{'mail'} . "', '"
             . $this->pw . "')";
      $userId = - 1;

      if ( $db->query( $sql ) )
      {
        $userId = $db->insert_id;
      }
      else
      {
        return false;
      }

      $sql = "INSERT " . "INTO userHatRechte (user_id, user_rechte_id) VALUES ";

      $i = 0;
      foreach ( $jsonUser->{'rechte'}->rechte as $recht )
      {
        if ( $i > 0 )
        {
          $sql .= ",";
        }
        $sql .= "(" . $userId . ", " . $recht->id . ")";
        $i ++;
      }
      if ( !$db->query( $sql ) )
      {
        return false;
      }
      else
      {
        $this->loadById( $db, $userId );
        return true;
      }
    }
    return false;
  }

  private function decryptPw($pw)
  {
    //Option to decrypt the password
    $options = [
      'cost' => 12,
    ];
    $this->pw = password_hash( $pw, PASSWORD_BCRYPT, $options );

  }

  public function deserialize($json)
  {
    $this->id = $json->{'id'};
    $this->mail = $json->{'mail'};
    $this->name = $json->{'name'};
    $this->decryptPw( $json->{'passwort'} );
  }

  public function updatePw ( mysqli $db)
  {
    $sql = "UPDATE `user` SET `pw` = '".$this->pw."' WHERE `user`.`id` = " . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function updateName(mysqli $db)
  {
    $sql = "UPDATE `user` SET `name` = '".$this->name."' WHERE `user`.`id` = " . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function updateMail(mysqli $db)
  {
    $sql = "UPDATE `user` SET `mail` = '".$this->mail."' WHERE `user`.`id` = " . $this->id;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function addRecht(mysqli $db, $idRecht)
  {
    $sql = "INSERT INTO `userHatRechte` (`user_id`, `user_rechte_id`) VALUES (".$this->id.", ".$idRecht.")";
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function removeRecht(mysqli $db, $idRecht)
  {
    $sql = "DELETE FROM `userHatRechte` WHERE `userHatRechte`.`user_id` = ".$this->id." AND `userHatRechte`.`user_rechte_id` = " . $idRecht;
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }
}

class BenutzerRechte
{
  /**
   * Enthält ein Array von Rechten
   * @var Recht[]
   */
  public $rechte;

  public function __construct ()
  {
    $this->rechte = array();
  }

  /**
   * Lädt die Rechte eines Benutzers
   *
   * @param mysqli $db
   * @param        $idBenutzer
   */
  public function loadRechteByBenutzerId ( mysqli $db, $idBenutzer )
  {
    $sql = "SELECT " . "user_rechte.id, user_rechte.name FROM userHatRechte, user_rechte "
           . "WHERE userHatRechte.user_rechte_id = user_rechte.id "
           . "AND userHatRechte.user_id = " . $idBenutzer;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $recht = new Recht();
        $recht->id = $row[ 'id' ];
        $recht->name = $row[ 'name' ];
        array_push( $this->rechte, $recht );
      }
    }
  }

  /**
   * Lädt die Liste der möglichen Rechte
   *
   * @param mysqli $db
   */
  public function loadRechteBenutzer ( mysqli $db )
  {
    $sql = "SELECT " . "user_rechte.id, user_rechte.name FROM user_rechte ";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $recht = new Recht();
        $recht->id = $row[ 'id' ];
        $recht->name = $row[ 'name' ];
        array_push( $this->rechte, $recht );
      }
    }
  }
}

class Recht
{
  /**
   * id des Benutzerrechts. Diese ist für die Zuordnung von einem Benutzer zu einem Benutzerrecht von Entscheidung.
   * @var int
   */
  public $id = - 1;
  /**
   * Bezeichnung des Benutzerrechts wie z.B. admin
   * @var string
   */
  public $name = '';

  public function __construct ()
  {
  }

}
