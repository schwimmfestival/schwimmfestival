<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 24.07.2016
 * Time: 00:22
 */
class ArtikelKategorie
{
  public $id = -1;
  public $name = '';
  public $kuerzel = '';

  private static $kategorien = array();

  public function loadById(mysqli $db, $id)
  {
    if(count( ArtikelKategorie::$kategorien ) < 1)
    {
      ArtikelKategorie::load( $db );
    }
    $kategorie = ArtikelKategorie::$kategorien[$id];
    $this->id = $id;
    $this->name = $kategorie->name;
    $this->kuerzel = $kategorie->kuerzel;
  }

  public static function load(mysqli $db)
  {
    $sql = "SELECT id, kategorie AS name, katKurz AS kuerzel FROM bu_kategorien" ;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $kategorie = new ArtikelKategorie();
        $kategorie->id = $row['id'];
        $kategorie->name = $row['name'];
        $kategorie->kuerzel = $row['kuerzel'];
        ArtikelKategorie::$kategorien[$row['id']] = $kategorie;
      }
    }
  }

}
