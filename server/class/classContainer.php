<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 13.04.2016
 * Time: 19:26
 */

require 'Adresse.php';
require 'Artikel.php';
require 'ArtikelKategorie.php';
require 'ArtikelRechnungshinweis.php';
require 'Benutzer.php';
require 'BenutzerListe.php';
require 'Besteller.php';
require 'Buchung.php';
require 'BuchungEinnahme.php';
require 'Challenge.php';
require 'group.php';
require 'Gruppe.php';
require 'Laufliste.php';
require 'Mahnung.php';
require 'PdfDLRG.php';
require 'Rechnung.php';
require 'Rechnungsadresse.php';
require 'Strecke.php';
require 'Teilnehmer.php';
require 'TeilnehmerSuche.php';
