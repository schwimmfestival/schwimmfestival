<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 24.07.2016
 * Time: 17:12
 */
class Strecke
{
  public $id = - 1;
  public $strecke = - 1;
  public $zeitEintrag = '';
  public $code = '';

  public function loadById ( mysqli $db, $id )
  {
    $sql = "SELECT id, strecke, zeitEintrag, code FROM tnStrecke WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
        if ( $row[ 'strecke' ] != null )
        {
          $this->strecke = $row[ 'strecke' ];
        }
        if ( $row[ 'zeitEintrag' ] != null )
        {
          $this->zeitEintrag = $row[ 'zeitEintrag' ];
        }
        if ( $row[ 'code' ] != null )
        {
          $this->code = $row[ 'code' ];
        }
      }
    }
  }

  public function decodeJson($json)
  {
    $this->id = $json->{'id'};
    $this->strecke = $json->{'strecke'};
    $this->code = $json->{'code'};
  }

  public function add ( mysqli $db, $teilnehmer, $code )
  {
    $sql = "INSERT INTO tnStrecke (teilnehmer_id, code) VALUES ($teilnehmer, '" . $code . "')";
    $db->query( $sql );
    $this->loadById( $db, $db->insert_id );
  }

  public function setStrecke ( mysqli $db )
  {
    $sql = "UPDATE `tnStrecke` SET `strecke`= " . $this->strecke . ",`zeitEintrag`= CURRENT_TIMESTAMP WHERE id = '" . $this->id . "'";
    $db->query( $sql );
    $this->loadById( $db, $this->id );
  }

  public function delete(mysqli $db )
  {
    $sql = "DELETE FROM tnStrecke WHERE id = " . $this->id;
    $db->query( $sql );
  }

  public function getIdByCode($code)
  {
    $split = explode( 'X', $code );
    $id = $split[0] -1000;
    return $id;
  }

}
