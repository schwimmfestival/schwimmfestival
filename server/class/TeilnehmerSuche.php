<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 04.08.2016
 * Time: 15:58
 */
class TeilnehmerSuche
{
  public $teilnehmer = array();

  public function searchByString ( mysqli $db, $string )
  {
    $tnList = array();
    $sql = "SELECT id FROM teilnehmer WHERE `vorname` LIKE '%" . $string . "%' OR nachname LIKE '%" . $string . "%'" ;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        array_push( $tnList, $row[ 'id' ] );
      }
    }

    foreach ($tnList as $id)
    {
      $tn = new Teilnehmer();
      $tn->loadById( $db, $id );
      array_push( $this->teilnehmer, $tn );
    }


  }
}
