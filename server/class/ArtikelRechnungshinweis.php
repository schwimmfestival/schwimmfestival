<?php

/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 24.07.2016
 * Time: 00:18
 */
class ArtikelRechnungshinweis
{
  public $id = - 1;
  public $hinweis = '';

  private static $hinweisliste = array();

  public function loadById ( mysqli $db, $id )
  {
    if(count(ArtikelRechnungshinweis::$hinweisliste) < 1)
    {
      ArtikelRechnungshinweis::load( $db );
    }

    $this->id = $id;
    $this->hinweis = ArtikelRechnungshinweis::$hinweisliste[$id];
  }

  public static function load ( mysqli $db )
  {
    $sql = "SELECT * FROM bu_rechnung_hinweis";
    if ($result = $db -> query($sql)) {
      while($row = $result->fetch_assoc()){
        ArtikelRechnungshinweis::$hinweisliste[$row['id']] = $row['hinweis'];
      }
    }else{
      echo $db->error;
    }
  }
}
