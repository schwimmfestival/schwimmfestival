<?php

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 07.04.2016
 * Time: 18:13
 */
class Adresse
{
  public $id = -1;
  public $vorname = '';
  public $nachname = '';
  public $gender = '';
  public $firma = '';
  public $strasse = '';
  public $plz = '';
  public $ort = '';
  public $typ = 'Rechnungsanschrift';

  public function __construct ()
  {
  }

  public function loadAdressById ( mysqli $db, $id = 0 )
  {
    $sql = "SELECT " . "id, strasse, plz, ort, typ, vorname, nachname, geschlecht, firma FROM adresse "
           . "WHERE id = " . $id;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $this->id = $row[ 'id' ];
        $this->vorname = $row[ 'vorname' ];
        $this->nachname = $row[ 'nachname' ];
        $this->gender = $row[ 'geschlecht' ];
        $this->firma = $row[ 'firma' ];
        $this->strasse = $row[ 'strasse' ];
        $this->plz = $row[ 'plz' ];
        $this->ort = $row[ 'ort' ];
        $this->typ = $row[ 'typ' ];
      }
    }
  }

  public function updateAdresseInDatenbank ( mysqli $db )
  {
    $sql = "UPDATE `adresse` SET "
           . "`strasse` = '" . $this->strasse
           . "', `plz` = '" . $this->plz
           . "', `ort` = '" . $this->ort
           . "', `typ` = '" . $this->typ
           . "', `vorname` = '" . $this->vorname
           . "', `nachname` = '" . $this->nachname
           . "', `geschlecht` = '" . $this->gender
           . "', `firma` = ' " . $this->firma
           . "' WHERE `adresse`.`id` = " . $this->id;
    if ( !$db->query( $sql ) )
    {
      echo 'File: ' . __FILE__ . ' Line: ' . __LINE__ . ' SQL ' . $sql;
    }
  }

  public function addToDatenbank (mysqli $db, $besteller = 'NULL')
  {
    $sql = "INSERT INTO `adresse` ( `strasse`, `plz`, `ort`, `typ`, `besteller_id`, `vorname`, `nachname`, `geschlecht`, `firma`) VALUES "
           ."( '".$this->strasse."', '".$this->plz."', '".$this->ort."', '".$this->typ."', ".$besteller
           .", '".$this->vorname."', '".$this->nachname."', '".$this->gender."', '".$this->firma."')";
    if ( !$db->query( $sql ) )
    {
      echo 'File: ' . __FILE__ . ' Line: ' . __LINE__ . ' SQL ' . $sql;
    }
    $this->id = $db->insert_id;
  }

  public function deserializeJson ( $jsonAdresse )
  {
    $this->id = $jsonAdresse->{'id'};
    $this->vorname = $jsonAdresse->{'vorname'};
    $this->nachname = $jsonAdresse->{'nachname'};
    $this->gender = $jsonAdresse->{'gender'};
    $this->firma = $jsonAdresse->{'firma'};
    $this->strasse = $jsonAdresse->{'strasse'};
    $this->plz = $jsonAdresse->{'plz'};
    $this->ort = $jsonAdresse->{'ort'};
    $this->typ = $jsonAdresse->{'typ'};
  }

}

class Adressen
{
  /**
   * @var Adresse[]
   */
  public $adressen = array();

  public function __construct ()
  {

  }

  public function loadAdressen ( mysqli $db, $idBesteller )
  {

    $sql = "SELECT " . "id, strasse, plz, ort, typ, vorname, nachname, geschlecht, firma FROM adresse "
           . "WHERE besteller_id = " . $idBesteller;
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        $adresse = new Adresse();
        $adresse->id = $row[ 'id' ];
        $adresse->vorname = $row[ 'vorname' ];
        $adresse->nachname = $row[ 'nachname' ];
        $adresse->gender = $row[ 'geschlecht' ];
        $adresse->firma = $row[ 'firma' ];
        $adresse->strasse = $row[ 'strasse' ];
        $adresse->plz = $row[ 'plz' ];
        $adresse->ort = $row[ 'ort' ];
        $adresse->typ = $row[ 'typ' ];
        array_push( $this->adressen, $adresse );
      }
    }
  }
}
