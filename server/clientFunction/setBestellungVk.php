<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 27.03.2016
 * Time: 18:13
 */

require_once '../connect/connect_db.inc';
require '../class/classContainer.php';
require_once '../func/serverFunc.php';
require '../constants.php';

$db = getConnection();
getHeader( 'json' );

$bestellung = json_decode(file_get_contents("php://input")); 

$erfolg = true;

if ( property_exists( $bestellung, 'participant' ) )
{

  $rechnungsadresse = $bestellung->{'billing'};

  $idBesteller = 0;
  $idAdresse = 0;
  $idBuchung = 0;
  $bestellCode = '';

  //Besteller anlegen
  $insertBesteller = "INSERT INTO besteller (email, pw, isActive) VALUES ('";
  $insertBesteller .= $rechnungsadresse->{'mail'} . "', '";
  $optionsPassword = [
    'cost' => 12,
  ];
  $insertBesteller .= password_hash( "hallo", PASSWORD_BCRYPT, $optionsPassword );
  $insertBesteller .= "', 1)";

  if ( $db->query( $insertBesteller ) )
  {
    $idBesteller = $db->insert_id;
  }
  else
  {
    errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ ."</br> " . $insertBesteller );
    $erfolg = false;
  }

  //Adresse zum Besteller hinzufügen
  $insertAdresse = "INSERT INTO adresse ";
  $insertAdresse .= "(  besteller_id, vorname, nachname, geschlecht, firma, strasse, plz, ort, typ) VALUES (";
  $insertAdresse .= $idBesteller . ", '";
  $insertAdresse .= $rechnungsadresse->{'firstname'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'lastname'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'gender'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'company'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'street'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'zip'} . "', '";
  $insertAdresse .= $rechnungsadresse->{'city'};
  $insertAdresse .= "', 'Rechnung')";

  if ( $db->query( $insertAdresse ) )
  {
    $idAdresse = $db->insert_id;
  }
  else
  {
    errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ ."</br> " . $insertAdresse );
    $erfolg = false;
  }

  //Buchung anlegen
  $insertBuchung = "INSERT INTO buchung (status, versandart, besteller_id, rechnungsadresse) VALUES ('offen','";
  $insertBuchung .= $rechnungsadresse->{'deliveryType'} . "',";
  $insertBuchung .= $idBesteller . ", ".$idAdresse.")";
  if ( $db->query( $insertBuchung ) )
  {
    $idBuchung = $db->insert_id;
  }
  else
  {
    errorHandling( $db->error . "</br> " . $insertBuchung );
    $erfolg = false;
  }

  $updateBuchung = "UPDATE `buchung` SET `bestcode` = '";
  $bestellCode = generateBestellCode( $idBuchung );
  $updateBuchung .= $bestellCode;
  $updateBuchung .= "' WHERE buchung.id = " . $idBuchung;
  if ( !$db->query( $updateBuchung ) )
  {
    errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ ."</br> " . $updateBuchung );
    $erfolg = false;
  }

  //Teilnehmer hinzufügen

  foreach ( $bestellung->{'participant'} as $participant )
  {
    $idTeilnehmer = 0;

    $insertTN = "INSERT INTO `teilnehmer` ( `vorname`, `nachname`, `gebdat`, `gender`, `buchung_id`) VALUES ( '";
    $insertTN .= $participant->{'vorname'} . "','";
    $insertTN .= $participant->{'nachname'} . "','";
    $gebDate = str_split( $participant->{'gebDate'}, 10 );
    $insertTN .= $gebDate[ 0 ] . "','";
    $insertTN .= $participant->{'gender'} . "',";
    $insertTN .= $idBuchung . ")";
    if ( $db->query( $insertTN ) )
    {
      $idTN = $db->insert_id;
    }
    else
    {
      errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ ."</br> " . $insertTN );
      $erfolg = false;
    }

    //Einnahmen des TN hinzufügen
    foreach ( $participant->{'einnahmen'}  as $einnahme )
    {
      $insertEinnahme = "INSERT INTO `buchung_einnahme` (`teilnehmer_id`, `buchung_id`, `bu_artikel_id`) VALUES (";
      $insertEinnahme .= $idTN . ", " . $idBuchung . ", ". $einnahme->{'id'} . ")";
      if ( !$db->query( $insertEinnahme ) )
      {
        errorHandling( $db->error . "</br> " . $insertEinnahme );
        $erfolg = false;
      }
    }

    //Gö Challenge
    foreach ( $participant->{'challenges'} as $challenge )
    {
      $insertChallenge = "INSERT INTO `tnZeit` (`strecke`, `sollZeit`, `teilnehmer_id`) VALUES ('";
      $insertChallenge .= $challenge->{'strecke'} . "','";
      $insertChallenge .= $challenge->{'zeitErwartet'} . "',";
      $insertChallenge .= $idTN . ")";
      if ( !$db->query( $insertChallenge ) )
      {
        errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ ."</br> " . $insertEinnahme );
        $erfolg = false;
      }
    }

    //Gruppen hinzufügen
    foreach ( $participant->{'groups'} as $gruppe )
    {
      $insertGruppe = "INSERT INTO `gruppeZuTn` (`teilnehmer_id`, `gruppe_id`) VALUES (";
      $insertGruppe .= $idTN . ",";
      $insertGruppe .= $gruppe . ")";
      if ( !$db->query( $insertGruppe ) )
      {
        errorHandling( $db->error . "</br> " . $insertGruppe );
        $erfolg = false;
      }
    }
  }

  //Mail an Besteller schicken zwecks validierung der E-Mail Adresse
  $erfolg = sendValidationMail( $rechnungsadresse, $idBesteller, $idBuchung, $db );

  $result = 'false';
  if ( $erfolg )
  {
    $result = 'true';
  }
  echo '{"erfolg":' . $result . '}';

}
