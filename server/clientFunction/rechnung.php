<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 14:43
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$json = json_decode( file_get_contents( "php://input" ) );

getHeader( 'json' );

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );

  if ( in_array( 'rechnungen', $userRechte ) )
  {
    $jobDone = true;
    $rechnung = new Rechnung();
    $db = getConnection();

    $isMail = true;
    if(isset( $_GET[ 'isMail' ] ))
    {
      $isMail = boolConverterString( $_GET[ 'isMail' ] );
    }

    if ( isset( $_GET[ 'bezahlt' ] ) && isset( $_GET[ 'geldeingang' ] ) )
    {
      $jobDone = $rechnung->setBezahlt( $db, $_GET[ 'bezahlt' ], prooveDate( $_GET[ 'geldeingang' ] ), $isMail );
    }
    if(isset( $_GET[ 'buchung' ] ))
    {
      $rechnung->loadRechnungByBuchungId( $db, $_GET[ 'buchung' ] );
    }
    if(isset( $_GET[ 'rechnung' ] ))
    {
      $rechnung->loadRechnungById( $db, $_GET[ 'rechnung' ] );
      if ( in_array( 'admin', $userRechte ) )
      {
        if(isset( $_GET[ 'updateRechnung' ]  ))
        {
          $rechnung->updateRechnung( $db );
        }
      }
    }
    if(isset( $_GET[ 'storniert' ] ))
    {
      $jobDone = $rechnung->setStorniert( $db, $_GET[ 'storniert' ], $isMail );
    }
    if(isset( $_GET[ 'mahnung' ] ) && isset( $_GET[ 'typ' ] ))
    {
      $rechnung->addMahnung( $db, $_GET[ 'mahnung' ], $_GET[ 'typ' ] );
    }
    if(isset( $_GET[ 'rechnungsliste' ] ))
    {
      $rechnListe = new RechnungsListe();
      $rechnListe->loadList($db, $_GET[ 'rechnungsliste' ]);

      echo json_encode($rechnListe);
    }
    else if ( $jobDone )
    {
      echo json_encode( $rechnung );
    }
    else
    {
      echo '{FALSE}';
    }
  }
  else
  {
    echo '{FALSE}';
  }
}
else
{
  echo '{FALSE}';
}

