<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 16.04.2016
 * Time: 18:38
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

$db = getConnection();

getHeader('json');

/*
 * Benutzerrechte können nur abgerufen werden, wenn ein Benutzer angemeldet ist.
 */
if($_SESSION['login'] == true)
{
    $listeRechte = new BenutzerRechte();
    $listeRechte->loadRechteBenutzer($db);
    echo json_encode($listeRechte);
}
else
{
    echo '{FALSE}';
}