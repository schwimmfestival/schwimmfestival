<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 07.04.2016
 * Time: 19:14
 */

if (!isset($_SESSION)) {
  session_start();
}

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

$db = getConnection();

$user = json_decode(file_get_contents("php://input"));

getHeader('json');
if(isset($user->{'mail'}))
{
    $_SESSION['login'] = false;
    $_SESSION['user'] ;
    $_SESSION['userRechte'] ;

    $benutzer = new Benutzer();
    $benutzer->loadBenutzer($db, $user->{'mail'}, $user->{'passwort'});
    if($_SESSION['login'])
    {
        echo json_encode($benutzer);
        $_SESSION['user'] = serialize($benutzer);
        $arrayRechte = array();
        foreach($benutzer->rechte->rechte as $recht){
            $arrayRechte[$recht->id] = $recht->name;
        }
        $_SESSION['userRechte'] = serialize($arrayRechte);
    }
}
else
{
    $_SESSION['login'] = false;
    session_destroy();
}
if(!$_SESSION['login'])
{
    echo '{FALSE}';
}
