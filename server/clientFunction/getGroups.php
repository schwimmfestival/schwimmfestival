<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 25.03.2016
 * Time: 12:05
 */

require_once '../connect/connect_db.inc';
require_once '../class/classContainer.php';
require_once '../func/getHeader.php';

$db = getConnection();
getHeader('json');

$sql = "SELECT gruppe.id, gruppe.name, gruppe.gruppe_typ_id AS idKategorie, gruppe_typ.name AS kategorie ";
$sql .= "FROM gruppe, gruppe_typ ";
$sql .= "WHERE gruppe.gruppe_typ_id = gruppe_typ.id ";

if(isset($_GET['kategorie'])){
    $sql .= " AND gruppe.gruppe_typ_id = " .  $_GET['kategorie'];
}
if(isset($_GET['nichtKategorie'])){
    $sql .= " AND gruppe.gruppe_typ_id != " .  $_GET['nichtKategorie'];
}
$sql .= " ORDER BY gruppe_typ.name, gruppe.name";

$gruppenListe = array();

if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()) {
        $gruppe = new Gruppe();
        $gruppe->id = $row['id'];
        $gruppe->name = $row['name'];
        $gruppe->idKategorie = $row['idKategorie'];
        $gruppe->kategorieName = $row['kategorie'];

        $gruppenListe[$row['id']] = $gruppe;
    }
} else{
    echo $db->error;
    echo "</br> " . $sql;
}

/*
 *
 $sql = "SELECT gruppe_typ.id, gruppe_typ.name AS kategorie";
$sql .= " FROM gruppe_typ WHERE 1 ORDER BY gruppe_typ.name";

$gruppenListe = array();

if ($result = $db -> query($sql)) {
    $gruppen = array();
    while($row = $result->fetch_assoc()){


        $sql2 = "SELECT gruppe.id, gruppe.name, gruppe.gruppe_typ_id AS idKategorie, gruppe_typ.name AS kategorie";
        $sql2 .= "FROM gruppe, gruppe_typ WHERE gruppe.gruppe_typ_id = ". $row['id'];
        $sql2 .= " ORDER BY gruppe.name";
        $result2 = $db -> query($sql2);
        while($row2 = $result2->fetch_assoc()){
            $gruppe = new gruppe();
            $gruppe->id = $row2['id'];
            $gruppe->name = $row2['name'];
            $gruppe->idKategorie = $row2['idKategorie'];
            $gruppe->kategorieName = $row['kategorie'];
            $gruppen[$row2['id']] = $gruppe;
        }
        if (isset($_GET['kategorie'])) {
            $gruppenListe[$row['kategorie']] = $gruppen;
            $gruppen = array();
        }
        else{
            $gruppenListe = $gruppen;
        }

    }
}else{
    echo $db->error;
}
*/


echo json_encode($gruppenListe);
