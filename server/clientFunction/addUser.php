<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 14.04.2016
 * Time: 00:04
 */
session_start();

error_reporting( E_ALL );
ini_set( 'display_errors', '1' );


require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

getHeader('json');

$userRechte = unserialize($_SESSION['userRechte']);

if($_SESSION['login'] == true
    AND in_array('admin', $userRechte))
{
    $db = getConnection();

    $jsonUser = json_decode(file_get_contents("php://input"));
    $jsonUser = json_decode($testUser);
    $newUser = new Benutzer();
    if($newUser->insertNewUser($db, $jsonUser))
    {
        echo '{TRUE}';
    }
    else
    {
        echo '{FALSE2}';
    }
}
// Kein User angemeldet oder Usre hat keine admin rechte.
else
{
    echo '{FALSE}';
}
