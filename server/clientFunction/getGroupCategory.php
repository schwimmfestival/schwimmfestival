<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 25.03.2016
 * Time: 13:14
 */

require_once '../connect/connect_db.inc';
require '../class/classContainer.php';

$db = getConnection();

$sql = "SELECT gruppe_typ.id, gruppe_typ.name AS kategorie";
$sql .= " FROM gruppe_typ WHERE 1 ORDER BY gruppe_typ.name";

$kategorien = array();

if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()){
        $kategorien[$row['id']] = $row['kategorie'];
    }
}else{
    echo $db->error;
}



header('Content-type: application/json');
echo json_encode($kategorien);
