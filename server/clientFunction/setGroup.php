<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 27.03.2016
 * Time: 15:47
 */

require_once '../connect/connect_db.inc';
require_once '../class/classContainer.php';
require_once '../func/getHeader.php';

$db = getConnection();
getHeader('json');


/*
$insert = "INSERT INTO temp (was, content) VALUES ('";
$insert .= "GET', '";
$insert .= $_SERVER['QUERY_STRING'] . "'),('";
$insert .= "POST', '";
$insert .= serialize($_POST) . "'),('";
$insert .= 'file_get_contents("php://input")' . "', '";
$json = json_decode(file_get_contents("php://input"));
$insert .= "name:" . $json->{'name'} . ", Kategorie:" . $json->{'idKategorie'} . " ";
$insert .= file_get_contents("php://input") . "')";

if ( $db -> query($insert)) {}else{
    echo $db->error . $insert;
}
*/

$gruppeJson = json_decode(file_get_contents("php://input"));

if(isset($gruppeJson->{'name'})) {

    $idGruppe = 0;


    $sql = "INSERT INTO gruppe ";
    $sql .= "( name, gruppe_typ_id) VALUES ('";
    $sql .= $gruppeJson->{'name'} . "', '";
    $sql .= $gruppeJson->{'idKategorie'};
    $sql .= " ')";

    if ($db->query($sql)) {
        $idGruppe = $db->insert_id;

        $selectGroup = "SELECT gruppe.id, gruppe.name, gruppe.gruppe_typ_id AS idKategorie, gruppe_typ.name AS kategorie ";
        $selectGroup .= "FROM gruppe, gruppe_typ ";
        $selectGroup .= "WHERE gruppe.gruppe_typ_id = gruppe_typ.id AND gruppe.id = " . $idGruppe;
        if ($result = $db->query($selectGroup)) {
            while ($row = $result->fetch_assoc()) {
                $gruppe = new Gruppe();
                $gruppe->id = $row['id'];
                $gruppe->name = $row['name'];
                $gruppe->idKategorie = $row['idKategorie'];
                $gruppe->kategorieName = $row['kategorie'];
            }

            echo json_encode($gruppe);
        } else {
            echo $db->error;
            echo "</br> " . $selectGroup;
        }
    } else {
        echo $db->error;
        echo "</br> " . $sql;
    }
}