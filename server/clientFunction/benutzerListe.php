<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 10.08.2016
 * Time: 16:53
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$benutzerJson = json_decode( file_get_contents( "php://input" ) );

getHeader( 'json' );

if ( isset( $_SESSION[ 'login' ] ) && $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  $db = getConnection();
  $liste= new BenutzerListe();
  if ( isset( $_GET[ 'search' ] ) )
  {
    if ( in_array( 'admin', $userRechte ) )
    {
      $liste->searchByName( $db, $_GET[ 'search' ] );
    }
  }
  echo json_encode( $liste );
}
