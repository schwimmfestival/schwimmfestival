<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 05.04.2016
 * Time: 21:07
 */
require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

$teilnehmerAbfrage = json_decode(file_get_contents("php://input"));

$db = getConnection();
getHeader('json');

if(property_exists($teilnehmerAbfrage, 'gebDate') ){
    $tn = new Teilnehmer();
    $tn->loadSelfService( $db, $teilnehmerAbfrage );
    echo json_encode($tn);
}
