<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 07.04.2016
 * Time: 19:15
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';


$rechnung = json_decode(file_get_contents("php://input"));

//$_SESSION['login'] = true;
getHeader('json');
$userRechte = unserialize($_SESSION['userRechte']);
if($_SESSION['login'] == true
    AND in_array('rechnungen', $userRechte)){
    // rechte prüfen
    $db = getConnection();
    $rechnListe = new RechnungsListe();
    $rechnListe->loadList($db);

    echo json_encode($rechnListe);
}
else
{
    echo '{FALSE}';
}

