<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 24.07.2016
 * Time: 20:42
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  if ( in_array( 'anmeldung', $userRechte ) )
  {
    if ( isset( $_GET[ 'teilnehmer' ] ) )
    {
      $db = getConnection();
      $vorname = '';
      $nachname = '';

      $sql = "INSERT INTO `tnStrecke`(`teilnehmer_id`) VALUES (" . $_GET[ 'teilnehmer' ] . ")";
      if(!$db->query( $sql ))
      {
        echo $sql;
        echo $db->error;
      }
      $idStrecke = $db->insert_id;

      $sql = "SELECT `vorname`, `nachname` FROM `teilnehmer` WHERE id = " . $_GET[ 'teilnehmer' ];
      if ( $result = $db->query( $sql ) )
      {
        while ( $row = $result->fetch_assoc() )
        {
          $vorname = $row[ 'vorname' ];
          $nachname = $row[ 'nachname' ];
        }
      }

      $tempTeilnehmer = html_entity_decode( $vorname, ENT_NOQUOTES, 'ISO-8859-1' ) . ' ' . html_entity_decode( $nachname, ENT_NOQUOTES, 'ISO-8859-1' ) . ' ' .
                        $_GET[ 'teilnehmer' ];
      $idStrecke = $idStrecke + 1000;
      $idTn = $_GET[ 'teilnehmer' ] + 1000;
      $barcode = $idStrecke . 'X' . $idTn;
      $seitenformat = array( 105, 148 );
      $pdf = new PDF_Code39( 'P', 'mm', $seitenformat );
      $pdf->AliasNbPages();
      $pdf->SetLeftMargin( 5 );
      $pdf->SetTopMargin( 5 );
      $pdf->AddPage();
      $pdf->SetFont( 'Arial', '', 8 );
      $pdf->Code39( 4, 4, $barcode, 1, 6 );
      $pdf->SetXY( 4, 15 );
      $pdf->Cell( 0, 5, $tempTeilnehmer, 0, 1, 'L' );
      $pdf->Output();

    }
  }
}
