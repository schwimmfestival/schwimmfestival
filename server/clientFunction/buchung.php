<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 12:01
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$buchungJson = json_decode( file_get_contents( "php://input" ) );
$db = getConnection();

getHeader( 'json' );
$buchung = new Buchung();

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  $benutzer = unserialize( $_SESSION[ 'user' ] );


  if ( in_array( 'rechnungen', $userRechte ) )
  {
    if ( isset( $_GET[ 'status' ] ) )
    {
      $buchung->setStatus( $db, $_GET[ 'buchung' ], $_GET[ 'status' ] );
    }
    else
    {
      if ( isset( $_GET[ 'buchung' ] ) )
      {
        $buchung->loadById( $db, $_GET[ 'buchung' ] );
      }
    }

  }

  if ( isset( $_GET[ 'task' ] ) )
  {
    if ( in_array( 'tageskasse', $userRechte ) )
    {
      if ( $_GET[ 'task' ] == 'tageskasse' )
      {
        $buchung->einnahmeTageskasse( $db, $buchungJson, $benutzer->id );
      }
    }
    if ( in_array( 'rechnungen', $userRechte ) || in_array( 'tageskasse', $userRechte ) )
    {
      if ( $_GET[ 'task' ] == 'stornieren' )
      {
        $buchung->deserializeJson( $buchungJson );
        $buchung->setStorniert( $db, $buchung->id );
      }

    }
    if ( in_array( 'rechnungen', $userRechte ) )
    {
      if ( $_GET[ 'task' ] == 'validateBesteller' )
      {
        $buchung->deserializeJson( $buchungJson );
        $buchung->sendValidationMail( $db );
      }
      if ( $_GET[ 'task' ] == 'updateRechnungsadresse' )
      {
        $buchung->updateRechnungsadresse( $db, $buchungJson );
      }
    }
    if ( in_array( 'rechnungen', $userRechte ) || in_array( 'tageskasse', $userRechte ) || in_array( 'anmeldung', $userRechte ) )
    {
      if ( $_GET[ 'task' ] == 'loadById' )
      {
        $buchung->loadById( $db, $_GET[ 'id' ] );
      }

    }

  }
  echo json_encode( $buchung );
}
elseif ( isset( $_GET[ 'task' ] ) && $_GET[ 'task' ] == 'selfService' )
{
  if (isset( $_GET[ 'selfService' ]))
  {
    if ($_GET[ 'selfService' ] == 'buchung')
    {
      $buchung->loadByRechnungUndEmail( $db, $_GET[ 'rechnung' ], $_GET[ 'mail' ] );
    }

  }
}
else
{
  echo '{FALSE}';
}

