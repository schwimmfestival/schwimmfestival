<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 12.08.2016
 * Time: 00:27
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';


$db = getConnection();

if ( isset( $_SESSION[ 'login' ] ) && $_SESSION[ 'login' ] == true )
{

	$strecke = 0;
	$vorname = '';
	$nachname = '';
	$name = '';

	$sql = "SELECT SUM(strecke) AS strecke FROM `tnStrecke` WHERE strecke !=1 AND teilnehmer_id =".$_GET['tnId'];
	if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()){
      $strecke = $row['strecke'];
    }
  }else{
    echo $sql . "\n</br>";
    echo $db -> error;
  }

	$sql = "SELECT vorname, nachname FROM teilnehmer WHERE id =".$_GET['tnId'];
	if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()){
      $vorname = $row['vorname'];
      $nachname = $row['nachname'];
    }
  }else{
    echo $sql . "\n</br>";
    echo $db -> error;
  }

	$name = html_entity_decode($vorname, ENT_NOQUOTES, 'ISO-8859-1'). ' ' . html_entity_decode($nachname, ENT_NOQUOTES, 'ISO-8859-1');

	$pdf=new PDF_Code39('P', 'mm', 'A4');
	$pdf->AliasNbPages();
	$pdf->SetMargins(0,0,0);
	$pdf->SetAutoPageBreak(false, 1);
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',16);
	$pdf->SetXY(45, 125);
	$pdf->MultiCell(120,8,$name,0,'C');
	$pdf->SetX(45);
	$pdf->MultiCell(120,8,"ist vom\n13.08.2016 bis 14.08.2016",0,'C');
	$pdf->SetX(45);
	$pdf->MultiCell(120,8,$strecke . ' m geschwommen',0,'C');
	$pdf->SetFont('Arial','',10);

	$pdf->Image('../pic/unterschrift.jpg',83, 277, 40);
	$pdf->SetXY(45, 285);
	$pdf->MultiCell(110,8,'',0,'C');
	$pdf->Output();

}
