<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 31.03.2016
 * Time: 18:57
 */

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

$bestellung = json_decode(file_get_contents("php://input"));

$db = getConnection();
getHeader('json');

$tnList = new TNListe();
$idBuchung = validateBestellung($bestellung->{'rechnung'}, $bestellung->{'mail'}, $db);

if($idBuchung > -1){
    $tnList->loadTeilnehmerFromBuchung($idBuchung, $db);
}
echo json_encode($tnList);


/**
 * Validiert ob Rechnungsnummer zum angegebenen Besteller (email) vorhanden ist.
 * @param $rechnung
 * @param $db
 * @return int
 *      Buchungsnummer wenn die Validierung positiv ausfällt und -1 wenn die Validierung negativ ausfällt.
 */
function validateBestellung($rechnung, $mail, $db){
    $idBuchung = -1;
    $rechnungNummer = str_split($rechnung, 4);
    $sql = "SELECT buchung.id "
        . "FROM besteller, buchung, rechnung "
        . "WHERE besteller.id = buchung.besteller_id AND buchung.id = rechnung.buchung_id "
        . "AND besteller.email = '" . $mail . "' "
        . "AND rechnung.id = " . $rechnungNummer[1];

    if ($result = $db->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $idBuchung = $row['id'];
        }
    }else{
        $idBuchung = -1;
    }

    return $idBuchung;
}



