<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 04.08.2016
 * Time: 16:09
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

getHeader( 'json' );

if ( isset( $_SESSION[ 'login' ] )&& $_SESSION[ 'login' ] == true )
{
  $db = getConnection();
  $suche = new TeilnehmerSuche();
  if(isset( $_GET[ 'task' ] ))
  {
    if ($_GET[ 'task' ] == 'suche')
    {
      $suche->searchByString( $db, $_GET[ 'string' ] );
    }
  }
  echo json_encode( $suche );
}

