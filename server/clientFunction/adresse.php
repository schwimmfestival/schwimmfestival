<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 12:01
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$adresseJson = json_decode( file_get_contents( "php://input" ) );

getHeader( 'json' );

if($_SESSION[ 'login' ] == true)
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  if ( in_array( 'admin', $userRechte ) )
  {
    $db = getConnection();

    $adresse = new Adresse();
    if ( isset( $_GET[ 'id' ] ) )
    {
      $adresse->loadAdressById($db, $_GET[ 'id' ] );
    }
    else
    {
      $adresse->deserializeJson( $adresseJson );
      $adresse->updateAdresseInDatenbank( $db );
    }
    echo json_encode( $adresse );
  }
  else
  {
    echo '{FALSE}';
  }
}
else
{
  echo '{FALSE}';
}
