<?php
/**
 * @author Florian
 */

require_once '../connect/connect_db.inc';
require_once '../class/classContainer.php';
require_once '../func/getHeader.php';


getHeader('json');
$db = getConnection();

$sql = "SELECT bu_artikel.id, bu_artikel.name, FORMAT(bu_artikel.preis, 2) AS preis, bu_artikel.bu_rechnung_hinweis_id, bu_artikel.steuer, bu_kategorien.kategorie";
$sql .= " FROM bu_artikel,bu_kategorien ";
$sql .= " WHERE bu_artikel.bu_kategorien_id = bu_kategorien.id ";
$sql .= " AND bu_artikel.jahr = YEAR(CURDATE()) ";

if (isset($_GET['kategorie'])) {
    $sql .= " AND bu_kategorien.id = " . $_GET['kategorie'];
}
elseif(isset($_GET['katkurz'])){
    $sql .= " AND bu_kategorien.katKurz = '" . $_GET['katkurz'] ."''";
}

$artikelList = array();

if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()){
        $artikel = new Artikel();
        $artikel->id = $row['id'];
        $artikel->name = $row['name'];
        $artikel->preis = $row['preis'];
        $artikel->steuer = $row['steuer'];
        $artikel->kategorie = $row['kategorie'];
        $artikel->rechnungshinweis = $row['bu_rechnung_hinweis_id'];

        $artikelList[$row['id']] = $artikel;
    }
}else{
    echo $db->error;
}

echo json_encode($artikelList);
