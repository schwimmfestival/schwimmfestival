<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 25.07.2016
 * Time: 21:33
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

getHeader( 'json' );

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  if ( in_array( 'anmeldung', $userRechte ) )
  {
    $db = getConnection();
    $challengeJson = json_decode( file_get_contents( "php://input" ) );

    $challenge = new Challenge();
    if(isset( $_GET['task'] ))
    {
      $challenge->decodeJson( $challengeJson );
      if($_GET['task'] == 'add' && isset( $_GET['teilnehmer'] ))
      {
        $challenge->addChallenge( $db, $_GET['teilnehmer'] );
      }
      else if($_GET['task'] == 'loadById')
      {
        $challenge->loadById( $db, $_GET['id'] );
      }
      else if($_GET['task'] == 'update')
      {
        $challenge->updateChallenge( $db );
      }
      else if($_GET['task'] == 'setIstZeit')
      {
        $challenge->setIstZeit( $db );
      }
      else if($_GET['task'] == 'delete')
      {
        $challenge->deleteById( $db );
        $challenge = new Challenge();
      }

    }
    echo json_encode( $challenge );
  }
}
