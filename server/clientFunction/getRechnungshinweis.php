<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 25.03.2016
 * Time: 11:37
 */

require_once '../connect/connect_db.inc';
require '../class/classContainer.php';
require_once '../func/getHeader.php';

getHeader('json');
$db = getConnection();

$sql = "SELECT * FROM bu_rechnung_hinweis";

$rechnungshinweise = array();

if ($result = $db -> query($sql)) {
    while($row = $result->fetch_assoc()){
        $rechnungshinweise[$row['id']] = $row['hinweis'];
    }
}else{
    echo $db->error;
}

echo json_encode($rechnungshinweise);
