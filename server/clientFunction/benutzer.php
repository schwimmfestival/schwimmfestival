<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 01.08.2016
 * Time: 18:58
 */

session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$benutzerJson = json_decode( file_get_contents( "php://input" ) );

getHeader( 'json' );

if ( isset( $_SESSION[ 'login' ] ) && $_SESSION[ 'login' ] == true )
{
  $db = getConnection();
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  $benutzerSession = unserialize( $_SESSION[ 'user' ] );
  $benutzer = new Benutzer();
  $benutzer->deserialize( $benutzerJson );
  if (isset( $_GET['task'] ))
  {
    if(in_array( 'admin', $userRechte ))
    {
      if($_GET['task'] == 'add')
      {
        $benutzer->insertNewUser( $db, $benutzerJson );
      }
      elseif($_GET['task'] == 'updateUserPw')
      {
        $benutzer->updatePw( $db );
      }
      elseif($_GET['task'] == 'updateUserName')
      {
        $benutzer->updateName( $db );
      }
      elseif($_GET['task'] == 'updateUserMail')
      {
        $benutzer->updateMail( $db );
      }
      elseif($_GET['task'] == 'addRecht')
      {
        $benutzer->addRecht( $db, $_GET['recht'] );
      }
      elseif($_GET['task'] == 'removeRecht')
      {
        $benutzer->removeRecht( $db, $_GET['recht'] );
      }
    }
    if($_GET['task'] == 'updateMail')
    {
      $benutzer->id = $benutzerSession->{'id'};
      $benutzer->updateMail( $db );
      $_SESSION[ 'user' ] = serialize( $benutzer );
    }
    elseif($_GET['task'] == 'updateName')
    {
      $benutzer->id = $benutzerSession->{'id'};
      $benutzer->updateName( $db );
      $_SESSION[ 'user' ] = serialize( $benutzer );
    }
    elseif($_GET['task'] == 'updatePw')
    {
      $benutzer->id = $benutzerSession->{'id'};
      $benutzer->updateMail( $db );
      $_SESSION[ 'user' ] = serialize( $benutzer );
    }
  }
  echo json_encode( $benutzer );
}
