<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 01.08.2016
 * Time: 16:08
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$buchungJson = json_decode( file_get_contents( "php://input" ) );

getHeader( 'json' );

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  $benutzer = unserialize( $_SESSION[ 'user' ] );
  $db = getConnection();
  $buchungen = new Buchungen();

  if ( isset( $_GET[ 'task' ] ) )
  {
    if ( in_array( 'rechnungen', $userRechte ) || in_array( 'tageskasse', $userRechte ) )
    {
      if ( $_GET[ 'task' ] == 'listOfUser' )
      {
        if ( isset( $_GET[ 'anzahl' ] ) )
        {
          $buchungen->loadLastN( $db, $benutzer->id, $_GET[ 'anzahl' ] );
        }
        else
        {
          $buchungen->loadLastN( $db, $benutzer->id );
        }
      }

    }
    if (in_array( 'rechnungen', $userRechte ))
    {
      if ( $_GET[ 'task' ] == 'listByStatus' )
      {
        $buchungen->loadByStatus( $db, $_GET[ 'status' ] );
      }
    }

  }
  echo json_encode( $buchungen );
}
else
{
  echo '{FALSE}';
}


