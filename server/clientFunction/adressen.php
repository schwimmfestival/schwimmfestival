<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 13.07.2016
 * Time: 13:05
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

//$_SESSION['login'] = true;
getHeader( 'json' );

if ( $_SESSION[ 'login' ] == true )
{
  $db = getConnection();
  if($_GET[ 'besteller' ])
  {
    $adressen = new Adressen();
    $adressen->loadAdressen( $db, $_GET[ 'besteller' ] );
    echo json_encode( $adressen );
  }
  else
  {
    echo '{FALSE}';
  }

}
else
{
  echo '{FALSE}';
}
