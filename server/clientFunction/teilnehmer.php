<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 24.07.2016
 * Time: 19:26
 */

//error_reporting( E_ALL );
//ini_set( 'display_errors', '1' );

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

$teilnehmer = json_decode( file_get_contents( "php://input" ) );

$db = getConnection();
getHeader( 'json' );

$tn = new Teilnehmer();

if ( isset( $_GET[ 'teilnehmer' ] ) )
{
  if ( isset( $_GET[ 'gruppeloeschen' ] ) )
  {
    $sql = "DELETE FROM `gruppeZuTn` WHERE `teilnehmer_id` = " . $_GET[ 'teilnehmer' ] . " AND `gruppe_id` = " . $_GET[ 'gruppeloeschen' ];
    $db->query( $sql );
  }
  if ( isset( $_GET[ 'gruppeadd' ] ) )
  {
    $sql = "INSERT INTO `gruppeZuTn` (`teilnehmer_id`, `gruppe_id`) VALUES (" . $_GET[ 'teilnehmer' ] . ", " . $_GET[ 'gruppeadd' ] . " )";
    $db->query( $sql );
  }

  $tn->loadById( $db, $_GET[ 'teilnehmer' ] );

}

if ( isset( $_GET[ 'update' ] ) )
{
  $tn->update( $db, $teilnehmer );
}

//Tn anhand des Barcodes laden
if ( isset( $_GET[ 'checkIn' ] ) )
{
  $tn->checkIn( $db, $_GET[ 'checkIn' ] );
}

echo json_encode( $tn );
