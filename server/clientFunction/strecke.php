<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 28.07.2016
 * Time: 15:44
 */
session_start();

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/getHeader.php';

getHeader( 'json' );

if ( $_SESSION[ 'login' ] == true )
{
  $userRechte = unserialize( $_SESSION[ 'userRechte' ] );
  if ( in_array( 'anmeldung', $userRechte ) )
  {
    $db = getConnection();
    $streckeJson = json_decode( file_get_contents( "php://input" ) );

    $strecke = new Strecke();
    if(isset( $_GET['task'] ))
    {
      $strecke->decodeJson( $streckeJson );
      if($_GET['task'] == 'setStrecke')
      {
        $strecke->setStrecke( $db );
      }
      if($_GET['task'] == 'delete')
      {
        $strecke->delete( $db );
      }
      if ($_GET['task'] == 'loadById') {
        $strecke->loadById($db, $_GET['id']);
      }
    }
    echo json_encode( $strecke );
  }
}
