<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 01.08.2016
 * Time: 22:23
 */
session_start();

error_reporting( E_ALL );
ini_set( 'display_errors', '1' );

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$db = getConnection();

if ( isset( $_SESSION[ 'login' ] ) && $_SESSION[ 'login' ] == true )
{
  if ( isset( $_GET[ 'task' ] ) )
  {
    if ( $_GET[ 'task' ] == 'laufliste' )
    {

    }
    if ( $_GET[ 'task' ] == 'essen' )
    {
      echo '<h2>Essenskalkulation</h2>';
      $sql = "SELECT COUNT(bu_artikel_id) AS anzahl, bu_artikel.preis, bu_artikel.name, bu_kategorien.kategorie FROM `buchung_einnahme`, bu_artikel, bu_kategorien, buchung "
             .
             "WHERE buchung_einnahme.bu_artikel_id = bu_artikel.id AND bu_artikel.bu_kategorien_id = bu_kategorien.id AND buchung_einnahme.buchung_id = buchung.id "
             . "AND buchung.status != 'storniert' GROUP BY buchung_einnahme.bu_artikel_id";
      if ( $result = $db->query( $sql ) )
      {
        $colorswitch = true;
        echo '<table border="0"><tr><th>Anzahl</th><th>Preis</th><th>Artikel</th><th>Kategorie</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          if ( $colorswitch )
          {
            echo '<tr style="background-color: #eaeaea">';
          }
          else
          {
            echo '<tr>';
          }
          echo '<td>' . $row[ 'anzahl' ] . '</td>'
               . '<td>' . $row[ 'preis' ] . '</td>'
               . '<td>' . $row[ 'name' ] . '</td>'
               . '<td>' . $row[ 'kategorie' ] . '</td>'
               . '</tr>';
          $colorswitch = !$colorswitch;
        }
        echo '</table>';
        echo '<p>Anzahl Vollverpflegung: ' . anzahlVollverpflegung( $db ) . '</p>';

      }
    }
    if ( $_GET[ 'task' ] == 'einnahmen' )
    {
      echo '<h2>Einnahmen</h2>';
      $sql = "SELECT buchung.status, COUNT(bu_artikel_id) AS anzahl, bu_artikel.preis, bu_artikel.name, bu_kategorien.kategorie FROM "
             . "`buchung_einnahme`, bu_artikel, bu_kategorien, buchung "
             . "WHERE buchung_einnahme.bu_artikel_id = bu_artikel.id AND bu_artikel.bu_kategorien_id = bu_kategorien.id "
             .
             "AND buchung_einnahme.buchung_id = buchung.id GROUP BY buchung_einnahme.bu_artikel_id, buchung.status ORDER BY buchung.status, bu_artikel.name, bu_kategorien.kategorie";
      if ( $result = $db->query( $sql ) )
      {
        $colorswitch = true;
        echo '<table border="0"><tr><th>Status</th><th>Anzahl</th><th>Preis</th><th>Artikel</th><th>Kategorie</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          if ( $colorswitch )
          {
            echo '<tr style="background-color: #eaeaea">';
          }
          else
          {
            echo '<tr>';
          }
          echo '<td>' . $row[ 'status' ] . '</td>'
               . '<td>' . $row[ 'anzahl' ] . '</td>'
               . '<td>' . $row[ 'preis' ] . '</td>'
               . '<td>' . $row[ 'name' ] . '</td>'
               . '<td>' . $row[ 'kategorie' ] . '</td>'
               . '</tr>';
          $colorswitch = !$colorswitch;
        }
        echo '</table>';

        echo '<p>Anzahl Vollverpflegung: ' . anzahlVollverpflegung( $db ) . '</p>';
      }
    }
    if ( $_GET[ 'task' ] == 'goechallenge' )
    {
      echo '<h2>Gesamtliste G&ouml;-Challenge</h2>';
      $sql = "SELECT strecke, istZeit, teilnehmer_id AS idTn, teilnehmer.vorname AS vorname, teilnehmer.nachname AS name FROM `tnZeit`, teilnehmer "
             ."WHERE tnZeit.teilnehmer_id = teilnehmer.id AND tnZeit.istZeit > 1 ORDER BY strecke, istZeit";
      if ( $result = $db->query( $sql ) )
      {
        $colorswitch = true;
        echo '<table border="0"><tr><th>Strecke</th><th>Zeit (hh:mm:ss.%)</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          if ( $colorswitch )
          {
            echo '<tr style="background-color: #eaeaea">';
          }
          else
          {
            echo '<tr>';
          }
          echo '<td>' . $row[ 'strecke' ] . '</td>'
               . '<td>' . zeitAusMillisekunden( $row[ 'istZeit' ] ) . '</td>'
               . '<td>' . $row[ 'idTn' ] . '</td>'
               . '<td>' . $row[ 'vorname' ] . '</td>'
               . '<td>' . $row[ 'name' ] . '</td>'
               . '</tr>';
          $colorswitch = !$colorswitch;
        }
        echo '</table>';
      }
      echo '<h2>Gesamtliste G&ouml;-Challenge weiblich</h2>';
      challengeGenderGesamt( $db, 'w' );
      echo '<h2>Gesamtliste G&ouml;-Challenge m&auml;nnlich</h2>';
      challengeGenderGesamt( $db, 'm' );
      echo '<h2>Sch&uuml;lerinnen 2001-2008 G&ouml;-Challenge </h2>';
      challengeJahrGaenge( $db, 2001, 2008, 'w' );
      echo '<h2>Sch&uuml;ler 2001-2008 G&ouml;-Challenge </h2>';
      challengeJahrGaenge( $db, 2001, 2008, 'm' );
      echo '<h2>Jugend 1997-2000 G&ouml;-Challenge weiblich</h2>';
      challengeJahrGaenge( $db, 1997, 2000, 'w' );
      echo '<h2>Jugend 1997-2000 G&ouml;-Challenge m&auml;nnlich</h2>';
      challengeJahrGaenge( $db, 1997, 2000, 'm' );

      echo '<h2>Altersklassen </h2>';
      echo '<h3>Sch&uuml;lerinnen D 2007-2008 G&ouml;-Challenge</h3>';
      challengeJahrGaenge( $db, 2007, 2008, 'w' );
      echo '<h3>Sch&uuml;ler D 2007-2008 G&ouml;-Challenge </h3>';
      challengeJahrGaenge( $db, 2007, 2008, 'm' );
      echo '<h3>Sch&uuml;lerinnen C 2005-2006 G&ouml;-Challenge</h3>';
      challengeJahrGaenge( $db, 2005, 2006, 'w' );
      echo '<h3>Sch&uuml;ler C 2005-2006 G&ouml;-Challenge </h3>';
      challengeJahrGaenge( $db, 2005, 2006, 'm' );
      echo '<h3>Sch&uuml;lerinnen B 2003-2004 G&ouml;-Challenge</h3>';
      challengeJahrGaenge( $db, 2003, 2004, 'w' );
      echo '<h3>Sch&uuml;ler B 2003-2004 G&ouml;-Challenge </h3>';
      challengeJahrGaenge( $db, 2003, 2004, 'm' );
      echo '<h3>Sch&uuml;lerinnen A 2001-2002 G&ouml;-Challenge</h3>';
      challengeJahrGaenge( $db, 2001, 2002, 'w' );
      echo '<h3>Sch&uuml;ler A 2001-2002 G&ouml;-Challenge </h3>';
      challengeJahrGaenge( $db, 2001, 2002, 'm' );
      echo '<h3>Jugend B 1999-2000 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1999, 2000, 'w' );
      echo '<h3>Jugend B 1999-2000 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1999, 2000, 'm' );
      echo '<h3>Jugend A 1997-1998 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1997, 1998, 'w' );
      echo '<h3>Jugend A 1997-1998 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1997, 1998, 'm' );
      echo '<h3>AK 20 1987-1996 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1987, 1996, 'w' );
      echo '<h3>AK 20 1987-1996 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1987, 1996, 'm' );
      echo '<h3>AK 30 1982-1986 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1982, 1986, 'w' );
      echo '<h3>AK 30 1982-1986 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1982, 1986, 'm' );
      echo '<h3>AK 35 1977-1981 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1977, 1981, 'w' );
      echo '<h3>AK 35 1977-1981 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1977, 1981, 'm' );
      echo '<h3>AK 40 1972-1976 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1972, 1976, 'w' );
      echo '<h3>AK 40 1972-1976 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1972, 1976, 'm' );
      echo '<h3>AK 45 1967-1971 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1967, 1971, 'w' );
      echo '<h3>AK 45 1967-1971 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1967, 1971, 'm' );
      echo '<h3>AK 50 1962-1966 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1962, 1966, 'w' );
      echo '<h3>AK 50 1962-1966 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1962, 1966, 'm' );
      echo '<h3>AK 55 1957-1961 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1957, 1961, 'w' );
      echo '<h3>AK 55 1957-1961 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1957, 1961, 'm' );
      echo '<h3>AK 60 1952-1956 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1952, 1956, 'w' );
      echo '<h3>AK 60 1952-1956 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1952, 1956, 'm' );
      echo '<h3>AK 65 1947-1951 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1947, 1951, 'w' );
      echo '<h3>AK 65 1947-1951 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1947, 1951, 'm' );

      echo '<h3>AK 70 1942-1946 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1942, 1946, 'w' );
      echo '<h3>AK 70 1942-1946 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1942, 1946, 'm' );
      echo '<h3>AK 75 1937-1941 G&ouml;-Challenge weiblich</h3>';
      challengeJahrGaenge( $db, 1937, 1941, 'w' );
      echo '<h3>AK 75 1937-1941 G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1937, 1941, 'm' );
      echo '<h3>AK 80 1936- G&ouml;-Challenge m&auml;nnlich</h3>';
      challengeJahrGaenge( $db, 1902, 1936, 'm' );

    }
    if ( $_GET[ 'task' ] == 'strecken' )
    {
      echo '<h2>Streckenwertung</h2>';

      echo '<h3>j&uuml;ngste</h3>';
      $sql = "SELECT SUM(tnStrecke.strecke) as ges, teilnehmer.id as idTn, teilnehmer.vorname as vorname, teilnehmer.nachname as nachname, DATE_FORMAT(teilnehmer.gebdat, '%d.%m.%Y') as gebdat FROM `tnStrecke`, teilnehmer "
             ." WHERE tnStrecke.teilnehmer_id = teilnehmer.id AND tnStrecke.strecke > 1 AND teilnehmer.gender = 'w' Group BY teilnehmer.id ORDER BY teilnehmer.gebdat DESC LIMIT 1";
      if ( $result = $db->query( $sql ) )
      {
        echo '<table border="0"><tr><th>Strecke</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th><th>Geboren</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          echo '<tr>';
          echo '<td>' . $row[ 'ges' ] . '</td>'
               . '<td>' . $row[ 'idTn' ] . '</td>'
               . '<td>' . $row[ 'vorname' ] . '</td>'
               . '<td>' . $row[ 'nachname' ] . '</td>'
               . '<td>' . $row[ 'gebdat' ] . '</td>'
               . '</tr>';
        }
        echo '</table>';
      }

      echo '<h3>&auml;lteste</h3>';

      $sql = "SELECT SUM(tnStrecke.strecke) as ges, teilnehmer.id as idTn, teilnehmer.vorname as vorname, teilnehmer.nachname as nachname, DATE_FORMAT(teilnehmer.gebdat, '%d.%m.%Y') as gebdat FROM `tnStrecke`, teilnehmer "
             ." WHERE tnStrecke.teilnehmer_id = teilnehmer.id AND tnStrecke.strecke > 1 AND teilnehmer.gender = 'w' Group BY teilnehmer.id ORDER BY teilnehmer.gebdat ASC LIMIT 1";
      if ( $result = $db->query( $sql ) )
      {
        echo '<table border="0"><tr><th>Strecke</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th><th>Geboren</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          echo '<tr>';
          echo '<td>' . $row[ 'ges' ] . '</td>'
               . '<td>' . $row[ 'idTn' ] . '</td>'
               . '<td>' . $row[ 'vorname' ] . '</td>'
               . '<td>' . $row[ 'nachname' ] . '</td>'
               . '<td>' . $row[ 'gebdat' ] . '</td>'
               . '</tr>';
        }
        echo '</table>';
      }

      echo '<h3>j&uuml;ngster</h3>';

      $sql = "SELECT SUM(tnStrecke.strecke) as ges, teilnehmer.id as idTn, teilnehmer.vorname as vorname, teilnehmer.nachname as nachname, DATE_FORMAT(teilnehmer.gebdat, '%d.%m.%Y') as gebdat FROM `tnStrecke`, teilnehmer "
             ." WHERE tnStrecke.teilnehmer_id = teilnehmer.id AND tnStrecke.strecke > 1 AND teilnehmer.gender = 'm' Group BY teilnehmer.id ORDER BY teilnehmer.gebdat DESC LIMIT 1";
      if ( $result = $db->query( $sql ) )
      {
        echo '<table border="0"><tr><th>Strecke</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th><th>Geboren</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          echo '<tr>';
          echo '<td>' . $row[ 'ges' ] . '</td>'
               . '<td>' . $row[ 'idTn' ] . '</td>'
               . '<td>' . $row[ 'vorname' ] . '</td>'
               . '<td>' . $row[ 'nachname' ] . '</td>'
               . '<td>' . $row[ 'gebdat' ] . '</td>'
               . '</tr>';
        }
        echo '</table>';
      }

      echo '<h3>&auml;ltester</h3>';

      $sql = "SELECT SUM(tnStrecke.strecke) as ges, teilnehmer.id as idTn, teilnehmer.vorname as vorname, teilnehmer.nachname as nachname, DATE_FORMAT(teilnehmer.gebdat, '%d.%m.%Y') as gebdat FROM `tnStrecke`, teilnehmer "
             ." WHERE tnStrecke.teilnehmer_id = teilnehmer.id AND tnStrecke.strecke > 1 AND teilnehmer.gender = 'm' Group BY teilnehmer.id ORDER BY teilnehmer.gebdat ASC LIMIT 1";
      if ( $result = $db->query( $sql ) )
      {
        echo '<table border="0"><tr><th>Strecke</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th><th>Geboren</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          echo '<tr>';
          echo '<td>' . $row[ 'ges' ] . '</td>'
               . '<td>' . $row[ 'idTn' ] . '</td>'
               . '<td>' . $row[ 'vorname' ] . '</td>'
               . '<td>' . $row[ 'nachname' ] . '</td>'
               . '<td>' . $row[ 'gebdat' ] . '</td>'
               . '</tr>';
        }
        echo '</table>';
      }

      echo '<h3>weiblich ab 2003</h3>';
      streckeJahrGang($db, 2003, 2016, 'w');
      echo '<h3>m&auml;nnlich ab 2003</h3>';
      streckeJahrGang($db, 2003, 2016, 'm');
      echo '<h3>weiblich 1999-2002</h3>';
      streckeJahrGang($db, 1999, 2002, 'w');
      echo '<h3>m&auml;nnlich 1999-2002</h3>';
      streckeJahrGang($db, 1999, 2002, 'm');
      echo '<h3>weiblich 1957-1998</h3>';
      streckeJahrGang($db, 2003, 2016, 'w');
      echo '<h3>m&auml;nnlich 1957-1998</h3>';
      streckeJahrGang($db, 2003, 2016, 'm');
      echo '<h3>weiblich -1956</h3>';
      streckeJahrGang($db, 1902, 1956, 'w');
      echo '<h3>m&auml;nnlich -1956</h3>';
      streckeJahrGang($db, 1902, 1956, 'm');

      echo '<h2>Gruppen</h2>';
      $sql = "SELECT SUM(tnStrecke.strecke) as ges, gruppe.name as gruppe, gruppe_typ.name as typ FROM `tnStrecke`, gruppe, gruppeZuTn, gruppe_typ "
             ." WHERE tnStrecke.strecke > 1 AND tnStrecke.teilnehmer_id = gruppeZuTn.teilnehmer_id AND gruppeZuTn.gruppe_id = gruppe.id "
             ." AND gruppe.gruppe_typ_id = gruppe_typ.id Group BY gruppe.id ORDER BY gruppe.gruppe_typ_id, ges DESC";
      if ( $result = $db->query( $sql ) )
      {
        $colorswitch = true;
        echo '<table border="0"><tr><th>Strecke</th><th>Gruppe</th><th>Typ</th></tr>';
        while ( $row = $result->fetch_assoc() )
        {
          if ( $colorswitch )
          {
            echo '<tr style="background-color: #eaeaea">';
          }
          else
          {
            echo '<tr>';
          }
          echo '<td>' . $row[ 'ges' ] . '</td>'
               . '<td>' . $row[ 'gruppe' ] . '</td>'
               . '<td>' . $row[ 'typ' ] . '</td>'
               . '</tr>';
          $colorswitch = !$colorswitch;
        }
        echo '</table>';
      }
    }

  }

}

if ( isset( $_GET[ 'task' ] ) )
{
  if ( $_GET[ 'task' ] == 'gruppen' )
  {
    $sql = "SELECT COUNT(teilnehmer_id) AS anzahl, gruppe_id, gruppe.name, gruppe_typ.name AS typ FROM `gruppeZuTn`, gruppe, gruppe_typ WHERE gruppe_id = gruppe.id "
           . "AND gruppe.gruppe_typ_id = gruppe_typ.id GROUP BY gruppe_id ORDER BY gruppe_typ.id, gruppe.name";
    if ( $result = $db->query( $sql ) )
    {
      echo '<table border="1"><tr><th>Anzahl</th><th>Gruppe Name</th><th>Typ</th><th>ID Gruppe</th></tr>';
      while ( $row = $result->fetch_assoc() )
      {
        echo '<tr>'
             . '<td>' . $row[ 'anzahl' ] . '</td>'
             . '<td>' . $row[ 'name' ] . '</td>'
             . '<td>' . $row[ 'typ' ] . '</td>'
             . '<td>' . $row[ 'gruppe_id' ] . '</td>'
             . '</tr>';
      }
      echo '</table>';
    }
  }
}

function anzahlVollverpflegung ( mysqli $db )
{
  $sql = "SELECT count(einnahmen.tn) as vollverpflegungen FROM( SELECT COUNT(bu_artikel_id) as anzahl, teilnehmer_id as tn FROM `buchung_einnahme` "
         ." WHERE bu_artikel_id = 3 OR bu_artikel_id = 4 OR bu_artikel_id = 5 OR bu_artikel_id = 6 OR bu_artikel_id = 7 OR bu_artikel_id = 8 GROUP BY teilnehmer_id) "
         ." as einnahmen WHERE einnahmen.anzahl =3";
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      return $row[ 'vollverpflegungen' ];
    }
  }

}

function streckeJahrGang(mysqli $db, $vonJahr, $bisJahr, $gender)
{
  $sql = "SELECT SUM(tnStrecke.strecke) as ges, teilnehmer.id as idTn, teilnehmer.vorname as vorname, teilnehmer.nachname as nachname FROM `tnStrecke`, teilnehmer "
         ." WHERE tnStrecke.teilnehmer_id = teilnehmer.id AND tnStrecke.strecke > 1 AND teilnehmer.gender = '" . $gender
         ."' AND teilnehmer.gebdat >= '".$vonJahr."-01-01' AND teilnehmer.gebdat <= '".$bisJahr."-12-31' Group BY teilnehmer.id ORDER BY ges DESC";

  if ( $result = $db->query( $sql ) )
  {
    $colorswitch = true;
    echo '<table border="0"><tr><th>Strecke</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th></tr>';
    while ( $row = $result->fetch_assoc() )
    {
      if ( $colorswitch )
      {
        echo '<tr style="background-color: #eaeaea">';
      }
      else
      {
        echo '<tr>';
      }
      echo '<td>' . $row[ 'ges' ] . '</td>'
           . '<td>' . $row[ 'idTn' ] . '</td>'
           . '<td>' . $row[ 'vorname' ] . '</td>'
           . '<td>' . $row[ 'nachname' ] . '</td>'
           . '</tr>';
      $colorswitch = !$colorswitch;
    }
    echo '</table>';
  }
}

function challengeJahrGaenge ( mysqli $db, $vonJahr, $bisJahr, $gender )
{
  $sql = "SELECT strecke, istZeit, teilnehmer_id AS idTn, teilnehmer.vorname AS vorname, teilnehmer.nachname AS name FROM `tnZeit`, teilnehmer "
         ." WHERE tnZeit.teilnehmer_id = teilnehmer.id AND tnZeit.istZeit > 1 AND teilnehmer.gebdat >= '" . $vonJahr . "-01-01' "
         ." AND teilnehmer.gebdat <= '" . $bisJahr . "-12-31' AND teilnehmer.gender = '".$gender."' ORDER BY strecke, istZeit";
  if ( $result = $db->query( $sql ) )
  {
    $colorswitch = true;
    echo '<table border="0"><tr><th>Strecke</th><th>Zeit (hh:mm:ss.%)</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th></tr>';
    while ( $row = $result->fetch_assoc() )
    {
      if ( $colorswitch )
      {
        echo '<tr style="background-color: #eaeaea">';
      }
      else
      {
        echo '<tr>';
      }
      echo '<td>' . $row[ 'strecke' ] . '</td>'
           . '<td>' . zeitAusMillisekunden( $row[ 'istZeit' ] ) . '</td>'
           . '<td>' . $row[ 'idTn' ] . '</td>'
           . '<td>' . $row[ 'vorname' ] . '</td>'
           . '<td>' . $row[ 'name' ] . '</td>'
           . '</tr>';
      $colorswitch = !$colorswitch;
    }
    echo '</table>';
  }
}

function challengeGenderGesamt(mysqli $db, $gender)
{
  $sql = "SELECT strecke, istZeit, teilnehmer_id AS idTn, teilnehmer.vorname AS vorname, teilnehmer.nachname AS name FROM `tnZeit`, teilnehmer "
         ."WHERE tnZeit.teilnehmer_id = teilnehmer.id AND tnZeit.istZeit > 1 AND teilnehmer.gender = '".$gender."' ORDER BY strecke, istZeit";
  if ( $result = $db->query( $sql ) )
  {
    $colorswitch = true;
    echo '<table border="0"><tr><th>Strecke</th><th>Zeit (hh:mm:ss.%)</th><th>ID Teilnehmer</th><th>Vorname</th><th>Nachname</th></tr>';
    while ( $row = $result->fetch_assoc() )
    {
      if ( $colorswitch )
      {
        echo '<tr style="background-color: #eaeaea">';
      }
      else
      {
        echo '<tr>';
      }
      echo '<td>' . $row[ 'strecke' ] . '</td>'
           . '<td>' . zeitAusMillisekunden( $row[ 'istZeit' ] ) . '</td>'
           . '<td>' . $row[ 'idTn' ] . '</td>'
           . '<td>' . $row[ 'vorname' ] . '</td>'
           . '<td>' . $row[ 'name' ] . '</td>'
           . '</tr>';
      $colorswitch = !$colorswitch;
    }
    echo '</table>';
  }
}
