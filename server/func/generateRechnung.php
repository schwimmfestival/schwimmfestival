<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 27.03.2016
 * Time: 19:07
 */

function generateRechnung ( $idBuchung, mysqli $db, $update = false, $barkasse = false )
{
  $idRechnung = 0;
  $pdf = new PdfDLRG( 'P', 'mm', 'A4' );
  $pdf->iban = 'DE24 2605 0001 0056 0563 36';
  $pdf->rechnungDatum = date( "d.m.Y" );

  //Rechnung zu Buchung anlegen
  $sql = "SELECT rechnung.id AS rechnung FROM rechnung WHERE rechnung.buchung_id = " . $idBuchung;
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      $idRechnung = $row[ 'rechnung' ];
      $pdf->rechnungNummer = '24h-' . $idRechnung;
    }
  }
  if ( !$update && $idRechnung != 0 ) //Rechnung existiert und es soll kein update durchgeführt werden
  {
    if ( file_exists( '../Rechnungen/' . $pdf->rechnungNummer . "Rechnung.pdf" ) )
    {
      return false;
    }
  }
  if ( $idRechnung == 0 )
  {
    $sql = "INSERT INTO `rechnung`(`datum`, `buchung_id`) VALUES ('" . date( "Y-m-d" ) . "', " . $idBuchung . " )";
    if ( $db->query( $sql ) )
    {
      $idRechnung = $db->insert_id;
      $pdf->rechnungNummer = '24h-' . $idRechnung;
    }
  }
  if ( $idRechnung == 0 ) // Rechnung konnte nicht erstellt werden
  {
    return false;
  }
  if ( $update )
  {
    $sql = "UPDATE `rechnung` SET `datum` = '" . date( "Y-m-d" ) . "' WHERE rechnung.id = " . $idRechnung;
    if ( !$db->query( $sql ) )
    {
      echo "Kein update der Rechnung möglich";
      return false;
    }
  }

  $pdf->AliasNbPages();
  $pdf->SetLeftMargin( 20 );
  $pdf->AddPage();
  $pdf->SetXY( 20, 55 );
  $pdf->SetFont( 'Arial', '', 8 );
  $pdf->Cell( 0, 4, utf8_decode( "DLRG Göttingen, Willi-Eichler-Straße 26, 37079 Göttingen" ), 0, 0, 'L' );

  //Rechnungsempfänger
  $pdf->SetXY( 20, 61 );
  $pdf->SetFont( 'Arial', '', 10 );

  $rechnungsadresse = new Rechnungsadresse();
  $rechnungsadresse->loadAdresseByBuchungId( $db, $idBuchung );
  if ( $rechnungsadresse->id == 0 )//Rechnungsadresse konnte nicht geladen werden
  {
    return false;
  }
  $tempAnschrift = $rechnungsadresse->firma . "\n"
                   . $rechnungsadresse->vorname . ' ' . $rechnungsadresse->nachname . "\n"
                   . $rechnungsadresse->strasse . "\n"
                   . $rechnungsadresse->plz . ' ' . $rechnungsadresse->ort;
  $pdf->MultiCell( 0, 4, html_entity_decode( $tempAnschrift, ENT_NOQUOTES, 'ISO-8859-1' ), 0 );

  //Faltlinie
  $pdf->SetXY( 10, 105 );
  $pdf->SetFont( 'Arial', 'B', 6 );
  $pdf->Cell( 0, 1, "-", 0, 0, 'L' );

  //Titel
  $pdf->SetXY( 20, 110 );
  $pdf->SetFont( 'Arial', 'B', 16 );
  $pdf->Cell( 0, 10, utf8_decode( "Rechnung " . $pdf->rechnungNummer ), 0, 1, 'L' );

  //Warenkorp
  $pdf->SetFont( 'Arial', 'B', 10 );
  $pdf->MultiCell( 0, 8, utf8_decode( "Auftrag: " . $idBuchung ), 0 );

  $header = array( 'ArtikelNr', 'Artikel', 'Anzahl', 'Einzelpreis', 'Summe' );
  $w = array( 20, 70, 20, 20, 20 );
  // Header Warenkorb
  for ( $i = 0; $i < count( $header ); $i ++ )
    $pdf->Cell( $w[ $i ], 6, utf8_decode( $header[ $i ] ), 1, 0, 'C' );
  $pdf->Ln();
  $pdf->SetFont( 'Arial', '', 10 );

  $gesamtSummeBestellung = 0;
  $sql = "SELECT bu_artikel.id, bu_artikel.name, SUM(bu_artikel.preis) AS summe, COUNT(bu_artikel.preis) AS anzahl,  "
         . "bu_artikel.preis, bu_rechnung_hinweis.id AS hinweis, bu_rechnung_hinweis.hinweis AS hinweistext "
         . "FROM bu_kategorien, bu_artikel, buchung_einnahme, bu_rechnung_hinweis  "
         . "WHERE buchung_einnahme.buchung_id =  " . $idBuchung
         . " AND buchung_einnahme.bu_artikel_id = bu_artikel.id  AND bu_artikel.bu_rechnung_hinweis_id = bu_rechnung_hinweis.id "
         . "AND bu_artikel.bu_kategorien_id = bu_kategorien.id"
         . " GROUP BY bu_artikel.id "
         . "ORDER BY bu_kategorien.kategorie, bu_artikel.name";
  $steuerhinweise = array();
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      $steuerhinweise[ $row[ 'hinweis' ] ] = $row[ 'hinweistext' ];
      $pdf->Cell( $w[ 0 ], 5, html_entity_decode( $row[ 'id' ], ENT_NOQUOTES, 'ISO-8859-1' ), 'LR', 0, 'R' );
      $pdf->Cell( $w[ 1 ], 5, html_entity_decode( $row[ 'name' ], ENT_NOQUOTES, 'ISO-8859-1' ) . '  *' . $row[ 'hinweis' ], 'LR' );
      $pdf->Cell( $w[ 2 ], 5, $row[ 'anzahl' ], 'LR' );
      $pdf->Cell( $w[ 3 ], 5, utf8_decode( numberToEuro( $row[ 'preis' ], 'Euro' ) ), 'LR', 0, 'R' );
      $pdf->Cell( $w[ 4 ], 5, utf8_decode( numberToEuro( $row[ 'summe' ], 'Euro' ) ), 'LR', 0, 'L' );
      $pdf->Ln();

      $gesamtSummeBestellung = $gesamtSummeBestellung + $row[ 'summe' ];
    }
    $pdf->Cell( array_sum( $w ), 0, '', 'T' );
  }
  else
  {
    echo '<br> Konnte Artikel nicht laden <br>';
    return false;
  }
  $pdf->Ln();
  $pdf->SetFont( 'Arial', 'B', 12 );
  // http://www.gesetze-im-internet.de/ustdv_1980/__33.html Umsatzsteuer-Durchführungsverordnung (UStDV)
  // § 33 Rechnungen über Kleinbeträge
  if ( $gesamtSummeBestellung > 150 )
  {
    $pdf->zeigeSteuernummer();
  }
  $pdf->Cell( 0, 10, utf8_decode( "Rechnungsbetrag: " . numberToEuro( $gesamtSummeBestellung, 'Euro' ) ), 0, 1, 'L' );
  $pdf->SetFont( 'Arial', '', 10 );

  if ( !$barkasse )
  {
    $pdf->MultiCell( 0, 5, utf8_decode( "Bitte überweisen Sie " . numberToEuro( $gesamtSummeBestellung, 'Euro' )
                                        . " ohne Abzug binnen 4 Tagen auf das folgende Konto:"
                                        . "\n" . "DLRG Göttingen \n" . "Sparkasse Göttingen  \n" . "BIC: NOLADE21GOE \n" . "IBAN: " . $pdf->iban
                                        . "\n" . "Verwendungszweck: " . $pdf->rechnungNummer ), 0 );
  }
  else
  {
    $pdf->MultiCell( 0, 5, utf8_decode( "Betrag in Bar erhalten." ), 0 );
  }
  $pdf->Ln();

  $hinweis = "";
  $sql = "SELECT versandart FROM `buchung` WHERE id = " . $idBuchung;
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      $pdf->versandart = $row[ 'versandart' ];
    }
  }
  if ( $pdf->versandart == 'kasse' )
  {
    $hinweis .= "Die Karten werden zudem an der Tageskasse für sie hinterlegt.";
  }
  $pdf->MultiCell( 0, 5, utf8_decode( $hinweis ), 0 );
  $pdf->Ln();
  $pdf->MultiCell( 0, 5, utf8_decode( "Mit freundlichen Grüßen \n Ihr Team vom Schwimmfestival" ), 0 );
  $pdf->Ln();
  foreach ( $steuerhinweise as $key => $value )
  {
    $pdf->MultiCell( 0, 5, html_entity_decode( "*" . $key . " : " . $value, ENT_NOQUOTES, 'ISO-8859-1' ), 0 );
  }

  $pdf->Output( '../Rechnungen/' . $pdf->rechnungNummer . 'Rechnung.pdf', 'F' );
  // echo '<a href="../Rechnungen/' . $pdf->rechnungNummer . 'Rechnung.pdf">Rechnung</a>';

  if ( !$barkasse )
  {
    $updateBuchung = "UPDATE `buchung` SET `status` = 'Rechnung' WHERE `buchung`.`id` = " . $idBuchung;
    if ( !$db->query( $updateBuchung ) )
    {
      return false;
    }
  }

  return true;

}
