<?php
    /**
 * Erzeugt einen Währungswert z.b. 3.5 wird zu 3,50 €
 */
function numberToEuro($number, $euroSymbol){
	
	$string = str_replace('.', ',', $number);
	$partString = explode(',', $string);
	if (count($partString)>1) { //z.B. 3.5
		if (strlen($partString[1]) >1) { //z.B. 3,52
			$string = $partString[0] . ','. $partString[1] . ' ' . $euroSymbol;
			return $string;
		}else{
			$string = $partString[0] . ','. $partString[1] . '0 ' . $euroSymbol;
			return $string;
		}
	}else{
		$string = $partString[0] . ',00 '. $euroSymbol;
		return $string;
	}
	
} 
?>