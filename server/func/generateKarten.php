<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 31.03.2016
 * Time: 18:36
 */

function generateKarten ( $idBuchung, mysqli $db, $kasse, $idRechnung, $update=false )
{
  if ( !file_exists( '../Karten/24h-' . $idRechnung . 'Karten.pdf' ) || $update )
  {
    $pdf = new PDF_Code39( 'P', 'mm', 'A4' );
    $pdf->AddPage();
    $pdf->SetFont( 'Arial', '', 12 );
    $i = 0;
    $hoeheGraphiken = 0;

    if ( $kasse == 'VK' )
    {
      $erw = STARTGEBUEHR_VK;
      $erm = STARTGEBUEHR_ERM_VK;
    }
    else
    {
      $erw = STARTGEBUEHR_TK;
      $erm = STARTGEBUEHR_ERM_TK;
    }

    $sql = "SELECT teilnehmer.id, teilnehmer.vorname, teilnehmer.nachname, DATE_FORMAT(teilnehmer.gebdat,'%d.%m.%Y') "
           ."AS gebdat, bu_artikel.name AS artikel, bu_artikel.preis "
           ."FROM teilnehmer, buchung_einnahme, bu_artikel "
           ."WHERE buchung_einnahme.buchung_id = " . $idBuchung . " "
           ."AND (buchung_einnahme.bu_artikel_id = " . $erw . " OR buchung_einnahme.bu_artikel_id = " . $erm . ") "
           ."AND buchung_einnahme.bu_artikel_id = bu_artikel.id AND buchung_einnahme.teilnehmer_id = teilnehmer.id ORDER BY teilnehmer.vorname";
    if ( $result = $db->query( $sql ) )
    {
      while ( $row = $result->fetch_assoc() )
      {
        //Damit 3 Karten auf eine Seite passen
        if ( $i > 0 )
        {
          if ( $i % 3 == 0 )
          {
            $pdf->AddPage();
          }
          else
          {
            $pdf->SetY( 100 * ( $i % 3 ) );
          }
          switch ( $i % 3 )
          {
            case 1:
              $hoeheGraphiken = 90;
              break;
            case 2:
              $hoeheGraphiken = 190;
              break;
            default:
              $hoeheGraphiken = 0;
              break;
          }
        }
        $temp = 10 + $hoeheGraphiken;
        $pdf->Image( '../pic/logo24h.png', 170, $temp, 30 );
        $pdf->SetFont( 'Arial', 'B', 16 );
        $pdf->Cell( 0, 8, utf8_decode( VERANSTALTUNG ), 0, 1, 'L' );
        $pdf->SetFont( 'Arial', 'B', 14 );
        $pdf->MultiCell( 180, 6, html_entity_decode( VERANSTALTUNG_ZEITRAUM . ", " . VERANSTALTUNG_ORT
                                                     . ", \n" . VERANSTALTUNG_STARTZEIT . " Start, Einlass ab " . VERANSTALTUNG_EINLASS, ENT_NOQUOTES,
                                                     'ISO-8859-1' ), 0, 'L' );
        $pdf->SetFont( 'Arial', 'B', 12 );
        $temp = html_entity_decode( $row[ 'artikel' ], ENT_NOQUOTES, 'ISO-8859-1' )
                . " " . numberToEuro( $row[ 'preis' ], 'EURO' ) . " "
                . html_entity_decode( $row[ 'vorname' ], ENT_NOQUOTES, 'ISO-8859-1' ) . " "
                . html_entity_decode( $row[ 'nachname' ], ENT_NOQUOTES, 'ISO-8859-1' ) . " " . $row[ 'gebdat' ];
        $pdf->Cell( 0, 6, $temp, 0, 1, 'L' );
        $pdf->SetFont( 'Arial', '', 8 );
        $temp = 40 + $hoeheGraphiken;
        $barcode = '';
        $idTemp = 10000 + $idBuchung;
        $barcode .= $idTemp;
        $idTemp = 10000 + $row[ 'id' ];
        $barcode .= $idTemp;
        $pdf->Code39( 120, $temp, $barcode, 1, 10 );

        $pdf->MultiCell( 100, 3.6,
                         utf8_decode( "\nWichtige Hinweise zum Ticket: Voraussetzung für den Einlass ist die Vorlage der gültigen Eintrittskarte (Ticket) am Einlasskontrollpunkt der Veranstaltung. Eine Person wird pro vorgelegtem gültigen Ticket eingelassen. Der Ticket-Inhaber hat seinen Ausdruck vor dem Zugriff und der Vervielfältigung durch Dritte stets zu schützen, und ihn vor Beschädigungen " ),
                         0, 'L' );
        $pdf->MultiCell( 0, 3.6,
                         utf8_decode( "oder starker Verschmutzung zu schützen. Täuschungsversuche werden zur Anzeige gebracht. Der gewerbsmäßige Weiterverkauf der Eintrittsberechtigungen ohne ausdrückliche schriftliche Genehmigung des Veranstalters ist untersagt. Keine Haftung für Sach- und Körperschäden. Zurücknahme der Karte nur bei Absage der gesamten Veranstaltung. Das Mitbringen von Hunden sowie Glasbehältern, Dosen, pyrotechnischen Gegenständen, Fackeln, Waffen u.ä. ist untersagt. Bei Nichtbeachtung erfolgt Verweis aus der Veranstaltung. Den Anweisungen des Ordnungspersonals ist Folge zu leisten. Ein Rechtsanspruch auf Teilnahme an allen angekündigten Veranstaltungen ist nicht gegeben. Programmänderungen sind vorbehalten. " ),
                         0, 'L' );

        $i ++;

      }
    }
    $pdf->Output( '../Karten/24h-' . $idRechnung . 'Karten.pdf' );
  }
}

