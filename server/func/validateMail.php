<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 29.03.2016
 * Time: 10:35
 */

//error_reporting( E_ALL );
//ini_set( 'display_errors', '1' );

require '../connect/connect_db.inc';
require '../constants.php';
require '../class/classContainer.php';
require '../func/serverFunc.php';

$db = getConnection();

if ( isset( $_GET[ 'mail' ] ) AND isset( $_GET[ 'idBuchung' ] ) )
{
  getHeader( 'html' );
  $rechnungsnummer = '';

  $sql = "SELECT rechnung.id FROM rechnung WHERE rechnung.buchung_id = " . $_GET[ 'idBuchung' ];
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      $rechnungsnummer = $row[ 'id' ];
    }
  }
  //var_dump($rechnungsnummer);

  if ( $rechnungsnummer == '' )
  {
    if ( !generateRechnung( $_GET[ 'idBuchung' ], $db ) )
    {
      echo 'Es gibt ein Problem mit der Rechnungserstellung. Bitte schreiben Sie an info@schwimmfestival.de';
      header( "HTTP/1.1 500 Internal Server Error" );
      exit( 500 );
    }
    else
    {
      // echo 'Rechnung wurde erzeugt <br>';
    }
  }

  //Update Besteller
  $email = '';
  $sql = "SELECT buchung.besteller_id AS id, besteller.email, rechnung.id AS rechnung  "
         . "FROM buchung, besteller, rechnung "
         . "WHERE buchung.besteller_id = besteller.id AND buchung.id = rechnung.buchung_id "
         . "AND buchung.id =" . $_GET[ 'idBuchung' ];
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {
      $idBesteller = $row[ 'id' ];
      $email = $row[ 'email' ];
      $rechnungsnummer = $row[ 'rechnung' ];
    }

    generateKarten( $_GET[ 'idBuchung' ], $db, 'VK', $rechnungsnummer );
  }
  else
  {
    header( "HTTP/1.1 500 Internal Server Error" );
    exit( 500 );
  }
  if ( $email == $_GET[ 'mail' ] )
  {
    $sql = "UPDATE `besteller` SET `isActive` = '2' WHERE `besteller`.`id` = " . $idBesteller
           . " AND `besteller`.`email` = '" . $_GET[ 'mail' ] . "'";
    if ( !$db->query( $sql ) )
    {
      echo "Es ist ein Problem aufgetreten " . $sql;
    }
    else
    {

      if ( sendRechnungMail( $_GET[ 'idBuchung' ], $db, $rechnungsnummer ) )
      {
        echo 'Vielen Dank für Ihre Mithilfe. Wir haben Ihnen soeben Ihre Rechnung und Ihre Eintrittskarten zugesand.';
      }
      else
      {
        echo "Leider konnten wir keine Email an Sie generieren. Bitte schreiben Sie eine E-Mail an info@schwimmfestival.de";
      }
    }
  }
  else
  {
    echo "E-Mail und Besteller stimmen nicht überein! Bitte schreiben Sie eine E-Mail an info@schwimmfestival.de";
  }

}
