<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 16.07.2016
 * Time: 23:35
 */

function boolConverterString ($string)
{
  if($string == 'true' OR $string == 'TRUE')
  {
    return true;
  }
  return false;
}

function boolConverterBool ($bool)
{
  if($bool)
  {
    return 'true';
  }
  return 'false';
}
