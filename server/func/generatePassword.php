<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 27.03.2016
 * Time: 20:15
 */

function generatePassword( $length = 10 ){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}
