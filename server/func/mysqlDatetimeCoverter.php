<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 14.07.2016
 * Time: 11:17
 */

/**
 * @param        $dateTime
 * @param string $format ''|'datum'|'zeit'
 *
 * @return string
 */
function mysqlDatetimeConverter ( $dateTime, $format = '' )
{
  $temp = explode( ' ', $dateTime );
  $date = $temp[ 0 ];
  if (count( $temp ) > 1)
  {
    $time = $temp[ 1 ];
  }
  $dateSplit = explode( '-', $date );

  if ( $format == 'datum' )
  {
    $convertedDateTime = $dateSplit[ 2 ] . '.' . $dateSplit[ 1 ] . '.' . $dateSplit[ 0 ];
  }
  else
  {
    if ( $format == 'zeit' )
    {
      $convertedDateTime = $time;
    }
    else
    {
      $convertedDateTime = $dateSplit[ 2 ] . '.' . $dateSplit[ 1 ] . '.' . $dateSplit[ 0 ] . ' ' . $time;
    }
  }

  return $convertedDateTime;
}
