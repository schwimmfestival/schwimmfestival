<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 31.03.2016
 * Time: 18:37
 */

function sendRechnungMail ( $idBuchung, $db, $rechnungsnummer )
{
  $rechnung = new Rechnung();
  $rechnung->loadRechnungById( $db, $rechnungsnummer );
  if($rechnung->id == 0)
  {
    echo 'Es ist leider ein Fehler aufgetreen.';
  }

  $message = "Hallo " . html_entity_decode( $rechnung->adresse->vorname, ENT_NOQUOTES, 'UTF-8' ) . " "
             . html_entity_decode( $rechnung->adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
             . "anbei erhalten Sie die Rechnung und die Eintrittskarten für das " . VERANSTALTUNG
             . " am " . VERANSTALTUNG_ZEITRAUM . " im " . html_entity_decode( VERANSTALTUNG_ORT, ENT_NOQUOTES, 'UTF-8' ) . ". Start ist um "
             . VERANSTALTUNG_STARTZEIT . ". Einlass ist ab " . VERANSTALTUNG_EINLASS
             . "\n \n"
             . "Bitte überweisen Sie den gesamten Rechnungsbetrag binnen 4 Tagen auf das in der Rechnung genannte Konto. "
             . "Sollten wir Innerhalb dieser Zeit keinen Zahlungseingang feststellen können, behalten wir uns vor, die Bestellung zu stornieren. \n \n"
             . MAIL_FOOT;

  $mail = getMail();
  $mail->Subject = 'Rechnung 24h-' . $rechnungsnummer . " " . VERANSTALTUNG;
  $mail->AddAddress( $rechnung->getMailAdresse( $db ) );
  $mail->AddAddress( "buchung@schwimmfestival.de" );
  $mail->Body = $message;
  $mail->addAttachment( '../Karten/24h-' . $rechnungsnummer . 'Karten.pdf' );

  $mail->addAttachment( '../Rechnungen/24h-' . $rechnungsnummer . 'Rechnung.pdf' );

  if ( !$mail->send() )
  {
    echo "Mailer Error: " . $mail->ErrorInfo;
    return false;
  }
  else
  {
    $sql = "UPDATE `buchung` SET `status` = 'Rechnung' WHERE `buchung`.`id` = " . $idBuchung;
    if ( !$db->query( $sql ) )
    {
      return false;
    }
    return true;
  }

}
