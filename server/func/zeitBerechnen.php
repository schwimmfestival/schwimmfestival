<?php
/**
 * Created by IntelliJ IDEA.
 * User: Florian Unger
 * Date: 08.08.2016
 * Time: 21:32
 */

function zeitAusMillisekunden($zahl)
{
  $stunden = 0;
  $minuten = 0;
  $sekunden = 0;
  if($zahl >= 3600000 )
  {
    $stunden = floor( $zahl / 3600000 );
    $zahl = $zahl - ($stunden * 3600000);
  }

  if($zahl >= 60000)
  {
    $minuten = floor( $zahl / 60000 );
    $zahl = $zahl - ($minuten * 60000);
  }
  if($zahl >= 1000)
  {
    $sekunden = floor( $zahl / 1000 );
    $zahl = $zahl - ($sekunden * 1000);
  }
  $hunderstel = floor( $zahl / 10 );

  if ($stunden < 10)
  {
    $stunden = '0' . $stunden;
  }
  if ($minuten < 10)
  {
    $minuten = '0' . $minuten;
  }
  if ($sekunden < 10)
  {
    $sekunden = '0' . $sekunden;
  }
  if ($hunderstel < 10)
  {
    $hunderstel = '0' . $hunderstel;
  }

  return $stunden . ':' .$minuten . ':' . $sekunden . '.' . $hunderstel;

}
