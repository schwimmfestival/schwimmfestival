<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 28.03.2016
 * Time: 17:58
 */

require '../connect/connect_mail.inc';

require 'boolConverter.php';

require_once 'errorHandling.php';

require 'generateBestellCode.php';
require 'generateKarten.php';
require 'generatePassword.php';
require 'generateRechnung.php';

require 'getHeader.php';

require 'mysqlDatetimeCoverter.php';

require 'numberToEuro.php';

require 'prooveDate.php';

require 'sendRechnungMail.php';
require 'sendValidationMail.php';

require 'zeitBerechnen.php';



