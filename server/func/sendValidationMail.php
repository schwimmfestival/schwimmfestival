<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 29.03.2016
 * Time: 09:51
 */

function sendValidationMail ( $rechnungsadresse, $idBesteller, $idBuchung, $db )
{
  $email = $rechnungsadresse->{'mail'};
  $adresse = new Adresse();
  $adresse->vorname = $rechnungsadresse->{'firstname'};
  $adresse->nachname = $rechnungsadresse->{'lastname'};
  return sendMailBestellerValidation( $adresse, $email, $idBesteller, $idBuchung, $db );
}

function sendMailBestellerValidation (Adresse $adresse, $email, $idBesteller, $idBuchung, $db )
{
  $mail = getMail();
  $mail->Subject = 'Bestellbestätigung ' . VERANSTALTUNG . ' vom ' . VERANSTALTUNG_ZEITRAUM;
  $mail->AddAddress( $email );

  $message = "Hallo " . html_entity_decode( $adresse->vorname, ENT_NOQUOTES, 'UTF-8' ) . " "
             . html_entity_decode( $adresse->nachname, ENT_NOQUOTES, 'UTF-8' ) . ", \n"
             . "zum Abschluss Ihrer Bestellung müssen Sie noch Ihre E-Mail Adresse bestätigen. \n \n"
             . "Rufen Sie dazu Bitte den folgenden Link auf. Sollte sich kein Browserfenster öffnen "
             . "kopieren Sie den Link bitte in ein Browserfenster und rufen ihn auf. \n"
             . "https://anmeldung.schwimmfestival.de/server/func/validateMail.php?mail="
             . $email
             . "&idBesteller="
             . $idBesteller
             . "&idBuchung="
             . $idBuchung
             . " \n \n"
             . "Nachdem Sie ihre E-Mail bestätigt haben bekommen sie eine weitere E-Mail mit Ihrer Rechnung sowie den Eintrittskarten. \n"
             . "Sofern ihre E-Mail Adresse nicht bestätigt wird, wird die Bestellung in 3 Tagen unsererseits storniert! \n \n"
             . "Ihre Bestellung ist wie folgt bei uns eingegangen. \n"
             . "ArtikelNr, Artikel, Anzahl, Einzelpreis, Summe \n";

  $gesamtSummeBestellung = 0;
  $sql = "SELECT bu_artikel.id,".
         " bu_artikel.name,".
         " SUM(bu_artikel.preis) AS summe,".
         " COUNT(bu_artikel.preis) AS anzahl, ".
         " bu_artikel.preis ".
         " FROM bu_kategorien, bu_artikel, buchung_einnahme, buchung ".
         " WHERE ".
          " buchung.id = " . $idBuchung .
          " AND buchung.id = buchung_einnahme.buchung_id ".
          " AND buchung_einnahme.bu_artikel_id = bu_artikel.id ".
          " AND bu_artikel.bu_kategorien_id = bu_kategorien.id ".
         " GROUP BY bu_artikel.id ORDER BY bu_kategorien.kategorie, bu_artikel.name";
  if ( $result = $db->query( $sql ) )
  {
    while ( $row = $result->fetch_assoc() )
    {

      $message .= $row[ 'id' ] . ", " . html_entity_decode( $row[ 'name' ], ENT_NOQUOTES, 'UTF-8' ) . ", "
                  . $row[ 'anzahl' ] . ", "
                  . numberToEuro( $row[ 'preis' ], 'Euro' ) . ", "
                  . numberToEuro( $row[ 'summe' ], 'Euro' ) . "\n";
      $gesamtSummeBestellung = $gesamtSummeBestellung + $row[ 'summe' ];
    }
  }
  else
  {
    errorHandling( $db->error . " File:". __FILE__ . "Line:" . __LINE__ . "</br> " . $sql );
  }

  $message .= " \n Gesamtsumme der Bestellung: " . numberToEuro( $gesamtSummeBestellung, 'Euro' ) . "\n \n ";

  $message .= MAIL_FOOT;
  $mail->Body = $message;

  if ( !$mail->send() )
  {
    errorHandling( 'senden Fehlgeschlagen' );
    return false;
  }
  return true;
}
