<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 27.03.2016
 * Time: 17:05
 */

function getHeader($type){
    if($type == 'json'){
        header('Content-type: application/json');
        header('Cache-Control: no-cache, must-revalidate');

    }
    if($type == 'html'){
        echo "
   <html>
   <head>
  <meta charset=\"utf-8\">
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <base href=\"/\">
  <title>Schwimmfestival</title>

  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  </head>
  <body>
        ";
    }
}
