-- phpMyAdmin SQL Dump
-- version 4.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Erstellungszeit: 25. Mrz 2016 um 12:19
-- Server-Version: 5.1.73
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `dlrg_goettingen_24h`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bu_kategorien`
--

CREATE TABLE `bu_kategorien` (
  `id` int(11) NOT NULL,
  `kategorie` varchar(45) NOT NULL,
  `katKurz` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bu_kategorien`
--

INSERT INTO `bu_kategorien` (`id`, `kategorie`, `katKurz`) VALUES
(1, 'TN Essen', 'TNEssen'),
(2, 'TN Essen vegetarisch', 'TNVegEssen'),
(3, 'TN Vorkasse', 'TNVK'),
(4, 'TN Tageskasse', 'TN');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bu_kategorien`
--
ALTER TABLE `bu_kategorien`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bu_kategorien`
--
ALTER TABLE `bu_kategorien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
