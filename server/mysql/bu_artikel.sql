-- phpMyAdmin SQL Dump
-- version 4.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Erstellungszeit: 25. Mrz 2016 um 12:19
-- Server-Version: 5.1.73
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `dlrg_goettingen_24h`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bu_artikel`
--

CREATE TABLE `bu_artikel` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `preis` float UNSIGNED DEFAULT NULL,
  `bu_kategorien_id` int(11) NOT NULL,
  `bu_rechnung_hinweis_id` int(11) DEFAULT NULL,
  `steuer` int(11) DEFAULT '0',
  `jahr` year(4) DEFAULT NULL COMMENT AS `Jahr in dem die Presie gültig sind`
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bu_artikel`
--

INSERT INTO `bu_artikel` (`id`, `name`, `preis`, `bu_kategorien_id`, `bu_rechnung_hinweis_id`, `steuer`, `jahr`) VALUES
(3, 'Fr&uuml;hst&uuml;ck', 2.5, 1, 1, 0, 2016),
(4, 'Fr&uuml;hst&uuml;ck', 2.5, 2, 1, 0, 2016),
(5, 'Mittagessen', 3, 1, 1, 0, 2016),
(6, 'Mittagessen', 3, 2, 1, 0, 2016),
(7, 'Abendessen', 4.5, 1, 1, 0, 2016),
(8, 'Abendessen', 4.5, 2, 1, 0, 2016),
(9, 'Startgeb&uuml;hr', 6, 3, 2, 0, 2016),
(10, 'Startgeb&uuml;hr', 8, 4, 2, 0, 2016),
(11, 'Startgeb&uuml;hr erm&auml;&szlig;igt', 4, 3, 2, 0, 2016),
(12, 'Startgeb&uuml;hr erm&auml;&szlig;igt', 6, 4, 2, 0, 2016);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bu_artikel`
--
ALTER TABLE `bu_artikel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bu_artikel_bu_kategorien1_idx` (`bu_kategorien_id`),
  ADD KEY `fk_bu_artikel_bu_rechnung_hinweis1_idx` (`bu_rechnung_hinweis_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bu_artikel`
--
ALTER TABLE `bu_artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bu_artikel`
--
ALTER TABLE `bu_artikel`
  ADD CONSTRAINT `fk_bu_artikel_bu_kategorien1` FOREIGN KEY (`bu_kategorien_id`) REFERENCES `bu_kategorien` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bu_artikel_bu_rechnung_hinweis1` FOREIGN KEY (`bu_rechnung_hinweis_id`) REFERENCES `bu_rechnung_hinweis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
