<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 31.03.2016
 * Time: 22:35
 */

//Veranstaltung
define("VERANSTALTUNG","Schwimmfestival 2016");

//IBAN

//Veranstaltungszeitraum
define("VERANSTALTUNG_ZEITRAUM","13.-14.08.2016");
//Ort
define("VERANSTALTUNG_ORT", "Freibad Brauweg in G&ouml;ttingen");
//Startzeit
define("VERANSTALTUNG_STARTZEIT", "11:00 Uhr");
//Einlass
define("VERANSTALTUNG_EINLASS", "10:00 Uhr");

//Artikel Startgebühren
define("STARTGEBUEHR_VK", 9);
define("STARTGEBUEHR_ERM_VK", 11);
define("STARTGEBUEHR_TK", 10);
define("STARTGEBUEHR_ERM_TK", 12);

//Mail
define("MAIL_FOOT", "Mit freundlichen Grüßen \n vom Schwimmfestival Team \n \n"
    . "Deutsche Lebens-Rettungs-Gesellschaft \n"
    ."Landesverband Niedersachen – Bezirk Göttingen \n"
    ."Ortsgruppe Göttingen e.V. \n"
    ."\n"
    ."Anschrift: \n"
    ."DLRG Göttingen e.V. \n"
    ."Willi-Eichler-Straße 26  \n"
    ."37079 Göttingen \n"
    ."\n"
    ."Telefon: 0551 30 77 922 \n"
    ."Telefax: 0551 48 95 596 \n"
    ."\n"
    ."Webseite: http://www.goettingen.dlrg.de \n"
    ."Webseite Jugend: http://www.goettingen.dlrg-jugend.de \n");
